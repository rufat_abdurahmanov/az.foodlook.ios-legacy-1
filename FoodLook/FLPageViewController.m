//
//  FLPageViewController.m
//  FoodLook
//
//  Created by Rinat Yahin on 28.01.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLPageViewController.h"
#import "UIImageView+AFNetworking.h"
@interface FLPageViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *backgraundView;

@end

@implementation FLPageViewController
{
    UISwipeGestureRecognizer* swipeLeft;
    UISwipeGestureRecognizer* swipeRight;
    NSInteger imageIndex;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.imageView setImageWithURL:[NSURL URLWithString:self.str]];
    imageIndex = self.index;
    [self addGestureTap];
    [self addGestureSwipe];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addGestureTap{
    UIGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissModalView)];
    [self.backgraundView addGestureRecognizer:tap];
}

-(void)addGestureSwipe {
    swipeLeft = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipe:)];
    swipeRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipe:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    swipeLeft.numberOfTouchesRequired = 1;
    swipeRight.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:swipeRight];
    [self.view addGestureRecognizer:swipeLeft];
}

-(void)dismissModalView{
    
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (void)handleSwipe:(UISwipeGestureRecognizer *)sender {
    
    if (sender.direction & UISwipeGestureRecognizerDirectionLeft) {
        imageIndex ++;
    }
    if (sender.direction & UISwipeGestureRecognizerDirectionRight) {
         imageIndex --;
    }
    imageIndex = (imageIndex < 0) ? ([_array count] -1):(imageIndex)% [_array count];
    [self.imageView setImageWithURL:[NSURL URLWithString:[_array objectAtIndex:imageIndex]]];
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
