//
//  FLCommentViewController.m
//  FoodLook
//
//  Created by Rinat Yahin on 07/01/15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLCommentViewController.h"
#import "ServerManager.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "FLCommentModel.h"
#import "FLCommentTableViewCell.h"
#import  <QuartzCore/QuartzCore.h>
#import "FLDetailFoodViewController.h"
#import "FLDetailCommentViewController.h"
#import "NSString+CalculateHeight.h"
#import <MBProgressHUD.h>
@interface FLCommentViewController () <UITextViewDelegate, UITableViewDataSource, UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) FLCommentTableViewCell* customCell;
@property (strong, nonatomic) NSArray* arrayComment;

@end

@implementation FLCommentViewController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getCommentsFromServer];
      [_textField setPlaceholder:NSLocalizedString(@"Add a comment", nil)];
        [_btnSend setTitle:NSLocalizedString(@"Send", nil) forState:UIControlStateNormal];
      [_backBtn setTitle:NSLocalizedString(@"Feedback", nil) forState:UIControlStateNormal];
    [_btnSend sizeToFit];

}

- (void)viewDidLoad {
     [super viewDidLoad];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.arrayComment = [NSArray new];
    [self getCommentsFromServer];
    [self.scrollView setScrollEnabled:NO];

     
    _btnSend.layer.cornerRadius = 10; // this value vary as per your desire
    _btnSend.clipsToBounds = YES;

    
    UITapGestureRecognizer* tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    
    [self.tableView addGestureRecognizer:tapper];
    [self.tableView isEditing];
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
}

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"Swipe received.");
}

#pragma mark - Actions

- (IBAction)sendComment:(id)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if ([self textFieldEmptyOrNot:self.textField]){
        self.textField.text = @" ";
        NSLog(@"Empty");
    }else {
        [self.view endEditing:YES];
        [self postCommentToServer];
         [self performSelector:@selector(scrollToBottom) withObject:nil afterDelay:0.5f];
         NSLog(@"NotEmpty");
    }

}


- (IBAction)goBack:(UIButton *)sender {

    [self.navigationController popViewControllerAnimated:YES];
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - API

-(void)postCommentToServer {
  
    [[ServerManager sharedManager] postCommentInServerWithID:self.strID andTextComment:self.textField.text withOnSuccess:^(id results) {
        NSLog(@"%@", results);
        [self getCommentsFromServer];
         self.textField.text = @" ";
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}

-(void)getCommentsFromServer {
    
    [[ServerManager sharedManager] getCommentWithID:self.strID onSuccess:^(NSArray* result) {
        self.arrayComment = result;
        [self.tableView reloadData];
//        [self scrollToBottom];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"%@",error);
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
    
}



#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        [self dismissViewControllerAnimated:YES completion:^{
        
        }];
        return NO;
    }

    return YES;
}


#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrayComment count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     FLCommentTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (!cell) {
        cell = [[FLCommentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    FLCommentModel* comment = [self.arrayComment objectAtIndex:indexPath.row];
    NSString* str = [NSString stringWithFormat:@"%@:",[comment.user objectForKey:@"username"]] ;
    cell.lblUsername.text = str;
    cell.lblComment.text  = comment.text;
    
    CGFloat tableViewWidth =   self.tableView.frame.size.width;
    CGFloat dX = tableViewWidth - tableViewWidth/5;
    
    CGFloat commentHeight = [comment.text getHeightOfFont:cell.lblComment.font widht:dX];
    CGFloat userName = [str getHeightOfFont:cell.lblUsername.font widht:tableViewWidth/5];
    cell.lblComment.frame = CGRectMake(tableViewWidth/5, cell.lblComment.frame.origin.y, dX, commentHeight);
    cell.lblUsername.frame = CGRectMake(0, cell.lblUsername.frame.origin.y, tableViewWidth/5, userName);
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FLCommentModel* post = [self.arrayComment objectAtIndex:indexPath.row];
    NSString* strComment = post.text;
    UIFont* font = [UIFont systemFontOfSize:13];
    CGFloat tableViewWidth =   self.tableView.frame.size.width;
    CGFloat dX = (tableViewWidth - tableViewWidth/5);
    NSString* strUser = [NSString stringWithFormat:@"%@:",[post.user objectForKey:@"username"]] ;
    
    CGFloat userName = [strUser getHeightOfFont:font widht:tableViewWidth/5];
    CGFloat commentHeight = [strComment getHeightOfFont:font widht:dX];
    NSLog(@"%f", commentHeight);
    if (commentHeight <  userName ) {
        return userName;
    }
    
    
    return commentHeight;
}



- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  UITableViewCellEditingStyleDelete;
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        FLCommentModel* comment = [self.arrayComment objectAtIndex:indexPath.row];
        NSString* str = comment.idComment;
        
        NSMutableArray* tempArray = [NSMutableArray arrayWithArray:self.arrayComment];
        [tempArray removeObject:comment];
        self.arrayComment = tempArray;
        NSLog(@"%@  %ld", str, (long)indexPath.row) ;
        
        [tableView beginUpdates];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        [self deleteCommentInServerWithIDComment:str];
        [tableView endUpdates];
    }
}

-(void)deleteCommentInServerWithIDComment:(NSString*)strIDComment {
    [[ServerManager sharedManager] deleteCommentInServerWithID:strIDComment withOnSuccess:^(id results) {
        NSLog(@"%@",results);
    } onFailure:^(NSError *error, NSInteger statusCode) {
        NSLog(@"%@",error);
    }];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    FLCommentModel* comment = [self.arrayComment objectAtIndex:indexPath.row];
//    NSLog(@"%@", comment.text);
    NSDictionary* dict = @{@"text":comment.text ,@"id": comment.idComment};
    [self performSegueWithIdentifier:@"FLDetailCommentViewController" sender:dict];

}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"FLDetailCommentViewController"]) {
        FLDetailCommentViewController* vc = [segue destinationViewController];
        vc.dict = sender;

    }
}

-(void)scrollToBottom{
    
    [self.tableView scrollRectToVisible:CGRectMake(0, self.tableView.contentSize.height - self.tableView.bounds.size.height, self.tableView.bounds.size.width, self.tableView.bounds.size.height) animated:YES];
    
}

#pragma mark - UIKeyboardDidShowNotification 


//- (void)keyboardDidShow:(NSNotification *)notification
//{
//    // Called when the keyboard finished showing up
//   
//
//}



#pragma mark - UITapGestureRecognizer

- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
    
}

#pragma mark - UITextField

-(BOOL)textFieldEmptyOrNot:(UITextField *)textfield {
    
    NSString *rawString = [textfield text];
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed = [rawString stringByTrimmingCharactersInSet:whitespace];
    if ([trimmed length] == 0)
        return YES;
    else
        return NO;
}



- (void) dealloc {

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    

    
    self.scrollView.contentOffset = CGPointZero;
}


@end
