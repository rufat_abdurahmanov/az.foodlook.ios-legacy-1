//
//  FLCommentTableViewCell.h
//  FoodLook
//
//  Created by Rinat Yahin on 07/01/15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLCommentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblUsername;
@property (weak, nonatomic) IBOutlet UILabel *lblComment;



- (CGFloat) heightForText:(NSString*) text;


@end
