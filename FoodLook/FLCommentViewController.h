//
//  FLCommentViewController.h
//  FoodLook
//
//  Created by Rinat Yahin on 07/01/15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TPKeyboardAvoidingScrollView;

@interface FLCommentViewController : UIViewController

@property (strong, nonatomic) NSString* strID;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@end
