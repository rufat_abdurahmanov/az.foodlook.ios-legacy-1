//
//  FLDetailsRestoViewController.m
//  FoodLook
//
//  Created by Yahin Rinat on 12.08.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import "FLDetailsRestoViewController.h"
#import "FLRestoTableViewCell.h"
#import "ServerManager.h"
#import  <QuartzCore/QuartzCore.h>
#import "FLMenuResto.h"
#import "FLMailViewViewController.h"
#import "UIImageView+AFNetworking.h"
#import "FLWebViewController.h"
#import "FLMapViewController.h"
#import "FLMenuResto.h"
#import "FLCategoriesMenuViewController.h"
#import "FLModalViewController.h"
#import <Social/Social.h>
#import "FLCommentViewController.h"
#import "FLPhotoRestoCollectionViewCell.h"
#import "FLPageViewController.h"
#import <MBProgressHUD.h>

@interface FLDetailsRestoViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate>
#pragma mark - IBOutlet
@property (weak, nonatomic) IBOutlet UIImageView *imageResto;
@property (weak, nonatomic) IBOutlet UICollectionView  *collectionView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *conteinerButtonView;
@property (weak, nonatomic) IBOutlet UIView *moreInfoView;
@property (weak, nonatomic) IBOutlet UIView *addCommentView;

@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *addComentBtn;
@property (weak, nonatomic) IBOutlet UILabel *likeCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *shareCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentCountLabel;


@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UILabel *priceRestoLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameRestoLabel;
@property (weak, nonatomic) IBOutlet UIButton *moreInfo;
@property (weak, nonatomic) IBOutlet UIButton *addReadBtn;
@property (weak, nonatomic) IBOutlet UIButton *makeReservBtn;

#pragma mark - property
@property (nonatomic,retain) NSDictionary* data;
@property (strong,nonatomic) FLModalViewController *modal;
@property (strong, nonatomic) NSString* acceptLanguage;
@end
static NSString* kSettingsLanguage = @"language";
@implementation FLDetailsRestoViewController
{
    UIButton* button;
    NSArray* categories;
    NSDictionary* social;
    NSArray* photoRestoArr;
    NSDictionary* restoResult;
    
}

#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - ViewController Life Cycle

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_moreInfo setTitle:NSLocalizedString(@"More info", nil) forState:UIControlStateNormal];
    [_addReadBtn setTitle:NSLocalizedString(@"Add or read feedback", nil) forState:UIControlStateNormal];
    [_makeReservBtn setTitle:NSLocalizedString(@"Make Reservation", nil) forState:UIControlStateNormal];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadLanguage];
    [self getRestoFromServer];
    _moreInfo.layer.cornerRadius = 10; // this value vary as per your desire
    _moreInfo.clipsToBounds = YES;
    
    [self categoriesButton];
    [self settingsScrolView];
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
    [self socialSettings];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

}
-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"Swipe received.");
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark - UIScrolView
-(void)settingsScrolView {
    CGRect contentButtom = _conteinerButtonView.frame;
    CGFloat maxY = CGRectGetMaxY(contentButtom);
    CGFloat frameCollectionView = maxY + 50;
    
    CGRect collectionView = CGRectMake(_collectionView.frame.origin.x, frameCollectionView, _collectionView.frame.size.width, _collectionView.frame.size.height);
    
    _collectionView.frame = collectionView;
    
    CGFloat maxYcollection = CGRectGetMaxY(collectionView);
    CGFloat frameMoreInfro = maxYcollection + 20;
    
    CGRect moreInfoFrame = CGRectMake(_moreInfoView.frame.origin.x, frameMoreInfro, _moreInfoView.frame.size.width, _moreInfoView.frame.size.height);
    
    _moreInfoView.frame = moreInfoFrame;
    
    CGFloat maxMoreInfo = CGRectGetMaxY(moreInfoFrame);
    CGFloat frameAddComment = maxMoreInfo;
    
    CGRect addCommentFrame = CGRectMake(_addCommentView.frame.origin.x, frameAddComment, _addCommentView.frame.size.width, _addCommentView.frame.size.height);
    
    _addCommentView.frame = addCommentFrame;
    
    CGFloat menuHeight  = _conteinerButtonView.frame.size.height;
    CGFloat collHeight = _collectionView.frame.size.height;
    CGFloat moreInfo = _moreInfoView.frame.size.height;
    CGFloat addCommentView = _addCommentView.frame.size.height;
    CGFloat sizeOfContent = 0;
    
    sizeOfContent = menuHeight+collHeight + moreInfo + addCommentView;
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.scrollView.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    self.scrollView.contentSize = contentRect.size;
}
#pragma mark - Custom view with UIButton

-(void)categoriesButton{

   
    int rowY = 0;
    float sumX = 20;
    NSInteger i = 0;
    for (NSDictionary* dict in categories) {
        
        button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        NSString* str = [dict objectForKey:@"label"];
        [button setTitle:[NSString stringWithFormat:@"%@",str]forState:UIControlStateNormal];
        [button setTintColor:[UIColor whiteColor]];
        [button sizeToFit];
        [button setTag:i];
        [button addTarget:self action:@selector(buttonClicked:)
                            forControlEvents: UIControlEventTouchUpInside];
        i ++;
        float width = button.frame.size.width;
        if (sumX + width > _conteinerButtonView.frame.size.width)
        {
            sumX = 20;
            rowY++;

        }

        button.frame = CGRectMake(sumX, rowY * 20, width, 25);
        [self.conteinerButtonView addSubview:button];
        sumX = sumX + width + 10;
    }
    
    [self.conteinerButtonView setFrame:CGRectMake(_conteinerButtonView.frame.origin.x, _conteinerButtonView.frame.origin.y, _conteinerButtonView.frame.size.width, rowY * 20 + 25)];
         
}

- (void)buttonClicked:(UIButton*)sender
{
    NSString* strLabel = [restoResult objectForKey:@"label"];
    NSDictionary* dct = [categories objectAtIndex:sender.tag];
    NSString* categoriesID = [dct objectForKey:@"id"];
    NSString* categoriesName = [dct objectForKey:@"label"];
   
    _data = @{@"categories":categoriesID, @"idRest":_idResto,@"nameCategories":categoriesName,@"label":strLabel};
    [self performSegueWithIdentifier:@"FLCategoriesMenuViewController" sender:_data];
}

-(void)loadLanguage
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.acceptLanguage = [userDefaults objectForKey:kSettingsLanguage];
    
}




#pragma mark - API
-(void)getRestoFromServer
{
    
    [[ServerManager sharedManager]getRestoDetailWithID:_idResto onSuccess:^(id result) {
        restoResult = result;
        NSLog(@"%@",result);
        social = [result  objectForKey:@"social"];
        photoRestoArr = [result objectForKey:@"photos"];
        categories = [result objectForKey:@"menu"];
        [self updateUI];
        [self.collectionView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } withAcceptLanguage:_acceptLanguage onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
    
}
-(void)postShareFrommServerWithStrStrID:(NSString*)strID {
    [[ServerManager sharedManager]postShareInServerWithID:strID withOnSuccess:^(id results) {
        [self getRestoFromServer];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

-(void)deleteLikeInServerWithIDEntity:(NSString*)strEntity andWithDishID:(NSString*)dishID {
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[ServerManager sharedManager]deleteLikeInServerWithID:strEntity withOnSuccess:^(id results) {
        [self getRestoFromServer];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

-(void)updateUI
{
 
    [self.imageResto setImageWithURL:
     [NSURL URLWithString:[restoResult objectForKey:@"background_image"]]];
    [self.logoImageView setImageWithURL:
     [NSURL URLWithString:[restoResult objectForKey:@"logo"]]];
    [self categoriesButton];
    self.priceRestoLabel.text =  [NSString stringWithFormat:@"%@",[restoResult objectForKey:@"price"]];
    self.nameRestoLabel.text =  [NSString stringWithFormat:@"%@",[restoResult objectForKey:@"label"]];
    [self.nameRestoLabel sizeToFit];
    self.logoImageView.layer.borderWidth=1.0;
    self.logoImageView.layer.masksToBounds = YES;
    self.logoImageView.layer.borderColor=[[UIColor redColor] CGColor];
    
    //social
    self.likeCountLabel.text =  [NSString stringWithFormat:@"%@",[social objectForKey:@"likes"]];
    [self.likeCountLabel sizeToFit];
    self.shareCountLabel.text =  [NSString stringWithFormat:@"%@",[social objectForKey:@"shares"]];
    [self.shareCountLabel sizeToFit];
    self.commentCountLabel.text = [NSString stringWithFormat:@"%@", [social objectForKey:@"comments"]];
    [self.commentCountLabel sizeToFit];
    NSString* str = [NSString stringWithFormat:@"%@",[social objectForKey:@"like_id"]];
    NSString* strNull = @"<null>";
    if ([str isEqualToString:strNull]) {
        [_likeButton setImage:[UIImage imageNamed:@"btnLikeGrey@2x"] forState:UIControlStateNormal];
    }else if (![str isEqualToString:strNull]) {
        [_likeButton setImage:[UIImage imageNamed:@"btnLikeRed@2x"] forState:UIControlStateNormal];
    }

}

-(void)socialSettings {
    
    static float offset = 17;
    static float offsetLbl = 4;
    
    float maxLikeBtnY = CGRectGetMaxX(self.likeButton.frame);
    self.likeCountLabel.frame = CGRectMake(maxLikeBtnY + offsetLbl,
                                         self.likeCountLabel.frame.origin.y,
                                         self.likeCountLabel.frame.size.width,
                                         self.likeCountLabel.frame.size.height);
    
    float maxLikeY = CGRectGetMaxX(self.likeCountLabel.frame);
    self.shareButton.frame = CGRectMake(maxLikeY + offset,
                                        self.shareButton.frame.origin.y,
                                        self.shareButton.frame.size.width,
                                        self.shareButton.frame.size.height);
    
    
    
    float maxshareBtnY = CGRectGetMaxX(self.shareButton.frame);
    self.shareCountLabel.frame = CGRectMake(maxshareBtnY + offsetLbl,
                                          self.shareCountLabel.frame.origin.y,
                                          self.shareCountLabel.frame.size.width,
                                          self.shareCountLabel.frame.size.height);
    
    float maxCountShareY = CGRectGetMaxX(self.shareCountLabel.frame);
    self.addComentBtn.frame = CGRectMake(maxCountShareY + offset,
                                             self.addComentBtn.frame.origin.y,
                                             self.addComentBtn.frame.size.width,
                                             self.addComentBtn.frame.size.height);
    
    
    float maxbtnCommentY = CGRectGetMaxX(self.addComentBtn.frame);
    
    self.commentCountLabel.frame = CGRectMake(maxbtnCommentY + offsetLbl,
                                            self.commentCountLabel.frame.origin.y,
                                            self.commentCountLabel.frame.size.width,
                                            self.commentCountLabel.frame.size.height);
}



#pragma mark - Social

- (IBAction)likeAction:(UIButton *)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString* strIdResto = _idResto;
    [self getRestoFromServer];
    NSString* str = [NSString stringWithFormat:@"%@",[social objectForKey:@"like_id"]];
    [[ServerManager sharedManager] postLikeInServerWithID:strIdResto withOnSuccess:^(id result) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
    } onFailure:^(NSError *error, NSInteger statusCode) {
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self deleteLikeInServerWithIDEntity:str andWithDishID:strIdResto];
    }];
}
- (IBAction)addComment:(UIButton *)sender {
    [self performSegueWithIdentifier:@"CommentResto" sender:nil];
}

- (IBAction)postToFacebook:(id)sender {
    
    NSString* strIdResto = [restoResult objectForKey:@"id"];
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [controller setInitialText:[restoResult objectForKey:@"label"]];
        [controller addURL:[NSURL URLWithString:[restoResult objectForKey:@"background_image"]]];
        [controller setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    [self postShareFrommServerWithStrStrID:strIdResto];
                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
        }];
        [self presentViewController:controller animated:YES completion:nil];
    }else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!"
                                                        message:@"Please login to Facebook from the phone settings"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Ok",nil];
        [alert show];
    }
}




- (IBAction)backButtonAction:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - ModalViewController

- (IBAction)toggleHalfModal:(UIButton *)sender {
    
    NSString* str = _idResto;
    self.modal = [self.storyboard instantiateViewControllerWithIdentifier:@"modalVC"];
    self.modal.data = str;
    [self addChildViewController:self.modal];
    self.modal.view.frame = [self maxCoordinateModalView];
    [self.view addSubview:self.modal.view];
    
    [UIView animateWithDuration:1 animations:^{
        self.modal.view.frame = [self minCoordinateModalView];
    } completion:^(BOOL finished) {
        [self.modal didMoveToParentViewController:self];
        [self addGestureTap];
    }];
    
}

-(void)addGestureTap{
  UIGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissModalView)];
    [self.view addGestureRecognizer:tap];
}



-(void)dismissModalView{
    
    [UIView animateWithDuration:1 animations:^{
        self.modal.view.frame = [self maxCoordinateModalView];
    } completion:^(BOOL finished) {
        [self.modal.view removeFromSuperview];
        [self.modal removeFromParentViewController];
        self.modal = nil;
        
    }];
}

-(CGRect)maxCoordinateModalView{
    
    CGRect rectInfoView = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y,                    self.view.frame.size.width, self.view.frame.size.height);
    CGFloat maxInfoViewYCordinate =  CGRectGetMaxY(rectInfoView);
    CGRect frame = CGRectMake(self.view.frame.origin.x, maxInfoViewYCordinate,self.view.frame.size.width, self.view.frame.size.height);
    return frame;
    
}

-(CGRect)minCoordinateModalView{
    
    CGRect rectInfoView = CGRectMake(self.logoImageView.frame.origin.x, self.logoImageView.frame.origin.y,self.logoImageView.frame.size.width, self.logoImageView.frame.size.height);
    CGFloat minYrectView =  CGRectGetMinY(rectInfoView);
    
    CGRect frame = CGRectMake(self.view.frame.origin.x, minYrectView, self.view.frame.size.width, self.view.frame.size.height - minYrectView);;
    return frame;
    
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"FLCategoriesMenuViewController"]){
        FLCategoriesMenuViewController *controller = (FLCategoriesMenuViewController*)segue.destinationViewController;
        controller.dict = sender;
    }
    
    if ([segue.identifier isEqualToString:@"CommentResto"]) {
        FLCommentViewController* vc = [segue destinationViewController];
        vc.strID = _idResto;
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSLog(@"%lu",(unsigned long)[photoRestoArr count]);
    return [photoRestoArr count];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *reuseIdentifier = @"Cell";

    FLPhotoRestoCollectionViewCell *cell = (FLPhotoRestoCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    NSString* strURL = [photoRestoArr objectAtIndex:indexPath.row];
    [cell.imageView setImageWithURL:[NSURL URLWithString:strURL]];
    
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString* url  = [photoRestoArr objectAtIndex:indexPath.row];
   NSInteger index = indexPath.row;

    FLPageViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"FLPageViewController"];
    controller.str= url;
    controller.array = photoRestoArr;
    controller.index = index;
    [self.view addSubview:controller.view];
    [self addChildViewController:controller];
    [controller didMoveToParentViewController:self];
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Adjust cell size for orientation
    CGSize result = [[UIScreen mainScreen] bounds].size;
    if(result.height == 480)
    {
        // iPhone Classic
        return CGSizeMake(90.0f, 80.0f);
    }
    else if(result.height == 568)
    {
        // iPhone 5
        return CGSizeMake(110.0f, 100.0f);
    }
    else if(result.height == 667)
    {
        // iPhone 6
        return CGSizeMake(180.0f, 150.0f);
    }
    else if(result.height == 736)
    {
        // iPhone 6 Plus
       return CGSizeMake(180.0f, 170.0f);
    }
    return CGSizeMake(100.f, 100.f);

}





@end
