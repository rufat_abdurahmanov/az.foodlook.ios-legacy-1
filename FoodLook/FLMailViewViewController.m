//
//  FLMailViewViewController.m
//  FoodLook
//
//  Created by Yahin Rinat on 15.11.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import "FLMailViewViewController.h"
#import <MessageUI/MessageUI.h>

@interface FLMailViewViewController ()<UINavigationBarDelegate, UITextViewDelegate, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation FLMailViewViewController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
       [self.textView becomeFirstResponder];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
}

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"Swipe received.");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark UITextViewDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    return YES;

}

- (UIBarPosition)positionForBar:(id <UIBarPositioning>)bar {
    return UIBarPositionTopAttached; }
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)cancel:(id)sender{
   [self.navigationController popViewControllerAnimated:YES];
    [UIView animateWithDuration:0.4 animations:^ {
        CGRect rect = self.view.bounds;
        rect.origin.y += rect.size.height;
        self.view.frame = rect;
        
    }
                     completion:^(BOOL finished)
     {
         [self.view removeFromSuperview];
         [self removeFromParentViewController];
     }];
}
-(IBAction)send:(id)sender
{

    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    // Email Subject
    [mc setSubject:@"From FoodLook App"];
    // Email Content
    [mc setMessageBody:self.textView.text isHTML:YES];
    // To address
    [mc setToRecipients:[NSArray arrayWithObject:self.email]];
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
    
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];

}
@end
