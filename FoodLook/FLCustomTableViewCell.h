//
//  FLCustomTableViewCell.h
//  FoodLook
//
//  Created by Yahin Rinat on 28.06.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FLCustomTableViewCell;

@protocol FLCustomTableViewCellDelegate <NSObject>

@optional
-(void)likeButton:(FLCustomTableViewCell*)cell;
-(void)shareButton:(FLCustomTableViewCell*)cell;
-(void)commentButton:(FLCustomTableViewCell*)cell;
@end

@interface FLCustomTableViewCell : UITableViewCell

@property (weak, nonatomic) id<FLCustomTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *dishesImage;
@property (weak, nonatomic) IBOutlet UIImageView *placeHolder;
@property (weak, nonatomic) IBOutlet UIImageView *manat;
@property (weak, nonatomic) IBOutlet UILabel *nameDishesLabel;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIButton *buttonlikes;
@property (weak, nonatomic) IBOutlet UILabel *labelPrice;
@property (weak, nonatomic) IBOutlet UILabel *like_quantity;


@end
