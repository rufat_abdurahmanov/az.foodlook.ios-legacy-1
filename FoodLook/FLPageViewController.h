//
//  FLPageViewController.h
//  FoodLook
//
//  Created by Rinat Yahin on 28.01.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLPageViewController : UIViewController
@property(strong, nonatomic) NSString* str;
@property(strong, nonatomic) NSArray* array;
@property(assign, nonatomic) NSInteger index;
@end
