//
//  FLMenuResto.m
//  FoodLook
//
//  Created by Yahin Rinat on 10.10.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import "FLMenuResto.h"

@implementation FLMenuResto
- (id) initWithServerResponse:(NSDictionary*) responseObject
{
    self = [super init];
    if (self) {
        
        
        
        NSString* idDish = [[responseObject objectForKey:@"id"]stringValue];
        
        if (idDish) {
            self.idDish = idDish;
            
        }
        
        NSString* cost = [responseObject objectForKey:@"price"];
        
        if (cost) {
            self.cost = cost;
            
        }
        
        NSString* nameDish = [responseObject objectForKey:@"label"];
        
        if (nameDish) {
            self.nameDish = nameDish;
            
        }
        
        NSString* imageDish = [responseObject objectForKey:@"image"];

        if (imageDish) {
 
            self.imageDish = imageDish;
        }
        
        
    }
    return self;
}

@end
