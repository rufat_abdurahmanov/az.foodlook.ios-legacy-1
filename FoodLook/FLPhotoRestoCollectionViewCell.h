//
//  FLPhotoRestoCollectionViewCell.h
//  FoodLook
//
//  Created by Rinat Yahin on 28.01.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLPhotoRestoCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end
