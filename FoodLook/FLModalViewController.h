//
//  FLModalViewController.h
//  FoodLook
//
//  Created by Rinat Yahin on 27.01.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLModalViewController : UIViewController

@property (strong, nonatomic) NSString* data;

@end
