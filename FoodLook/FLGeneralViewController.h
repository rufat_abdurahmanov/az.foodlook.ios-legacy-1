//
//  FLGeneralViewController.h
//  FoodLook
//
//  Created by Yahin Rinat on 28.06.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

typedef enum {
    VegeterianModel,
    AvaregeFoodModel,
    CourseModel
} FoodModelType;


@interface FLGeneralViewController : UIViewController

@property (assign, nonatomic) NSInteger currentPage;
@property (assign, nonatomic) NSInteger currentResoPage;
@property (weak, nonatomic) IBOutlet UITableView *restoTableView;
@property (weak, nonatomic) IBOutlet UITableView *foodTableView;
@property (strong, nonatomic) UIView* black;
@property (weak, nonatomic) IBOutlet UIButton *sidebarButton;




@end
