//
//  FLModalViewController.m
//  FoodLook
//
//  Created by Rinat Yahin on 27.01.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLModalViewController.h"
#import "ServerManager.h"
#import "FLMailViewViewController.h"
#import "FLMapViewController.h"
#import "FLWebViewController.h"
#import "FLLocationModel.h"
#import "FLInfoRestoCell.h"
#import "FLBranchesRestaurantCell.h"
#import "NSString+CalculateHeight.h"
#import <MBProgressHUD.h>

@interface FLModalViewController () <UITableViewDataSource, UITableViewDelegate,FLInfoRestoCellDelegate,FLBranchesRestaurantCellDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) FLInfoRestoCell* infoCell;
@property (strong, nonatomic) FLBranchesRestaurantCell* branchCell;
@property (strong, nonatomic) NSString* acceptLanguage;
@end
static NSString* kSettingsLanguage = @"language";


@implementation FLModalViewController 
{
    NSDictionary* resultDict;
    NSArray* locations;
    NSString* kitchenOpen;
    NSString* kitchenClose;
    NSString* restoOpen;
    NSString* restoClose;
    NSString* creditCard;
    NSString* parking;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadLanguage];
    [self getRestoFromServer];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

}


-(void)loadLanguage
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.acceptLanguage = [userDefaults objectForKey:kSettingsLanguage];
    
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.tableView reloadData];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - API

-(void)getRestoFromServer
{
    NSString* strID = self.data;
    [[ServerManager sharedManager]getRestoDetailWithID:strID onSuccess:^(id result) {
        resultDict = result;
        NSLog(@"%@",result);
        
        NSDictionary* response = (NSDictionary *)result;
        NSMutableArray* objectsArray = [NSMutableArray array];
        for (NSDictionary* dict in [response objectForKey:@"locations"]) {
            FLLocationModel* model = [[FLLocationModel alloc]initWithServerResponse:dict];
            [objectsArray addObject:model];
        }
        locations = [NSArray arrayWithArray:objectsArray];
        [self updateUI];
        [self.tableView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } withAcceptLanguage:_acceptLanguage onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}


-(void)updateUI{
    
    NSString* tempRestoOpen = [resultDict objectForKey:@"restaurant_open"];
    NSString* tempRestoClose = [resultDict objectForKey:@"restaurant_close"];
    restoOpen = [self dateWithJSONString:tempRestoOpen];
    restoClose = [self dateWithJSONString:tempRestoClose];
    
    NSString* tempKitchenOpen = [resultDict objectForKey:@"kitchen_open"];
    NSString* tempkitchenClose = [resultDict objectForKey:@"kitchen_close"];
    
    kitchenOpen = [self dateWithJSONString:tempKitchenOpen];
    kitchenClose = [self dateWithJSONString:tempkitchenClose];
    

//    
    NSNumber * isSuccessCard = (NSNumber *)[resultDict objectForKey: @"payment_cards"];
    if([isSuccessCard boolValue] == YES)
    {
        creditCard = [NSString stringWithFormat:@"Yes"];
        //    NSLog(@"%@", self.post.acceptPaymentCards);
    } else {
        creditCard = [NSString stringWithFormat:@"No"];
        //    NSLog(@"%@", self.post.acceptPaymentCards);
    }
    NSNumber * isSuccessParking = (NSNumber *)[resultDict objectForKey: @"parking"];
    if([isSuccessParking boolValue] == YES)
    {
        parking = [NSString stringWithFormat:@"Yes"];
        
    } else {
        parking = [NSString stringWithFormat:@"No"];
        
    }
    
}

- (NSString*)dateWithJSONString:(NSString*)dateStr
{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    
    [dateFormat setDateFormat:@"HH:mm"];
    
    NSString* dateStr2 = [dateFormat stringFromDate:date];
    
    NSLog(@"Date -- %@",dateStr2);
    
    return dateStr2;

}




#pragma mark - Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"FLMailViewViewController"]){
        FLMailViewViewController *controller = (FLMailViewViewController*)segue.destinationViewController;
        controller.email = sender;
    }
    
    if([segue.identifier isEqualToString:@"FLWebViewController"]){
        FLWebViewController *controller = (FLWebViewController*)segue.destinationViewController;
        controller.urlWeb = sender;
    }
    
    if([segue.identifier isEqualToString:@"FLMapViewController"]){
        FLMapViewController *controller = (FLMapViewController*)segue.destinationViewController;
       controller.dict = sender;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [locations count];

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *infoCell = @"InfoCell";
    static NSString *branchesCell  = @"BranchesCell";
    NSString* reuseIdentifier;
    if (indexPath.row == 0) {
        // Use a specific image.
  
        reuseIdentifier = infoCell;
         self.infoCell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
        if(! self.infoCell) {
             self.infoCell = [[FLInfoRestoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
        }
     //    [_setPriceLabel setText:NSLocalizedString(@"Set price", nil)];
        FLLocationModel* model = [locations objectAtIndex:indexPath.row];
        self.infoCell.delegate = self;
        
        self.infoCell.separatorInset = UIEdgeInsetsMake(0.f, 15, 0.f, 15.f);
        [self.infoCell.kitchenLabel1 setText:NSLocalizedString(@"Kitchen", nil)];
        [self.infoCell.kitchenLabel1 sizeToFit];
        NSString* kitchen = [resultDict objectForKey:@"cuisine"];
        CGFloat cellWidth =   self.infoCell.frame.size.width;
        CGFloat KitchenLabelWidth = self.infoCell.kitchenLabel1.frame.size.width;
        CGFloat dX = (cellWidth - KitchenLabelWidth);
        CGFloat dXofs = dX - 20;
        CGFloat heightKithLbl = [kitchen getHeightOfFont:[UIFont systemFontOfSize:14] widht:dXofs];
        self.infoCell.kitchenLabel.frame = CGRectMake(self.infoCell.kitchenLabel1.frame.size.width + 10, self.infoCell.kitchenLabel.frame.origin.y, dXofs, heightKithLbl);
        self.infoCell.kitchenLabel.text = kitchen;
        CGFloat kitchenY = CGRectGetMaxY(self.infoCell.kitchenLabel.frame);
        
        
        NSString* telephones = model.telephones;
        [self.infoCell.phoneNumberLabel1 setText:NSLocalizedString(@"Tel", nil)];
        [self.infoCell.phoneNumberLabel1 sizeToFit];
        CGFloat phoneLabel1Width = self.infoCell.phoneNumberLabel1.frame.size.width;
        CGFloat dXPhone = ((cellWidth - phoneLabel1Width)-10);
        CGFloat heightPhoneLbl = [telephones getHeightOfFont:[UIFont systemFontOfSize:13] widht:dXPhone];
        self.infoCell.phoneNumberLabel.frame = CGRectMake(self.infoCell.phoneNumberLabel1.frame.size.width +10, kitchenY + 8, dXPhone, heightPhoneLbl);
         self.infoCell.phoneNumberLabel1.frame = CGRectMake(self.infoCell.phoneNumberLabel1.frame.origin.x, kitchenY + 8, self.infoCell.phoneNumberLabel1.frame.size.width, self.infoCell.phoneNumberLabel1.frame.size.height);
        self.infoCell.phoneNumberLabel.text = telephones;
        CGFloat phoneY = CGRectGetMaxY(self.infoCell.phoneNumberLabel.frame);
        
        NSString* adress = model.address;
        [self.infoCell.adresLabel1 setText:NSLocalizedString(@"Adress", nil)];
        [self.infoCell.adresLabel1 sizeToFit];
        CGFloat adresLabel1Width = self.infoCell.adresLabel1.frame.size.width;
        CGFloat dxAdess = (cellWidth - adresLabel1Width);
        CGFloat heightAdressLbl = [adress getHeightOfFont:[UIFont systemFontOfSize:13] widht:dxAdess];
        self.infoCell.adresLabel.frame = CGRectMake(self.infoCell.adresLabel1.frame.size.width + 10, phoneY + 8, dxAdess, heightAdressLbl);
        self.infoCell.adresLabel1.frame = CGRectMake(self.infoCell.adresLabel1.frame.origin.x, phoneY + 8, self.infoCell.adresLabel1.frame.size.width, self.infoCell.adresLabel1.frame.size.height);
        self.infoCell.adresLabel.text = adress;
        CGFloat adressY = CGRectGetMaxY(self.infoCell.adresLabel.frame);
        
        NSString* clockRestoran  = [NSString stringWithFormat:@"%@-%@",restoOpen,restoClose];
        [self.infoCell.workClocksLabel1 setText:NSLocalizedString(@"Working hours", nil)];
        [self.infoCell.workClocksLabel1 sizeToFit];
        [self.infoCell.workClocksLabel sizeToFit];
        self.infoCell.workClocksLabel.frame = CGRectMake(self.infoCell.workClocksLabel1.frame.size.width + 10, adressY + 8, 100, self.infoCell.workClocksLabel.frame.size.height);
        self.infoCell.workClocksLabel1.frame = CGRectMake(self.infoCell.workClocksLabel1.frame.origin.x, adressY + 8, self.infoCell.workClocksLabel1.frame.size.width, self.infoCell.workClocksLabel1.frame.size.height);
        self.infoCell.workClocksLabel.text = clockRestoran;
        CGFloat restoWorkY = CGRectGetMaxY(self.infoCell.workClocksLabel.frame);
        
        NSString* clockKitchen  = [NSString stringWithFormat:@"%@-%@",kitchenOpen,kitchenClose];
        [self.infoCell.clocksKithenLabel1 setText:NSLocalizedString(@"Working Hours cuisine", nil)];
        [self.infoCell.clocksKithenLabel1 sizeToFit];
        [self.infoCell.clocksKithenLabel sizeToFit];
        self.infoCell.clocksKithenLabel.frame = CGRectMake(self.infoCell.clocksKithenLabel1.frame.size.width + 10, restoWorkY + 8, self.infoCell.clocksKithenLabel.frame.size.width, self.infoCell.clocksKithenLabel.frame.size.height);
        self.infoCell.clocksKithenLabel1.frame = CGRectMake(self.infoCell.clocksKithenLabel1.frame.origin.x, restoWorkY + 8, self.infoCell.clocksKithenLabel1.frame.size.width, self.infoCell.clocksKithenLabel1.frame.size.height);
        self.infoCell.clocksKithenLabel.text = clockKitchen;
        CGFloat kitchenWorkY = CGRectGetMaxY(self.infoCell.clocksKithenLabel.frame);
        
        self.infoCell.creditCardLabel.text = creditCard;
        [self.infoCell.creditCardLabel1 setText:NSLocalizedString(@"Credit Card", nil)];
        [self.infoCell.creditCardLabel1 sizeToFit];
        [self.infoCell.creditCardLabel sizeToFit];
        self.infoCell.creditCardLabel.frame = CGRectMake(self.infoCell.creditCardLabel1.frame.size.width + 10, kitchenWorkY + 8, self.infoCell.creditCardLabel.frame.size.width, self.infoCell.creditCardLabel.frame.size.height);
        self.infoCell.creditCardLabel1.frame = CGRectMake(self.infoCell.creditCardLabel1.frame.origin.x, kitchenWorkY + 8, self.infoCell.creditCardLabel1.frame.size.width, self.infoCell.creditCardLabel1.frame.size.height);
        CGFloat creditCardY = CGRectGetMaxY(self.infoCell.creditCardLabel.frame);
        
        
        [self.infoCell.parkingLabel1 setText:NSLocalizedString(@"Parking", nil)];
        [self.infoCell.parkingLabel1 sizeToFit];
        self.infoCell.parkingLabel.text = parking;
        [self.infoCell.parkingLabel sizeToFit];
        self.infoCell.parkingLabel.frame = CGRectMake(self.infoCell.parkingLabel1.frame.size.width + 10, creditCardY + 8, self.infoCell.parkingLabel.frame.size.width, self.infoCell.parkingLabel.frame.size.height);
        self.infoCell.parkingLabel1.frame = CGRectMake(self.infoCell.parkingLabel1.frame.origin.x, creditCardY + 8, self.infoCell.parkingLabel1.frame.size.width, self.infoCell.parkingLabel1.frame.size.height);
        CGFloat parkingY = CGRectGetMaxY(self.infoCell.parkingLabel.frame);
        
        
        [self.infoCell.buttonMap setTitle:NSLocalizedString(@"Show on map", nil) forState:UIControlStateNormal];
        self.infoCell.buttonMap.frame = CGRectMake(self.infoCell.buttonMap.frame.origin.x, parkingY + 20, self.tableView.frame.size.width, self.infoCell.buttonMap.frame.size.height);
        CGFloat mapY = CGRectGetMaxY(self.infoCell.buttonMap.frame);
        
        self.infoCell.buttonView.frame = CGRectMake(self.infoCell.buttonView.frame.origin.x, mapY + 20, self.infoCell.buttonView.frame.size.width, self.infoCell.buttonView.frame.size.height);

        return  self.infoCell;
    }else
        reuseIdentifier = branchesCell;
    self.branchCell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if(!self.branchCell) {
        self.branchCell = [[FLBranchesRestaurantCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    FLLocationModel* model = [locations objectAtIndex:indexPath.row];
    self.branchCell.delegate = self;
    self.infoCell.separatorInset = UIEdgeInsetsMake(0.f, 15, 0.f, 15.f);
    CGFloat nameY = CGRectGetMaxY(self.branchCell.name.frame);
    
    NSString* adress = model.address;
    [self.branchCell.adresLabel1 setText:NSLocalizedString(@"Adress", nil)];
    [self.branchCell.adresLabel1 sizeToFit];
    CGFloat cellWidth =   self.branchCell.frame.size.width;
    CGFloat adresLabel1Width = self.branchCell.adresLabel1.frame.size.width;
    CGFloat dxAdess = (cellWidth - adresLabel1Width);
    CGFloat heightAdressLbl = [adress getHeightOfFont:[UIFont systemFontOfSize:13] widht:dxAdess];
    self.branchCell.adresLabel.frame = CGRectMake(self.branchCell.adresLabel1.frame.size.width + 10, nameY + 8, dxAdess, heightAdressLbl);
    self.branchCell.adresLabel1.frame = CGRectMake(self.branchCell.adresLabel1.frame.origin.x, nameY + 8, self.branchCell.adresLabel1.frame.size.width, self.branchCell.adresLabel1.frame.size.height);
    self.branchCell.adresLabel.text = adress;
    CGFloat adressY = CGRectGetMaxY(self.branchCell.adresLabel.frame);
    
    
    NSString* telephones = model.telephones;
    [self.branchCell.phoneNumberLabel1 setText:NSLocalizedString(@"Tel", nil)];
    [self.branchCell.phoneNumberLabel1 sizeToFit];
    CGFloat phoneLabel1Width = self.branchCell.phoneNumberLabel1.frame.size.width;
    CGFloat dXPhone = ((cellWidth - phoneLabel1Width)-10);
    CGFloat heightPhoneLbl = [telephones getHeightOfFont:[UIFont systemFontOfSize:13] widht:dXPhone];
    self.branchCell.phoneNumberLabel.frame = CGRectMake(self.branchCell .phoneNumberLabel1.frame.size.width +10, adressY + 8, dXPhone, heightPhoneLbl);
    self.branchCell.phoneNumberLabel1.frame = CGRectMake(self.branchCell.phoneNumberLabel1.frame.origin.x, adressY + 8, self.branchCell.phoneNumberLabel1.frame.size.width, self.branchCell.phoneNumberLabel1.frame.size.height);
    self.branchCell.phoneNumberLabel.text = telephones;
    CGFloat mapY = CGRectGetMaxY(self.branchCell.phoneNumberLabel.frame);
    
    [self.branchCell.map setTitle:NSLocalizedString(@"Show on map", nil) forState:UIControlStateNormal];
    self.branchCell.map.frame = CGRectMake(self.infoCell.buttonMap.frame.origin.x, mapY + 20, self.tableView.frame.size.width, self.infoCell.buttonMap.frame.size.height);
    
    
    NSInteger variable = indexPath.row;
    NSString* strBranch = NSLocalizedString(@"Branch", nil);
    CGRect frameBranch = CGRectMake(0, 0, SCREEN_WIDTH, 33);
        self.branchCell.name.text = [NSString stringWithFormat:@"%@ №%ld",strBranch,(long)variable + 1];
    self.branchCell.name.frame = frameBranch;
    return self.branchCell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIFont* font = [UIFont systemFontOfSize:13];
    if (indexPath.row == 0) {
    FLLocationModel* model = [locations objectAtIndex:indexPath.row];
    NSString* adress = model.address;
    NSString* kitchen = [resultDict objectForKey:@"cuisine"];
    CGFloat tableViewWidth =   self.tableView.frame.size.width;
    CGFloat dX = (tableViewWidth - tableViewWidth/5);
    CGFloat heightKithLbl = [kitchen getHeightOfFont:font widht:dX];
    CGFloat heightAdrs = [adress getHeightOfFont:font widht:dX];
    CGFloat offset = 320;
    CGFloat heightCell = heightKithLbl + heightAdrs + offset;

    return  heightCell ;
    }
    FLLocationModel* model = [locations objectAtIndex:indexPath.row];
    NSString* adress = model.address;
    CGFloat heightAdrs = [adress getHeightOfFont:font widht:100];
    CGFloat brunchOffset = 140;
    CGFloat heightBrunchCell = heightAdrs + brunchOffset;
    return heightBrunchCell;
}



#pragma mark - FLInfoRestoCellDelegate 

-(void)showInsto:(FLInfoRestoCell*)cell{
        [self performSegueWithIdentifier:@"FLWebViewController" sender:[resultDict objectForKey:@"instagram"]];
}
-(void)showFaceBook:(FLInfoRestoCell*)cell{
        [self performSegueWithIdentifier:@"FLWebViewController" sender:[resultDict objectForKey:@"facebook"]];
}
-(void)showMap:(FLInfoRestoCell*)cell{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    FLLocationModel* data = [locations  objectAtIndex:indexPath.row];
    NSDictionary* dict = @{@"latitude":data.latitude, @"longitude":data.longitude,@"telephones":data.telephones, @"address":data.address};
     [self performSegueWithIdentifier:@"FLMapViewController" sender:dict];
}
-(void)sendEmail:(FLInfoRestoCell*)cell{
        [self performSegueWithIdentifier:@"FLMailViewViewController" sender:[resultDict objectForKey:@"email"]];
}
-(void)showWeb:(FLInfoRestoCell*)cell{
        [self performSegueWithIdentifier:@"FLWebViewController" sender:[resultDict objectForKey:@"website"]];
}

#pragma mark - FLBranchesRestaurantCellDelegate

-(void)showMapBranches:(FLBranchesRestaurantCell*)cell{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    FLLocationModel* data = [locations  objectAtIndex:indexPath.row];
    NSDictionary* dict = @{@"latitude":data.latitude, @"longitude":data.longitude,@"telephones":data.telephones, @"address":data.address};
    [self performSegueWithIdentifier:@"FLMapViewController" sender:dict];
}


@end
