//
//  FLMapViewController.m
//  FoodLook
//
//  Created by Yahin Rinat on 17.11.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import "FLMapViewController.h"
#import <MapKit/MapKit.h>
#import "Annotation.h"
#import <MBProgressHUD.h>

@interface FLMapViewController () <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView* mapView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView* indicator;
@end

@implementation FLMapViewController

#define METERS_PER_MILE 1609.344



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self locationsResto];
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
}

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"Swipe received.");
}


-(void)locationsResto {
    
    
    NSString *latstring = [_dict objectForKey:@"latitude"];
    NSString *lonstring = [_dict objectForKey:@"longitude"];
    NSLog(@"latstring: %@, lonstring: %@", latstring, lonstring);
    
    double latdouble = [latstring doubleValue];
    double londouble = [lonstring doubleValue];
    
    CLLocationCoordinate2D  coordinate;
    
    coordinate.latitude = latdouble;
    coordinate.longitude = londouble;
  
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 0.5* METERS_PER_MILE, 0.5*METERS_PER_MILE);
    Annotation* ann = [[Annotation alloc]init];
    ann.title = [_dict objectForKey:@"address"];
    ann.subtitle = [_dict objectForKey:@"telephone_numbers"];
    ann.coordinate = coordinate;
    [_mapView setRegion:viewRegion animated:YES];
    [self.mapView addAnnotation:ann];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
    self.mapView.delegate = nil;
    NSLog(@"MapView deallocate");
}

-(IBAction)cancel:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - MKMapViewDelegate


- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated {
    
}
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    
}

- (void)mapViewWillStartLoadingMap:(MKMapView *)mapView {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}
- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView {
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
- (void)mapViewDidFailLoadingMap:(MKMapView *)mapView withError:(NSError *)error {
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}


- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    
    
    // Add an annotation
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = userLocation.coordinate;
    point.title = @"Where am I?";
    point.subtitle = @"I'm here!!!";
    
    [self.mapView addAnnotation:point];
}
@end
