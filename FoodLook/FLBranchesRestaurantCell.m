//
//  FLBranchesRestaurantCell.m
//  FoodLook
//
//  Created by Rinat Yahin on 29.01.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLBranchesRestaurantCell.h"

@implementation FLBranchesRestaurantCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)showMapBranches:(id)sender {
    [self.delegate showMapBranches:self];
}

@end
