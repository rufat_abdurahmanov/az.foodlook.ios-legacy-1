//
//  FLLeftPanelWebViewController.h
//  FoodLook
//
//  Created by Rinat Yahin on 09.02.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLLeftPanelWebViewController : UIViewController
@property (strong, nonatomic) NSString* urlWeb;
@end
