//
//  FLWebViewController.h
//  FoodLook
//
//  Created by Yahin Rinat on 15.11.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLWebViewController : UIViewController

@property (strong, nonatomic) NSString* urlWeb;

@end
