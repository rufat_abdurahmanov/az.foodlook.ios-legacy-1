//
//  FLImageFoodCollectionViewCell.h
//  FoodLook
//
//  Created by Yahin Rinat on 07.08.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FLImageFoodCollectionViewCell;

@protocol FLImageFoodCollectionViewCell <NSObject>

@optional
-(void)likeButton:(FLImageFoodCollectionViewCell*)cell;

@end

@interface FLImageFoodCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) id<FLImageFoodCollectionViewCell> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *imageFood;
@property (weak, nonatomic) IBOutlet UIButton *btnLike;
@property (weak, nonatomic) IBOutlet UILabel *lblLikeCount;
@property (weak, nonatomic) IBOutlet UIImageView *placeholder;



@end
