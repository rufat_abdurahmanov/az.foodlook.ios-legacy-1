//
//  FLLeftPanelViewController.m
//  FoodLook
//
//  Created by Yahin Rinat on 22.08.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import "FLLeftPanelViewController.h"
#import "FLLeftPanelWebViewController.h"



@interface FLLeftPanelViewController ()

@property (strong, nonatomic) FLLeftPanelWebViewController* webVC;
@property (strong, nonatomic) NSString* strUrl;
@property (weak, nonatomic) IBOutlet UIButton* myAccBtn;
@property (weak, nonatomic) IBOutlet UIButton* forUsers;
@property (weak, nonatomic) IBOutlet UIButton* forResto;
@property (weak, nonatomic) IBOutlet UIButton* contact;
@property (weak, nonatomic) IBOutlet UILabel* recomendFrendLbl;

@end

@implementation FLLeftPanelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_myAccBtn setTitle:NSLocalizedString(@"My account", nil) forState:UIControlStateNormal];
    [_myAccBtn sizeToFit];
    [_forUsers setTitle:NSLocalizedString(@"For users", nil) forState:UIControlStateNormal];
    [_forResto setTitle:NSLocalizedString(@"For restaurants", nil) forState:UIControlStateNormal];
    [_contact setTitle:NSLocalizedString(@"Contacts", nil) forState:UIControlStateNormal];
    [_recomendFrendLbl setText:NSLocalizedString(@"Recommend to a friend", nil)];
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    
}
-(IBAction)accBtn:(id)sender {
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)shareFriend:(id)sender{
    
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showFB"]) {
        FLLeftPanelWebViewController* vc = (FLLeftPanelWebViewController*)segue.destinationViewController;
        self.strUrl = @"https://www.facebook.com/foodlookapp";
        vc.urlWeb = self.strUrl;
        
    }
    if ([segue.identifier isEqualToString:@"showInsto"]) {
        FLLeftPanelWebViewController* vc = (FLLeftPanelWebViewController*)segue.destinationViewController;
        self.strUrl = @"http://instagram.com/foodlook_az";
        vc.urlWeb = self.strUrl;
        
    }
}
@end
