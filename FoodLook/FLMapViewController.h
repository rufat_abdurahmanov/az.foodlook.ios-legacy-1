//
//  FLMapViewController.h
//  FoodLook
//
//  Created by Yahin Rinat on 17.11.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FLMapViewController : UIViewController

@property (strong, nonatomic) NSDictionary* dict;

@end
