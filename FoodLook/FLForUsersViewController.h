//
//  FLForUsersViewController.h
//  FoodLook
//
//  Created by Rinat Yahin on 02.02.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWRevealViewController.h>
@interface FLForUsersViewController : UIViewController <SWRevealViewControllerDelegate>

@end
