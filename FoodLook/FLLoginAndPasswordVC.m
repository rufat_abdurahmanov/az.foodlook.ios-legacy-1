//
//  FLLoginVCViewController.m
//  FoodLook
//
//  Created by Rinat Yahin on 20.12.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import "FLLoginAndPasswordVC.h"
#import "ServerManager.h"
#import "FLGeneralViewController.h"
#import "FLAppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import <MBProgressHUD.h>
#import "CredentialStore.h"
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>
@interface FLLoginAndPasswordVC () <UITextFieldDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *paswordTextField;
@property (strong, nonatomic) UIAlertView* alertViewToken;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUP;
@property (weak, nonatomic) IBOutlet UIButton *btnLogIn;
@property (nonatomic, strong) CredentialStore *credentialStore;

@end

static NSString* kSettingsToken = @"token";
static NSString* kSettingsTokenDate = @"tokenDate";

@implementation FLLoginAndPasswordVC
{
    NSString* dateStrToken;
    NSDate* receiptDate;
    NSDate* currentDate;
    MBProgressHUD* hud;
    Reachability *internetReachableFoo;

}

#pragma mark - ViewController Life Cycle


- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        currentDate = [NSDate new];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.loginTextField becomeFirstResponder];
    
}
- (void)testInternetConnection
{
    
    internetReachableFoo = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    // Internet is reachable
    internetReachableFoo.reachableBlock = ^(Reachability*reach)
    {
        // Update the UI on the main thread
        
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    };
    
    // Internet is not reachable
    internetReachableFoo.unreachableBlock = ^(Reachability*reach)
    {
        // Update the UI on the main thread
     __weak typeof(self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            __strong typeof(self) strongSelf = weakSelf;
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Somting went wrong..."
                                                            message:@"Please check internet connection"
                                                           delegate:nil
                                                  cancelButtonTitle:@"ok"
                                                  otherButtonTitles:nil];
            [alert show];

            [strongSelf performSelector:@selector(exitApp) withObject:nil afterDelay:4];
        });
    };
    
    [internetReachableFoo startNotifier];
}

-(void)exitApp{
    UIApplication *app = [UIApplication sharedApplication];
    [app performSelector:@selector(suspend)];
    
    //wait 2 seconds while app is going background
    [NSThread sleepForTimeInterval:2.0];
    
    //exit app when app is in background
    exit(0);
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self testInternetConnection];
    self.alertViewToken.delegate = self;
    self.alertViewToken.tag = 1;
    self.credentialStore = [[CredentialStore alloc]init];
    [self settingsViewController];
}

-(NSString*)dateExpiresIndToken:(NSString*)strDate {
    NSInteger  expiresIn = [strDate integerValue];
    NSDate* tokenDate = [NSDate dateWithTimeIntervalSinceNow:expiresIn];
    NSLog(@"%@",tokenDate);
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *tokenDateStr = [formatter stringFromDate:tokenDate];

    return tokenDateStr;
}


-(IBAction)loggedIn:(id)sender {
    
   hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[ServerManager sharedManager]authorizeExistingUserWithLogin:self.loginTextField.text andPassword:self.paswordTextField.text withOnSuccess:^(id results) {
        NSLog(@"%@",results);
        NSString* strToken = [results objectForKey:@"access_token"];
        NSString* strRefreshToken = [results objectForKey:@"refresh_token"];
        NSString* strTokenDate = [results objectForKey:@"expires_in"];
        dateStrToken = [self dateExpiresIndToken:strTokenDate];
        [self.credentialStore setAuthToken:strToken];
        [self.credentialStore setAuthRefreshToken:strRefreshToken];
        [self.credentialStore setDateToken:dateStrToken];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"tokenGet" object:self];
        [self dismissViewControllerAnimated:YES completion:nil];
        
    } onFailure:^(NSError *error, NSInteger statusCode, NSString* responseString) {
        NSLog(@"error -  %@, code - %ld",error,(long)statusCode);
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        if (statusCode == 500) {
            hud.mode = MBProgressHUDModeText;
            hud.labelText = @"Something went wrong...";
            [hud hide:YES afterDelay:3];
        }else {
            NSData* jsonData = [responseString  dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
            NSString* errorMessage = [json objectForKey:@"error"];
            hud.mode = MBProgressHUDModeText;
            hud.labelText = @"Неверный логин или пароль";
            [hud hide:YES afterDelay:3];
        }
    }];
     [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}



-(IBAction)signUP:(id)sender {
    
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if ([self.paswordTextField.text length] > 2
        && [self.loginTextField.text length] > 2) {


    [[ServerManager sharedManager] getTempTokenFromServerWithOnSuccess:^(id results) {
        NSLog(@"%@",results);
        
        [[ServerManager sharedManager]
         registerNewUserInServerWithLogin:self.loginTextField.text
         andPassword:self.paswordTextField.text
         withOnSuccess:^(NSDictionary* results) {
             NSLog(@"%@",results);
             
             NSArray* temparray = @[@"User with this Username already exists."];
             NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:temparray,@"username", nil];
             
             NSArray *array1=@[results];
             NSArray *array2=@[dict];
             
             array1=[array1 sortedArrayUsingSelector:@selector(compare:)];
             array2=[array2 sortedArrayUsingSelector:@selector(compare:)];
             
             if ([array1 isEqualToArray:array2]) {
                 NSLog(@"both are same");
                 hud.mode = MBProgressHUDModeText;
                 hud.labelText = @"Пользователь с таким именем" ;
                 hud.detailsLabelText = @"существует";
                [hud hide:YES afterDelay:3];
             }else {
             
             NSString* username = results[@"username"];
             NSString* password = results[@"password"];
             [self.credentialStore setUsername:username];
             [self.credentialStore setPassword:password];
             [self loggedIn:self];
             }
         } onFailure:^(NSError *error, NSInteger statusCode) {
             NSLog(@"%@",error);
         }];
        
    } onFailure:^(NSError *error, NSInteger statusCode) {
      
    }];
        
    }else {
        hud.mode = MBProgressHUDModeText;
        hud.labelText = @"Введите логин или пароль";
        [hud hide:YES afterDelay:3];
    }
//     [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)buttonSettings {
    self.btnSignUP.layer.cornerRadius = 10;
    self.btnLogIn.layer.cornerRadius = 10;
    self.btnSignUP.clipsToBounds = YES;
    self.btnLogIn.clipsToBounds = YES;
    self.btnSignUP.layer.borderColor=[[UIColor redColor] CGColor];
    self.btnSignUP.layer.borderColor=[[UIColor redColor] CGColor];
    
}

-(void)settingsViewController {
    
    [self buttonSettings];
    
    [self.navigationController.navigationBar setHidden:YES];
    
    [self.paswordTextField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.loginTextField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    [self.btnSignUP setTitle:NSLocalizedString(@"Sign up", nil) forState:UIControlStateNormal];
    [self.btnLogIn setTitle:NSLocalizedString(@"Log In", nil) forState:UIControlStateNormal];
    NSString* strLogin =  NSLocalizedString(@"Choose login", nil);
    NSString* strPassword = NSLocalizedString(@"Choose Password", nil);
    
    UIColor *color = [UIColor whiteColor];
    _loginTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:strLogin attributes:@{NSForegroundColorAttributeName: color}];
    _paswordTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:strPassword attributes:@{NSForegroundColorAttributeName: color}];

}




-(NSString*)yesButtonClicked {
    return @"Ok";
}

-(void) allertViewRegistrTruble {
    self.alertViewToken = [[UIAlertView alloc] initWithTitle:@"Sorry!"
                                                     message:@"This username is already in use. Please choose another one"
                                                    delegate:nil
                                           cancelButtonTitle:nil
                                           otherButtonTitles:[self yesButtonClicked], nil];
    [self.alertViewToken show];
    
    
}

-(void) allertViewServerTruble {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!"
                                                    message:@"Server is not available at this time please try again later or visit"
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"Ok",nil];
    [alert show];
    
    
}
-(void) allertView {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"oops!"
                                                    message:@"Login and Password must have more than 4 characters"
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:[self yesButtonClicked], nil];
    [alert show];
}

-(void) allertViewWrongPassword {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"oops!"
                                                    message:@"Login and Password were entered incorrectly"
                                                   delegate:nil
                                          cancelButtonTitle:nil
                                          otherButtonTitles:[self yesButtonClicked], nil];
    [alert show];
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    if ([self.loginTextField.text length] <= 0 || [self.loginTextField.text length] < 4 ||[self.loginTextField.text length] > 23 ) {
        
        [self allertView];
        return NO;
    }
    
    return YES;
    if ([self.paswordTextField.text length] <= 0 || [self.paswordTextField.text length] < 4) {
        [self allertView];
        return NO;
    }
    
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([textField isEqual:self.paswordTextField]) {
        
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if ([textField isEqual:self.loginTextField] &&
        [self.loginTextField.text length] <= 0 | [self.loginTextField.text length] < 3 | [self.loginTextField.text length] > 23) {
        [self allertView];
        return NO;
    }
    [self.paswordTextField becomeFirstResponder];
    
    if ([textField isEqual:self.paswordTextField] &&
        [self.paswordTextField.text length] <= 0 | [self.paswordTextField.text length] < 3) {
        
        [self allertView];
        return NO;
    }
    
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:self.paswordTextField]) {
        if ([textField.text length] > 2) {
        }
        if ([textField.text length] < 2) {
            
        }
        
    }
    
    return YES;
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != 0)  // 0 == the cancel button
    {
        //home button press programmatically

    }
}

@end
