//
//  FLVegeterianDish.m
//  FoodLook
//
//  Created by Yahin Rinat on 04.12.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import "FLVegeterianDish.h"

@implementation FLVegeterianDish

- (id) initWithServerResponse:(NSDictionary*) responseObject
{
    self = [super init];
    if (self) {
        
        
        
        NSString* idDish = [[responseObject objectForKey:@"id"]stringValue];
        
        if (idDish) {
            self.idDish = idDish;
            
        }
        
//                NSString* created = [responseObject objectForKey:@"created"];
//        
//                if (created) {
//                    self.created = created;
//        
//                }
//                NSString* lastUpdated = [responseObject objectForKey:@"lastUpdated"];
//        
//                if (lastUpdated) {
//                    self.lastUpdated = lastUpdated;
//        
//                }
        
        NSString* cost = [responseObject objectForKey:@"price"];
        
        if (cost) {
            self.cost = cost;
            
        }
        
        NSString* label = [responseObject objectForKey:@"label"];
        
        if (label) {
            self.label = label;
            
        }
        
        NSString* urlString = [responseObject objectForKey:@"image"];
        if (urlString) {
            NSString *imageEscapedURL = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            self.image_URL = imageEscapedURL;
        }
        
        
        //   NSString* likeQuantity = [responseObject objectForKey:@"like_quantity"];
        NSNumber *likeQuantity = [NSNumber numberWithInt:[[responseObject valueForKey:@"like_quantity"] intValue]];
        
        if (label) {
            self.like_quantity = likeQuantity;
            
        }
        
        NSMutableDictionary* social = [NSMutableDictionary dictionaryWithDictionary:[responseObject objectForKey:@"social"]];
        if (social) {
            //     NSLog(@"%@",urlString);
            self.social = social;
        }
        
    }
    return self;
}

@end
