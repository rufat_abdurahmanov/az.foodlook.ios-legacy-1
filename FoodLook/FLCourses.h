//
//  FLCourses.h
//  FoodLook
//
//  Created by Yahin Rinat on 29.09.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FLCourses : NSObject

- (id) initWithServerResponse:(NSDictionary*) responseObject;


@property (strong, nonatomic) NSString* idDish;
@property (strong, nonatomic) NSString* label;
@property (strong, nonatomic) NSString* cost;
@property (strong, nonatomic) NSString* image_URL;
@property (strong, nonatomic) NSString* imgDishDetail;
@property (strong, nonatomic) NSString* ingredients;
@property (strong, nonatomic) NSMutableDictionary* social;



@end
