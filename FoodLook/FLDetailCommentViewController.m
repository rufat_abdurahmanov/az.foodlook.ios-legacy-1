//
//  FLDetailCommentViewController.m
//  FoodLook
//
//  Created by Rinat Yahin on 14/01/15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLDetailCommentViewController.h"
#import "FLCommentViewController.h"
#import "ServerManager.h"

@interface FLDetailCommentViewController () <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) FLCommentViewController* vc;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@end

@implementation FLDetailCommentViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_backBtn setTitle:NSLocalizedString(@"Feedback", nil) forState:UIControlStateNormal];
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self.textView becomeFirstResponder];
    NSLog(@"%@",[self.dict objectForKey:@"text"]);
    self.textView.text = [self.dict objectForKey:@"text"];
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
}

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"Swipe received.");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeModalView:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];

}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        NSString* str = [NSString stringWithFormat:@"%@", [self.dict objectForKey:@"id"]];
        [self editCommentInServerWithIDcomment:str andWithTextComment:textView.text];
   
        return NO;
    }
 
    return YES;
}

#pragma mark - API 

-(void)editCommentInServerWithIDcomment:(NSString*)idComment andWithTextComment:(NSString*)strText{
    
    
    
    [[ServerManager sharedManager]editCommentInServerWithID:idComment
                                         andWithTextComment:strText
                                              withOnSuccess:^(id result) {

                                              } onFailure:^(NSError *error, NSInteger statusCode) {
                                                  
                                              }];
}

@end
