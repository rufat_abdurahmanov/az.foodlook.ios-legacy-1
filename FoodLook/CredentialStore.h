//
//  CredentialStore.h
//  FoodLook
//
//  Created by Rinat Yahin on 14/04/15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CredentialStore : NSObject


-(BOOL)isLoggedIn;
-(void)clearSavedCredential;
-(NSString*)authToken;
-(NSString*)password;
-(NSString*)username;
-(NSString*)dateToken;
-(NSString*)refrashToken;
-(void)setDateToken:(NSString*)dateToken;
-(void)setPassword:(NSString*)password;
-(void)setUsername:(NSString*)username;
-(void)setAuthToken:(NSString*)authToken;
-(void)setAuthRefreshToken:(NSString*)refreshToken;

@end
