//
//  FLCourses.m
//  FoodLook
//
//  Created by Yahin Rinat on 29.09.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import "FLCourses.h"

@implementation FLCourses

- (id) initWithServerResponse:(NSDictionary*) responseObject
{
    self = [super init];
    if (self) {
        
        
        
        NSString* idDish = [[responseObject objectForKey:@"id"]stringValue];
        
        if (idDish) {
            self.idDish = idDish;
            
        }
        
        
        NSString* cost = [responseObject objectForKey:@"price"];
        
        if (cost) {
            self.cost = cost;
            
        }
        
        NSString* label = [responseObject objectForKey:@"label"];
        
        if (label) {
            self.label = label;
            
        }
        
        NSString* urlString = [responseObject objectForKey:@"image"];
        if (urlString) {
       //     NSLog(@"%@",urlString);
            self.image_URL = urlString;
        }
        
        NSMutableDictionary* social = [NSMutableDictionary dictionaryWithDictionary:[responseObject objectForKey:@"social"]];
        if (social) {
            self.social = social;
        }
        
    }
    return self;
}

- (BOOL)isEqual:(id)object {
    if (![object isKindOfClass:[FLCourses class]]) {
        return NO;
    }
    
    FLCourses *other = (FLCourses *)object;
    return other.idDish == self.idDish;
}

@end
