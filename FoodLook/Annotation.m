//
//  Annotation.m
//  FoodLook
//
//  Created by Yahin Rinat on 19.11.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import "Annotation.h"

@interface Annotation ()

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, assign) CLLocationCoordinate2D theCoordinate;


@end


@implementation Annotation 

- (id)initWithName:(NSString*)name address:(NSString*)address coordinate:(CLLocationCoordinate2D)coordinate {
    if ((self = [super init])) {
        if ([name isKindOfClass:[NSString class]]) {
            self.name = name;
        } else {
            self.name = @"Unknown charge";
        }
        self.address = address;
        self.theCoordinate = coordinate;
    }
    return self;
}

@end
