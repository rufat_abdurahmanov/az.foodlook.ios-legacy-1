//
//  ServerManager.h
//  NapoleonTest
//
//  Created by Yahin Rinat on 20.06.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import <Foundation/Foundation.h>


@class User;

@interface ServerManager : NSObject


@property (strong, nonatomic, readonly) User* currentUser;
@property (strong, nonatomic)  NSString* correctToken;
@property (strong, nonatomic) NSString* username;
@property (strong, nonatomic) NSString* password;


+(ServerManager*) sharedManager;


-(void)getCoursesFromServerWithCurrentPage:(NSInteger)page
                        withAcceptLanguage:(NSString*)language
                                 OnSuccess:(void(^)(id  results)) success
                                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

-(void)getCoursesDetailWithID:(NSString *)idCourse
           withAcceptLanguage:(NSString*)language
                    onSuccess:(void (^)(id result))success
                    onFailure:(void (^)(NSError* error, NSInteger statusCode))failure;

-(void)getRestorauntFromServerWithCurrentPage:(NSInteger)currentPage
                           withAcceptLanguage:(NSString*)language
                                    OnSuccess:(void (^)(id result))success
                                    onFailure:(void (^)(NSError * error, NSInteger statusCode))failure;

-(void)getRestoDetailWithID:(NSString *)idResto
                  onSuccess:(void (^)(id  result))success
         withAcceptLanguage:(NSString*)language
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;


- (void) getVegeterianCoursesFromServerOnSuccess:(void(^)(NSArray* results)) success
                              withAcceptLanguage:(NSString*)language
                                       onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

-(void)getRestorauntCategoriesWithRestoID:(NSString*)idResto andWithIDCategories:(NSString*)idCategories
                       withAcceptLanguage:(NSString*)language
                                onSuccess:(void (^)(id  results))success
                                onFailure:(void (^)(NSError *error, NSInteger statusCode))failure;

-(void)getStarredRestorauntFromServerOnSuccess:(void (^)(id result))success
                                     onFailure:(void (^)(NSError * error, NSInteger statusCode))failure;

#pragma mark - Authorization

-(void)loadLoginAndPasswordFromNSUSerDefaults;

-(void)getTempTokenFromServerWithOnSuccess:(void(^)(id results)) success
                                onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

-(void)registerNewUserInServerWithLogin:(NSString*)login andPassword:(NSString*)password
                           withOnSuccess:(void(^)(NSDictionary* results)) success
                               onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)authorizeExistingUserWithLogin:(NSString*)login
                           andPassword:(NSString*)password 
                         withOnSuccess:(void(^)(id results)) success
                             onFailure:(void(^)(NSError* error, NSInteger statusCode, NSString* responseString)) failure;

- (void)getRefreshTokenWithOldToken:(NSString*)oldToken withOnSuccess:(void(^)(id results)) success
                          onFailure:(void(^)(NSError* error)) failure;

#pragma mark - Comments

-(void)getCommentWithID:(NSString *)idEntity
              onSuccess:(void (^)(NSArray* result))success
              onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

-(void)postCommentInServerWithID:(NSString*)idEntity andTextComment:(NSString*)comment
             withOnSuccess:(void(^)(id results)) success
                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

-(void)deleteCommentInServerWithID:(NSString*)idComment
                     withOnSuccess:(void(^)(id results)) success
                         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

-(void)editCommentInServerWithID:(NSString *)idComment andWithTextComment:(NSString*)textComment
                   withOnSuccess:(void (^)(id result))success
                       onFailure:(void (^)(NSError* error, NSInteger statusCode))failure;

#pragma mark - Like
-(void)postLikeInServerWithID:(NSString *)idEntity
                   withOnSuccess:(void (^)(id results))success
                       onFailure:(void (^)(NSError*  error, NSInteger statusCode))failure;

-(void)deleteLikeInServerWithID:(NSString *)idEntity
                  withOnSuccess:(void (^)(id results))success
                      onFailure:(void (^)(NSError*  error, NSInteger statusCode))failure;

#pragma mark - Share

-(void)postShareInServerWithID:(NSString *)idEntity
                 withOnSuccess:(void (^)(id results))success
                     onFailure:(void (^)(NSError*  error, NSInteger statusCode))failure;


#pragma mark - Average Resto Bild

-(void)getAveragePriceDishFromServerWithFrommPrice:(NSInteger)maxPrice
                                withAcceptLanguage:(NSString*)language
                                      andOnSuccess:(void(^)(id results)) success
                                         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

-(void)getAverageRestoFromServerWithFrommPrice:(NSInteger)maxPrice
                                  andOnSuccess:(void(^)(id results)) success
                                     onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

#pragma mark - Search methods

-(void)searchDishInServerWithText:(NSString*)text
                        onSuccess:(void(^)(id results)) success
               withAcceptLanguage:(NSString*)language
                        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

-(void)searchRestoInServerWithText:(NSString*)text
                         onSuccess:(void(^)(id results)) success
                         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

#pragma mark - User Settings

- (void)setAuthTokenHeader;

-(void)changeOldPassword:(NSString*)oldPassword
         withNewPassword:(NSString*)newPassword
               onSuccess:(void(^)(id results)) success
      withAcceptLanguage:(NSString*)language
               onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;


@end
