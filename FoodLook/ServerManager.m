//
//  ServerManager.m
//  NapoleonTest
//
//  Created by Yahin Rinat on 20.06.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import "ServerManager.h"
#import "AFNetworking.h"
#import "FLCourses.h"
#import "FLResto.h"
#import "AFHTTPRequestOperationManager.h"
#import "FLVegeterianDish.h"
#import "FLStarredResto.h"
#import "FLCommentModel.h"
#import "FLAverageCourses.h"
#import "FLAverageResto.h"
#import "CredentialStore.h"
#define FOODLOOK_API @"http://foodlook.webfactional.com"

@interface ServerManager () <NSURLConnectionDelegate> 


@property (strong, nonatomic) AFHTTPRequestOperationManager* requestOperationManager;
@property (strong, nonatomic) AFJSONRequestSerializer* requestSerializer;

@property (strong, nonatomic) NSString* tempToken;
//@property (strong, nonatomic) NSString* token;

@end

static NSString* kSettingsLogin = @"loginUser";
static NSString* kSettingsPassword = @"passwordUser";

@implementation ServerManager
@synthesize correctToken;
- (instancetype)init
{
    self = [super init];
    if (self) {
            [self setAuthTokenHeader];
   
    }
    return self;
}


- (void)setAuthTokenHeader {
    CredentialStore *store = [[CredentialStore alloc] init];
    NSString *authToken = [store authToken];
    NSLog(@"%@",authToken);
    [self setCorrectToken:authToken];
}



+(ServerManager*) sharedManager {
    
    static ServerManager* manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ServerManager alloc]init];
    });
    return manager;
}

//-(NSString *)correctToken{
//    correctToken = self.token;
//    return correctToken;
//}

#pragma mark - Authorization

-(void)loadLoginAndPasswordFromNSUSerDefaults{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    [ServerManager sharedManager].password = [userDefaults objectForKey:kSettingsPassword];
    [ServerManager sharedManager].username= [userDefaults objectForKey:kSettingsLogin];
}

- (void) getTempTokenFromServerWithOnSuccess:(void(^)(id results)) success
                               onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

{
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"4b266b219656494cb6e4d69b4df4b8ff",@"client_id",
                            @"1029a64a7a98484bb71e6baae2dc8a3a",@"client_secret",
                            @"client_credentials",@"grant_type",   nil];
    
 
     AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
     
     [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
     
     [manager POST:@"/api/oauth2/token/" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
     // NSLog(@"JSON: %@", responseObject);
     [operation start];
     _tempToken = [responseObject objectForKey:@"access_token"];
     
     success(responseObject);
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
     NSLog(@"Error: %@", error);
     NSError* e;
     NSInteger statusCode;
     failure(e,statusCode);
     }];

}

- (void)registerNewUserInServerWithLogin:(NSString*)login
                             andPassword:(NSString*)password withOnSuccess:(void(^)(NSDictionary* results)) success
                               onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{

    NSError *error;
    NSDictionary *params = @{ @"username" : login, @"password" : password };
    NSData* jsonD = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&error];
    NSString* strJs = [[NSString alloc]initWithData:jsonD encoding:NSUTF8StringEncoding];
    
    NSString* urlString = @"https://www.foodlook.az/api/users/register/";
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setValue: [NSString stringWithFormat:@"Bearer %@", _tempToken] forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"POST"];
    //[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[strJs dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLResponse *res;
    
    NSData *resp = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&res error:
                    &error];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)res;
    NSInteger statusCode;
    statusCode = [httpResponse statusCode];
    NSString* str = [[NSString alloc] initWithData:resp encoding:NSUTF8StringEncoding];
    NSData *jsonData = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSError *e;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
    NSLog(@"%@",dict);
    //    self.token = [dict objectForKey:@"access_token"];
    //    NSLog(@"Temp Token %@",self.token);
    success(dict);
    
    
    
    /*
    NSDictionary *params = @{ @"username" : login, @"password" : password };
    NSError *error;
    NSData* jsonD = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&error];
    NSString* strJs = [[NSString alloc]initWithData:jsonD encoding:NSUTF8StringEncoding];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager.requestSerializer setValue:@"application/json"
                     forHTTPHeaderField:@"Content-Type"];
    
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", _tempToken]
                     forHTTPHeaderField:@"Authorization"];
    [manager POST:@"/api/users/register/"
       parameters:strJs
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              
        NSLog(@"JSON: %@", responseObject);
        [operation start];
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSData* jsonData = [operation.responseString  dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
        NSString* errorMessage = [json objectForKey:@"error"];
        NSLog(@"%@",errorMessage);
        NSLog(@"%@",[error description]);
    }];
        */
}

- (void)authorizeExistingUserWithLogin:(NSString*)login andPassword:(NSString*)password
             withOnSuccess:(void(^)(id results)) success
                 onFailure:(void(^)(NSError* error, NSInteger statusCode,NSString* responseString)) failure{
    
    
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
            @"05a64e606e4b4dd4baac91a0cd6992dd",@"client_id",
            @"c53bde5eefcf4c1592821ecd05175f97",@"client_secret",
                                             @"password",@"grant_type",
                                             login, @"username",
                                             password, @"password",nil];


    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded;charset=utf-8"
                     forHTTPHeaderField:@"Content-Type"];
    [manager POST:@"/api/oauth2/token/" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSLog(@"JSON: %@", responseObject);
        [operation start];
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failure) {
            failure(error, operation.response.statusCode,operation.responseString );
        }
        
    }];
    
}

- (void)getRefreshTokenWithOldToken:(NSString*)oldToken withOnSuccess:(void(^)(id results)) success
                             onFailure:(void(^)(NSError* error)) failure{
    
    
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"05a64e606e4b4dd4baac91a0cd6992dd",@"client_id",
                            @"c53bde5eefcf4c1592821ecd05175f97",@"client_secret",
                            @"refresh_token",@"grant_type",
                            oldToken, @"refresh_token",nil];
    
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded;charset=utf-8"
                     forHTTPHeaderField:@"Content-Type"];
    [manager POST:@"/api/oauth2/token/"
       parameters:params
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"refresh token: %@", responseObject);
        [operation start];
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        failure(error);
    }];
    
}

#pragma mark - GetDishes

-(void)getCoursesFromServerWithCurrentPage:(NSInteger)page
                            withAcceptLanguage:(NSString*)language
                            OnSuccess:(void(^)(id  results)) success
                            onFailure:(void(^)(NSError* error, NSInteger
                                               statusCode)) failure
{
  
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    //https://www.foodlook.az/api/courses/?page=2
    NSString* strReq = [NSString stringWithFormat:@"api/courses/?page=%ld",(long)page];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken]
                     forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:language forHTTPHeaderField:@"Accept-Language"];
    [manager GET:strReq
     parameters:nil
     success:^(AFHTTPRequestOperation *operation, id responseObject) {
//          NSLog(@"JSON: %@", responseObject);
             success(responseObject);
            [operation start];
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         
         if (failure) {
             failure(error, operation.response.statusCode);
         }
         
     }];

    
}




- (void) getVegeterianCoursesFromServerOnSuccess:(void(^)(NSArray* results)) success
                             withAcceptLanguage:(NSString*)language
                             onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:language forHTTPHeaderField:@"Accept-Language"];
    [manager
     GET:@"/api/courses/vegetarian/"
     parameters:nil
     success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
         
         if (success) {
             NSDictionary* courses = (NSDictionary *)responseObject;
          
             NSMutableArray* objectsArray = [NSMutableArray array];
             
             
             for (NSDictionary* dict in [courses objectForKey:@"results"]) {
                 
                 FLVegeterianDish* courses = [[FLVegeterianDish alloc]initWithServerResponse:dict];
                 [objectsArray addObject:courses];

             }
             success(objectsArray);
         }
         [operation start];
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         
         if (failure) {
             failure(error, operation.response.statusCode);
         }
         
     }];
    
    
}



-(void)getCoursesDetailWithID:(NSString *)idCourse
           withAcceptLanguage:(NSString*)language
                    onSuccess:(void (^)(id result))success
                    onFailure:(void (^)(NSError * error, NSInteger statusCode))failure
{

    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    NSString* str = [NSString stringWithFormat:@"/api/courses/%@/",idCourse];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:language forHTTPHeaderField:@"Accept-Language"];
    [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Accept"];
    [manager GET:str parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
          NSLog(@" CoursesDetail JSON: %@", responseObject);
          [operation start];
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
           NSLog(@"Error: %@", error);
    }];

    
}



#pragma mark - GetResto

-(void)getRestorauntFromServerWithCurrentPage:(NSInteger)currentPage
                           withAcceptLanguage:(NSString*)language
                                    OnSuccess:(void (^)(id result))success
                                    onFailure:(void (^)(NSError * error, NSInteger statusCode))failure
{
    
    
        AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
         NSString* strReq = [NSString stringWithFormat:@"api/restaurants/?page=%ld",(long)currentPage];
        [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
        [manager.requestSerializer setValue:language forHTTPHeaderField:@"Accept-Language"];
        [manager
         GET:strReq
         parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
//             NSLog(@"JSON: %@", responseObject);
             success(responseObject);
             [operation start];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             
         }];
    
}

//-(void)getStarredRestorauntFromServerOnSuccess:(void (^)(NSArray * results))success
//                              onFailure:(void (^)(NSError* error, NSInteger statusCode))failure
//{
//    
//    
//    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
//    
//    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
//    
//    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
//    [manager
//     
//     GET:@"/api/restaurants/promoted"
//     parameters:nil
//     success:^(AFHTTPRequestOperation *operation, id responseObject) {
//              NSLog(@"JSON: %@", responseObject);
//         
//         if (success) {
//             NSDictionary* courses = (NSDictionary *)responseObject;
//             
//             NSMutableArray* objectsArray = [NSMutableArray array];
//             //         NSLog(@"%@",responseObject);
//             
//             for (NSDictionary* dict in [courses objectForKey:@"results"]) {
//                 // NSLog(@"%@",[dict allKeys]);
//                 FLStarredResto* resto = [[FLStarredResto alloc]initWithServerResponse:dict];
//                 [objectsArray addObject:resto];
//             }
//             success(objectsArray);
//         }
//         [operation start];
//     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//         NSLog(@"Error: %@", error);
//         
//         
//     }];
//}

-(void)getStarredRestorauntFromServerOnSuccess:(void (^)(id result))success
                                    onFailure:(void (^)(NSError * error, NSInteger statusCode))failure
{
    
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
    [manager
     
     GET:@"/api/restaurants/promoted/"
     parameters:nil
     success:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSLog(@"JSON: %@", responseObject);
         success(responseObject);
         [operation start];
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
     }];
    
}


-(void)getRestoDetailWithID:(NSString *)idResto
                    onSuccess:(void (^)(id  result))success
                      withAcceptLanguage:(NSString*)language
                      onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure
{

    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    NSString* str = [NSString stringWithFormat:@"/api/restaurants/%@/",idResto];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:language forHTTPHeaderField:@"Accept-Language"];
    [manager GET:str parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
        [operation start];
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];

}

-(void)getRestorauntCategoriesWithRestoID:(NSString*)idResto andWithIDCategories:(NSString*)idCategories
                       withAcceptLanguage:(NSString*)language
                                onSuccess:(void (^)(id  results))success
                                onFailure:(void (^)(NSError *error, NSInteger statusCode))failure
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    NSString* str = [NSString stringWithFormat:@"/api/restaurants/%@/categories/%@/",idResto, idCategories];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Accept"];
    [manager.requestSerializer setValue:language forHTTPHeaderField:@"Accept-Language"];
    [manager GET:str parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSLog(@"JSON: %@", responseObject);
        [operation start];
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}


#pragma mark Comments

-(void)getCommentWithID:(NSString *)idEntity
                  onSuccess:(void (^)(NSArray*  result))success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    NSString* str = [NSString stringWithFormat:@"/api/comments/list/%@/",idEntity];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Accept"];
    [manager GET:str parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (success) {
            NSDictionary* courses = (NSDictionary *)responseObject;
            NSMutableArray* objectsArray = [NSMutableArray array];
            for (NSDictionary* dict in [courses objectForKey:@"results"]) {
                
                FLCommentModel* courses = [[FLCommentModel alloc]initWithServerResponse:dict];
                [objectsArray addObject:courses];
                
            }
            
            
            success(objectsArray);
        }
        [operation start];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}




-(void)postCommentInServerWithID:(NSString *)idEntity
                  andTextComment:(NSString *)comment
                   withOnSuccess:(void (^)(id))success
                       onFailure:(void (^)(NSError *, NSInteger))failure
{

    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            idEntity, @"entity",
                            comment,@"text", nil];
    
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
    
    [manager POST:@"/api/comments/" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
        [operation start];
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSError* e;
        NSInteger statusCode;
        failure(e,statusCode);
    }];
    
    
}

-(void)deleteCommentInServerWithID:(NSString *)idComment
                     withOnSuccess:(void (^)(id))success onFailure:(void (^)(NSError *, NSInteger))failure {

    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    NSString* str = [NSString stringWithFormat:@"/api/comments/%@/",idComment];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Accept"];
    [manager DELETE:str parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//    NSLog(@"JSON: %@", responseObject);
        
    [operation start];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//         NSLog(@"Error: %@", error);
    }];
    
}
    
-(void)editCommentInServerWithID:(NSString *)idComment andWithTextComment:(NSString*)textComment
                   withOnSuccess:(void (^)(id result))success
                       onFailure:(void (^)(NSError* error, NSInteger statusCode))failure{

    /*
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            textComment,@"text",nil];
    NSLog(@"%@",params);
    NSString* str = [NSString stringWithFormat:@"/api/comments/%@/",idComment];
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
    
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager PUT:str parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
               NSLog(@"JSON: %@", responseObject);
        [operation start];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    */
    
    NSError *error;
    NSDictionary *params = @{ @"text" : textComment};
    NSData* jsonD = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&error];
    NSString* strJs = [[NSString alloc]initWithData:jsonD encoding:NSUTF8StringEncoding];
    
    NSString* urlString = [NSString stringWithFormat:@"https://www.foodlook.az/api/comments/%@/",idComment];
    NSMutableURLRequest *request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setValue: [NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"PUT"];
    //[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[strJs dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLResponse *res;
    
    NSData *resp = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&res error:
                    &error];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)res;
    NSInteger statusCode;
    statusCode = [httpResponse statusCode];
    NSString* str = [[NSString alloc] initWithData:resp encoding:NSUTF8StringEncoding];
    NSData *jsonData = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSError *e;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&e];
//    NSLog(@"%@",dict);
    //    self.token = [dict objectForKey:@"access_token"];
    //    NSLog(@"Temp Token %@",self.token);
    success(dict);
    
    
}

#pragma mark - Likes


-(void)postLikeInServerWithID:(NSString *)idEntity
                   withOnSuccess:(void (^)(id results))success
                       onFailure:(void (^)(NSError*  error, NSInteger statusCode))failure
{
    
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            idEntity, @"entity",nil];
    
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
    
    [manager POST:@"/api/likes/" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSLog(@"JSON: %@", responseObject);
        
        success(responseObject);
        [operation start];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSError* e;
        NSInteger statusCode;
        failure(e,statusCode);
        NSLog(@"Error: %@", error);
    }];

}

-(void)deleteLikeInServerWithID:(NSString *)idEntity
                withOnSuccess:(void (^)(id results))success
                    onFailure:(void (^)(NSError*  error, NSInteger statusCode))failure {
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    NSString* str = [NSString stringWithFormat:@"/api/likes/%@/",idEntity];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
   
    [manager DELETE:str parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
        
        [operation start];
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"JSON: %@", error);
    }];
    
}


#pragma mark - Sharing


-(void)postShareInServerWithID:(NSString *)idEntity
                withOnSuccess:(void (^)(id results))success
                    onFailure:(void (^)(NSError*  error, NSInteger statusCode))failure
{
    
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            idEntity, @"entity",nil];
    
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
    
    [manager POST:@"/api/shares/" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
        [operation start];
        success(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSError* e;
        NSInteger statusCode;
        failure(e,statusCode);
    }];
    
}

#pragma mark - Average Price Dish

-(void)getAveragePriceDishFromServerWithFrommPrice:(NSInteger)maxPrice
                                    withAcceptLanguage:(NSString*)language
                                    andOnSuccess:(void(^)(id results)) success
                                    onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure
{
    NSInteger dX = maxPrice - 5;
    NSInteger intMinPrice = dX;
    if (dX <= 0) {
        intMinPrice = 0;
        maxPrice = 5;
    }
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:language forHTTPHeaderField:@"Accept-Language"];
    NSString* strQuery = [NSString stringWithFormat:@"/api/courses/search/price/from/%ld/to/%ld/",(long)intMinPrice,(long)maxPrice];

    [manager GET:strQuery
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
             success(responseObject);
             
             [operation start];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             
             if (failure) {
                 failure(error, operation.response.statusCode);
             }
             
         }];
    
}

#pragma mark - Average Resto

-(void)getAverageRestoFromServerWithFrommPrice:(NSInteger)maxPrice
                                      andOnSuccess:(void(^)(id results)) success
                                         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    NSInteger dX = maxPrice - 5;
    NSInteger intMinPrice = dX;
    if (dX <= 0) {
        intMinPrice = 0;
        maxPrice = 5;
    }
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
    NSString* strQuery = [NSString stringWithFormat:@"/api/restaurants/search/price/from/%ld/to/%ld/",(long)intMinPrice,(long)maxPrice];
    
    [manager GET:strQuery
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSLog(@"%@",responseObject);
                 success(responseObject);
             
             [operation start];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             
             if (failure) {
                 failure(error, operation.response.statusCode);
             }
             
         }];
    
}


#pragma mark - Search methods 

-(void)searchDishInServerWithText:(NSString*)text
                                    onSuccess:(void(^)(id results)) success
                                    withAcceptLanguage:(NSString*)language
                                     onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
        [manager.requestSerializer setValue:language forHTTPHeaderField:@"Accept-Language"];
    NSString *newString =[text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString* strQuery = [NSString stringWithFormat:@"/api/courses/search/?q=%@",newString];
    
    [manager GET:strQuery
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             NSLog(@"%@",responseObject);
             success(responseObject);
             
             [operation start];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             
             if (failure) {
                 failure(error, operation.response.statusCode);
             }
             
         }];
    
}

-(void)searchRestoInServerWithText:(NSString*)text
                        onSuccess:(void(^)(id results)) success
                        onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
    
    NSString *newString =[text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString* strQuery = [NSString stringWithFormat:@"/api/restaurants/search/?q=%@",newString];
    
    [manager GET:strQuery
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {

                 success(responseObject);
             
             [operation start];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             
             if (failure) {
                 failure(error, operation.response.statusCode);
             }
             
         }];
    
}

#pragma mark - User Settings 


-(void)changeOldPassword:(NSString*)oldPassword
         withNewPassword:(NSString*)newPassword
               onSuccess:(void(^)(id results)) success
      withAcceptLanguage:(NSString*)language
               onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure
{
    
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
                            oldPassword, @"old_password", newPassword, @"new_password", nil];
    
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"https://www.foodlook.az"]];
    
    [manager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@", correctToken] forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer setValue:language forHTTPHeaderField:@"Accept-Language"];
    [manager PUT:@"/api/users/me/" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {

        
        success(responseObject);
        [operation start];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSError* e;
        NSInteger statusCode;
        failure(e,statusCode);
       
    }];
    
    
    
    
}




@end
