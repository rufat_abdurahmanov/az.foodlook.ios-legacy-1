//
//  FLRestoTableViewCell.m
//  FoodLook
//
//  Created by Yahin Rinat on 03.07.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import "FLRestoTableViewCell.h"

@implementation FLRestoTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
