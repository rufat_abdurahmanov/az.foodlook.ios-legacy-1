//
//  FLForUsersViewController.m
//  FoodLook
//
//  Created by Rinat Yahin on 02.02.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLForUsersViewController.h"
#import "SWRevealViewController.h"


@interface FLForUsersViewController ()<UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *sidebarButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIView *customNavigationBar;
@property (weak, nonatomic) IBOutlet UIView *customTabBar;
@property (weak, nonatomic) IBOutlet UIButton *bottomBtn;
@property (weak, nonatomic) IBOutlet UIButton *forUsers;

@end

@implementation FLForUsersViewController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_sidebarButton setTitle:NSLocalizedString(@"For users", nil) forState:UIControlStateNormal];
    [_bottomBtn setTitle:NSLocalizedString(@"Recommend to a friend", nil) forState:UIControlStateNormal];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.sidebarButton  addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    
    CGFloat tb = self.customTabBar.frame.size.height;
    CGFloat ih = self.imgView.image.size.height;
    CGFloat nh = self.customNavigationBar.frame.size.height;
    
    CGFloat sizeOfContent = tb+ih+nh;
    
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, sizeOfContent);
    CGFloat maxNavY = CGRectGetMaxY(self.customNavigationBar.frame);
    self.imgView.frame = CGRectMake(0,maxNavY, SCREEN_WIDTH, self.imgView.image.size.height);
    CGFloat maxY = CGRectGetMaxY(self.imgView.frame);
    CGRect frameTabBar = CGRectMake(self.customTabBar.frame.origin.x, maxY, self.customTabBar.frame.size.width, self.customTabBar.frame.size.height);
    self.customTabBar.frame = frameTabBar;
    SWRevealViewController *revealViewController = self.revealViewController;
    UITapGestureRecognizer *tap = [self.revealViewController tapGestureRecognizer];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    revealViewController.delegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    switch (position) {
        case  FrontViewPositionLeft:
    
            break;
            
        case FrontViewPositionRight:
    
            break;
            
        default:
            break;
    }
}
@end
