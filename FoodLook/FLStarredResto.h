//
//  FLStarredResto.h
//  FoodLook
//
//  Created by Yahin Rinat on 08.12.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FLStarredResto : NSObject

- (id) initWithServerResponse:(NSDictionary*) responseObject;

@property (strong, nonatomic) NSString* idResto;
@property (strong, nonatomic) NSString* label;
@property (assign) BOOL is_promoted;
@property (strong, nonatomic) NSString * background_image_url;
@property (strong, nonatomic) NSString* logo_url;


@end
