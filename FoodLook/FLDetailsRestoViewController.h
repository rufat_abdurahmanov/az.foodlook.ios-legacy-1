//
//  FLDetailsRestoViewController.h
//  FoodLook
//
//  Created by Yahin Rinat on 12.08.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FLResto.h"

@class FLDetailsRestoViewController;

@protocol FLDetailsRestoViewControllerDelegate <NSObject>

-(void)passDataRestoVCWithIdDish:(NSString*)idResto;

@end

@interface FLDetailsRestoViewController : UIViewController
@property (weak, nonatomic) id <FLDetailsRestoViewControllerDelegate> delegate;
@property (strong,nonatomic)NSString* idResto;


@end
