//
//  FLRestoTableViewCell.h
//  FoodLook
//
//  Created by Yahin Rinat on 03.07.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLRestoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *restoImage;
@property (weak, nonatomic) IBOutlet UIImageView *imageLogo;
@property (weak, nonatomic) IBOutlet UIImageView *placeholder;
@property (weak, nonatomic) IBOutlet UILabel *lblNameResto;
@property (weak, nonatomic) IBOutlet UIButton *makeReservButton;
@property (weak, nonatomic) IBOutlet UIView *conteinerView;

@end
