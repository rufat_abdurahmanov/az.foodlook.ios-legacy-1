//
//  FLBranchesRestaurantCell.h
//  FoodLook
//
//  Created by Rinat Yahin on 29.01.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FLBranchesRestaurantCell;

@protocol FLBranchesRestaurantCellDelegate <NSObject>

@optional
-(void)showMapBranches:(FLBranchesRestaurantCell*)cell;
@end

@interface FLBranchesRestaurantCell : UITableViewCell
@property (weak, nonatomic) id<FLBranchesRestaurantCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *adresLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel1;
@property (weak, nonatomic) IBOutlet UILabel *adresLabel1;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIButton *map;
@end
