//
//  FLContactViewController.m
//  FoodLook
//
//  Created by Rinat Yahin on 02.02.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLContactViewController.h"
#import "SWRevealViewController.h"
#import "FLContactWebViewViewController.h"
#import "FLContactMailViewController.h"
@interface FLContactViewController ()<UIGestureRecognizerDelegate,SWRevealViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *sidebarButton;
@property (strong, nonatomic) FLContactWebViewViewController* webVC;
@property (strong, nonatomic) FLContactMailViewController* mailVC;
@property (strong, nonatomic) NSString* strUrl;
@property (weak, nonatomic) IBOutlet UILabel *label;
@end

@implementation FLContactViewController




-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_sidebarButton setTitle:NSLocalizedString(@"Contacts", nil) forState:UIControlStateNormal];
    [_label setText:NSLocalizedString(@"We are glad to see you", nil)];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    [self.sidebarButton  addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    UITapGestureRecognizer *tap = [self.revealViewController tapGestureRecognizer];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.delegate = self;
}


- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    switch (position) {
        case  FrontViewPositionLeft:
            
            break;
            
        case FrontViewPositionRight:
            
            break;
            
        default:
            break;
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(CGRect)maxCoordinateModalView{
    
    CGRect rectInfoView = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y,                    self.view.frame.size.width, self.view.frame.size.height);
    CGFloat maxInfoViewYCordinate =  CGRectGetMaxY(rectInfoView);
    CGRect frame = CGRectMake(self.view.frame.origin.x, maxInfoViewYCordinate,self.view.frame.size.width, self.view.frame.size.height);
    return frame;
    
}

-(CGRect)minCoordinateModalView{
    
    CGRect rectInfoView = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y,self.view.frame.size.width, self.view.frame.size.height);
    CGFloat minYrectView =  CGRectGetMinY(rectInfoView);
    CGRect frame = CGRectMake(self.view.frame.origin.x, minYrectView, self.view.frame.size.width, self.view.frame.size.height - minYrectView);
    return frame;
    
}


-(IBAction)showFaceBook:(id)sender {
    
    self.strUrl = @"https://www.facebook.com/foodlookapp";
    self.webVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FLContactWebViewViewController"];
    self.webVC.urlWeb = self.strUrl;
    [self addChildViewController:self.webVC];
    self.webVC.view.frame = [self maxCoordinateModalView];
    [self.view addSubview:self.webVC.view];

   
    [UIView animateWithDuration:1 animations:^{
        self.webVC.view.frame = [self minCoordinateModalView];
       
    } completion:^(BOOL finished) {
        [self.webVC didMoveToParentViewController:self];
        
        
    }];
  
}

-(IBAction)showInsto:(id)sender {
    self.strUrl = @"http://instagram.com/foodlook_az";
    self.webVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FLContactWebViewViewController"];
    self.webVC.urlWeb = self.strUrl;
    [self addChildViewController:self.webVC];
    self.webVC.view.frame = [self maxCoordinateModalView];
    [self.view addSubview:self.webVC.view];
    
    
    [UIView animateWithDuration:1 animations:^{
        self.webVC.view.frame = [self minCoordinateModalView];
        
    } completion:^(BOOL finished) {
        [self.webVC didMoveToParentViewController:self];
        
        
    }];
}

-(IBAction)showWeb:(id)sender {
    self.strUrl = @"http://www.foodlookapp.com/";
    self.webVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FLContactWebViewViewController"];
    self.webVC.urlWeb = self.strUrl;
    [self addChildViewController:self.webVC];
    self.webVC.view.frame = [self maxCoordinateModalView];
    [self.view addSubview:self.webVC.view];
    
    
    [UIView animateWithDuration:1 animations:^{
        self.webVC.view.frame = [self minCoordinateModalView];
        
    } completion:^(BOOL finished) {
        [self.webVC didMoveToParentViewController:self];
        
        
    }];
}

-(IBAction)showMail:(id)sender {
    self.strUrl = @"team@foodlook.az";
    self.mailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FLContactMailViewController"];
    self.mailVC.email = self.strUrl;
    [self addChildViewController:self.mailVC];
    self.mailVC.view.frame = [self maxCoordinateModalView];
    [self.view addSubview:self.mailVC.view];
    
    
    [UIView animateWithDuration:1 animations:^{
        self.mailVC.view.frame = [self minCoordinateModalView];
        
    } completion:^(BOOL finished) {
        [self.mailVC didMoveToParentViewController:self];
        
        
    }];
}


-(IBAction)callPhone:(id)sender {
    
}


@end
