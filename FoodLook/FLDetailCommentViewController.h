//
//  FLDetailCommentViewController.h
//  FoodLook
//
//  Created by Rinat Yahin on 14/01/15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLDetailCommentViewController : UIViewController

@property (strong, nonatomic) NSString* strTextComment;
@property (strong, nonatomic) NSDictionary* dict;


@end
