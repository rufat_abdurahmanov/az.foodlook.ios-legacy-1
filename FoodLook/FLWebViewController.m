//
//  FLWebViewController.m
//  FoodLook
//
//  Created by Yahin Rinat on 15.11.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import "FLWebViewController.h"
#import "SWRevealViewController.h"
#import <MBProgressHUD.h>
@interface FLWebViewController () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView* indicator;
@property (weak, nonatomic) IBOutlet UIWebView* webView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem* backButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem* forwardButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem* refresh;

//@property (strong, nonatomic) IBOutlet UITabBarItem*  dissmisVC;

@end

@implementation FLWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSLog(@"%@",self.urlWeb);
    NSURL* url = [NSURL URLWithString:self.urlWeb];
    NSURLRequest* req = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:req];

    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
}

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"Swipe received.");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
    self.webView.delegate = nil;
    NSLog(@"webView deallocate");
}



#pragma mark - Actions


-(IBAction)actionBack:(id)sender{
    if ([self.webView canGoBack]) {
        [self.webView stopLoading];
        [self.webView goBack];
    }
    
}

-(IBAction)actionForward:(id)sender{
    if ([self.webView canGoForward]) {
        [self.webView stopLoading];
        [self.webView goForward];
    }
}

-(IBAction)actionRefresh:(id)sender{
    [self.webView reload];
}

-(IBAction)cancel:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];

}

#pragma mark -  UIWebViewDelegate


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView {
  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}


- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    self.backButtonItem.enabled = [self.webView canGoForward];
    self.forwardButton.enabled = [self.webView canGoForward];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
   [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

@end
