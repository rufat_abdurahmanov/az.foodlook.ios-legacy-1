//
//  FLCategoriesMenuViewController.h
//  FoodLook
//
//  Created by Rinat Yahin on 26.01.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLCategoriesMenuViewController : UIViewController

@property (strong, nonatomic) NSDictionary* dict;

@end
