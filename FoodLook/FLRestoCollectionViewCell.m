//
//  FLRestoCollectionViewCell.m
//  FoodLook
//
//  Created by Yahin Rinat on 19.10.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import "FLRestoCollectionViewCell.h"

@implementation FLRestoCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
