//
//  FLMenuCell.m
//  FoodLook
//
//  Created by Rinat Yahin on 26.01.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLMenuCell.h"

@implementation FLMenuCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
