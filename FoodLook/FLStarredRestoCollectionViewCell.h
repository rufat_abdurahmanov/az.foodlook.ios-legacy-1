//
//  FLStarredRestoCollectionViewCell.h
//  FoodLook
//
//  Created by Yahin Rinat on 08.12.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLStarredRestoCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageResto;
@property (weak, nonatomic) IBOutlet UIImageView *placeholder;
@end
