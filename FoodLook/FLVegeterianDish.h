//
//  FLVegeterianDish.h
//  FoodLook
//
//  Created by Yahin Rinat on 04.12.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FLVegeterianDish : NSObject

@property (strong, nonatomic) NSString* idDish;

@property (strong, nonatomic) NSString* label;
@property (strong, nonatomic) NSString* cost;
@property (assign) BOOL isVegetarian;
@property (strong, nonatomic) NSString* image_URL;
@property (strong, nonatomic) NSString* imgDishDetail;
@property (strong, nonatomic) NSString* ingredients;
@property (strong, nonatomic) NSDictionary* result;
@property (strong, nonatomic) NSNumber* like_quantity;
@property (strong, nonatomic) NSMutableDictionary* social;

- (id) initWithServerResponse:(NSDictionary*) responseObject;
@end
