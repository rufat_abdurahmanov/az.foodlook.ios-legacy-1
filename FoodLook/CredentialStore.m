//
//  CredentialStore.m
//  FoodLook
//
//  Created by Rinat Yahin on 14/04/15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "CredentialStore.h"
#import <SSKeychain.h>
#import "ServerManager.h"
#define SERVICE_NAME @"FoodLook"
#define AUTH_REFRASH_TOKEN @"auth_refresh_token"
#define AUTH_TOKEN_KEY @"auth_token"
#define USERNAME_KEY @"username"
#define PASSWORD_KEY @"password"
#define DATE_TOKEN @"dateToken"

@implementation CredentialStore


#pragma mark - authToken

-(BOOL)isLoggedIn{
    return [self authToken] != nil;
}
-(void)clearSavedCredential{
    [self setAuthToken:nil];
}
-(NSString*)authToken{
  return  [self secureValueForKey:AUTH_TOKEN_KEY];
}
-(void)setAuthToken:(NSString*)authToken{
    NSLog(@"%@",authToken);
     [self setSecureValue:authToken forKey:AUTH_TOKEN_KEY];
}



#pragma mark - Password And Login 

-(NSString*)password {
    return [self secureValueForKey:PASSWORD_KEY];
}
-(NSString*)username {
    return  [self secureValueForKey:USERNAME_KEY];
}
-(void)setPassword:(NSString*)password {
    [self setSecureValue:password forKey:PASSWORD_KEY];
}
-(void)setUsername:(NSString*)username {
    [self setSecureValue:username forKey:USERNAME_KEY];
}


#pragma Refresh Token

-(NSString*)refrashToken {
     return [self secureValueForKey:AUTH_REFRASH_TOKEN];
}
-(void)setAuthRefreshToken:(NSString*)refreshToken {
     [self setSecureValue:refreshToken forKey:AUTH_REFRASH_TOKEN];
}

#pragma Date token

-(NSString*)dateToken {
    return [self secureValueForKey:DATE_TOKEN];
}
-(void)setDateToken:(NSString*)dateToken {
    [self setSecureValue:dateToken forKey:DATE_TOKEN];
}


-(void)setSecureValue:(NSString*)value forKey:(NSString*)key {
    if (value) {
        [SSKeychain setPassword:value forService:SERVICE_NAME account:key];
    }else {
        [SSKeychain deletePasswordForService:SERVICE_NAME account:key];
    }
}

-(NSString*)secureValueForKey:(NSString*)key {
    return [SSKeychain passwordForService:SERVICE_NAME account:key];
}
@end
