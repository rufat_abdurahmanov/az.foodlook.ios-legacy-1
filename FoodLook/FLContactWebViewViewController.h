//
//  FLContactWebView.h
//  FoodLook
//
//  Created by Rinat Yahin on 14.02.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLContactWebViewViewController : UIViewController
@property (strong, nonatomic) NSString* urlWeb;
@end
