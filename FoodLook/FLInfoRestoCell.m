//
//  FLInfoRestoCell.m
//  FoodLook
//
//  Created by Rinat Yahin on 29.01.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLInfoRestoCell.h"
#import "NSString+CalculateHeight.h"
@implementation FLInfoRestoCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)showInsto:(id)sender {
    [self.delegate showInsto:self];
}


- (IBAction)showFaceBook:(id)sender {
    [self.delegate showFaceBook:self];
}

- (IBAction)showMap:(id)sender {
    [self.delegate showMap:self];
}

- (IBAction)sendEmail:(id)sender {
    [self.delegate sendEmail:self];
}

- (IBAction)showWeb:(id)sender {
    [self.delegate showWeb:self];
}

- (CGFloat) heightForKithchen:(NSString*)textKitchen AndWithAdreess:(NSString*)adress {
    
    
    UIFont* font = [UIFont systemFontOfSize:14];
    CGFloat KithLbl = _kitchenLabel.frame.size.width;
    CGFloat dKitchen = SCREEN_WIDTH - KithLbl;
    CGFloat heightKithLbl = [textKitchen getHeightOfFont:font widht:dKitchen];
    CGFloat adrLbl = _adresLabel.frame.size.width;
    CGFloat dAdrs = SCREEN_WIDTH - adrLbl;
    CGFloat heightAdrs = [adress getHeightOfFont:font widht:dAdrs];
    CGFloat offset = 280;
    CGFloat heightCell = heightKithLbl + heightAdrs + offset;
    
    return heightCell;
}



@end
