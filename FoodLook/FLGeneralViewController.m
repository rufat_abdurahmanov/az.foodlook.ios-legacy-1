//
//  FLGeneralViewController.m
//  FoodLook
//
//  Created by Yahin Rinat on 28.06.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import "FLGeneralViewController.h"
#import "FLLoginAndPasswordVC.h"
#import "FLCustomTableViewCell.h"
#import "FLRestoTableViewCell.h"
#import <SWRevealViewController.h>
#import "UIImageView+AFNetworking.h"
#import "FLSearchViewController.h"
#import "NSString+CalculateHeight.h"
#import <Social/Social.h>
#import "FLDetailsRestoViewController.h"
#import "FLDetailFoodViewController.h"
#import "FLCourses.h"
#import "ServerManager.h"
#import "FLResto.h"
#import "FLVegeterianDish.h"
#import "FLAverageCourses.h"
#import <QuartzCore/QuartzCore.h>
#import "FLAverageResto.h"
#import "UIImage+Resize.h"
#import "CredentialStore.h"
#import <MBProgressHUD.h>
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>

@interface FLGeneralViewController () <UITableViewDataSource, UITableViewDelegate,FLCustomTableViewCellDelegate,FLDetailFoodViewControllerDelegate,FLDetailsRestoViewControllerDelegate, SWRevealViewControllerDelegate, UIGestureRecognizerDelegate>
{
    NSInteger counter;
    BOOL isAvrResto;
    BOOL isAvrCourses;
    BOOL isCourses;
    BOOL isResto;
    BOOL isVegeterian;
    BOOL isFoodTable;
    BOOL isRestoTable;
    UIImage* placeholderImg;
    NSDate* requiredDate;
    NSDate* currentDate;
    NSString* dateStrToken;
    Reachability *internetReachableFoo;
}

#pragma mark - IBOutlet
@property (weak, nonatomic) IBOutlet UIView *avregePriceView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControlAvrPrice;
@property (weak, nonatomic) IBOutlet UIButton *btnVegeterianMode;
@property (weak, nonatomic) IBOutlet UILabel *avrLblValue;
@property (weak, nonatomic) IBOutlet UIButton *wannaEatBtn;
@property (weak, nonatomic) IBOutlet UILabel *setPriceLabel;
@property (strong, nonatomic) NSMutableArray* restoArray;
@property (strong, nonatomic) NSMutableArray* coursesArray;
@property (strong, nonatomic) NSArray* averarageFoodArray;
@property (strong, nonatomic) NSArray* averageRestoArray;
@property (strong, nonatomic) NSArray* vegeterianArray;
@property (strong, nonatomic) NSString* acceptLanguage;
@property (strong, nonatomic) NSString* likeID;
@property (nonatomic, strong) CredentialStore* credentialStore;
@property (weak, nonatomic) FLRestoTableViewCell* restoCell;
@property (weak, nonatomic) FLCustomTableViewCell* foodCell;
@property (nonatomic, strong) dispatch_queue_t concurrentPhotoQueue;
@property (nonatomic, weak) UITapGestureRecognizer* swRevealTap;
@end

const int kCourseLoadingCellTag = 1;
const int kRestoLoadingCellTag = 2;
static NSString* kSettingsLanguage = @"language";

@implementation FLGeneralViewController

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
            placeholderImg = [UIImage imageNamed:@"placeholder.png"];
            currentDate = [NSDate new];
    }
    return self;
}

#pragma mark - ViewController LifeCycle
-(void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    [self.foodTableView reloadData];
    
    [self.navigationController.navigationBar setHidden:YES];
    [self.segmentControl setTitle:NSLocalizedString(@"Food", nil) forSegmentAtIndex:0];
    [self.segmentControl setTitle:NSLocalizedString(@"Restaurants", nil) forSegmentAtIndex:1];
    [_wannaEatBtn setTitle:NSLocalizedString(@"Wanna eat!", nil) forState:UIControlStateNormal];
    [_setPriceLabel setText:NSLocalizedString(@"Set Price", nil)];
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:22.0f],
                                 NSForegroundColorAttributeName:[UIColor whiteColor]};
    
    [self.segmentControlAvrPrice setTitleTextAttributes:attributes
                                     forState:UIControlStateNormal];
    [self.segmentControlAvrPrice setTitleTextAttributes:attributes
                                               forState:UIControlStateHighlighted];


}

    



-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void)method {

    isCourses = YES;
    isVegeterian = NO;
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    [self.segmentControl setSelectedSegmentIndex:0];
    self.restoTableView.contentOffset = CGPointMake(0, 0);
    self.coursesArray = [NSMutableArray array];
    self.restoArray = [NSMutableArray array];
    self.vegeterianArray = [NSArray array];
    self.averageRestoArray = [NSArray new];
    self.averageRestoArray = [NSArray new];
    
    [self.btnVegeterianMode addTarget:self action:@selector(vegeterianModeOn:) forControlEvents:UIControlEventTouchUpInside];
    self.foodCell.delegate = self;
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton  addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
    }
    revealViewController.delegate = self;

    
    
    
    [self loadLanguage];
    _currentPage = 1;
    _currentResoPage = 1;
    [[ServerManager sharedManager]setAuthTokenHeader];
    [self getCoursesFromServerWithIncreasePage:_currentPage];
    [self getRestorauntFromServerIncreasePage:_currentResoPage];
    
}

-(void)checkLoggedIn {
    if (![self.credentialStore isLoggedIn]) {
        FLLoginAndPasswordVC* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FLLoginAndPasswordVC"];
        [self.navigationController presentViewController:vc
                                                animated:YES completion:nil];
    }else {
         [self checkExpiresToken];
         [self method];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self testInternetConnection ];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(method)
                                                 name:@"tokenGet"
                                               object:nil];
    
    self.credentialStore = [[CredentialStore alloc]init];
    [self checkLoggedIn];
    isFoodTable = YES;

    _swRevealTap.delegate = self;
    
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    switch (position) {
        case  FrontViewPositionLeft:
            [self.view removeGestureRecognizer:_swRevealTap];
            break;
            
        case FrontViewPositionRight:
            _swRevealTap = [self.revealViewController tapGestureRecognizer];
            [self.view addGestureRecognizer:_swRevealTap];
            break;
            
        default:
            break;
    }
}

-(void)checkExpiresToken {

    NSString* strDate = [self.credentialStore dateToken];
    NSString* strOldToken = [self.credentialStore refrashToken];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    requiredDate = [dateFormat dateFromString:strDate];
    
        if ([currentDate compare:requiredDate] == NSOrderedDescending) {
            NSLog(@"latter");
            [[ServerManager sharedManager]getRefreshTokenWithOldToken:strOldToken withOnSuccess:^(id results) {
                NSString* strToken = [results objectForKey:@"access_token"];
                NSString* strRefreshToken = [results objectForKey:@"refresh_token"];
                NSString* strTokenDate = [results objectForKey:@"expires_in"];
                dateStrToken = [self dateExpiresIndToken:strTokenDate];
                [self.credentialStore setAuthToken:strToken];
                [self.credentialStore setAuthRefreshToken:strRefreshToken];
                [self.credentialStore setDateToken:dateStrToken];
            } onFailure:^(NSError *error) {
                FLLoginAndPasswordVC* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FLLoginAndPasswordVC"];
                [self.navigationController presentViewController:vc
                                                        animated:YES completion:nil];
            }];
        }
}

-(NSString*)dateExpiresIndToken:(NSString*)strDate {
    NSInteger  expiresIn = [strDate integerValue];
    NSDate* tokenDate = [NSDate dateWithTimeIntervalSinceNow:expiresIn];
    NSLog(@"%@",tokenDate);
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *tokenDateStr = [formatter stringFromDate:tokenDate];
    
    return tokenDateStr;
}

#pragma mark loadLanguage
-(void)loadLanguage
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.acceptLanguage = [userDefaults objectForKey:kSettingsLanguage];
    
}


#pragma mark Sorted Methods

-(void)vegeterianModeOn:(id)sender
{
    UIButton* button = (UIButton*)sender;
    button.selected = !button.selected;
    
    if (button.selected) {
        NSLog(@"selected");
        isVegeterian = YES;
        isCourses = NO;
        isAvrCourses = NO;
        isAvrResto = NO;
        isResto = NO;
       
        [self getVegeterianCoursesFromServer];
        [self.foodTableView reloadData];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
    }else {
        isVegeterian = NO;
        isCourses = YES;
        [self.foodTableView reloadData];
        NSLog(@"OFF");
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action

- (IBAction)selectSegmentControl:(id)sender {

    if (self.segmentControl.selectedSegmentIndex == 0) {
        isCourses = YES;
        isResto = NO;
        isAvrResto = NO;
        isVegeterian = NO;
        isFoodTable = YES;
        isRestoTable = NO;
        [self.foodTableView setHidden:NO];
        [self.restoTableView setHidden:YES];
        [self.btnVegeterianMode setHidden:NO];
        [self.foodTableView  reloadData];
        
    }else if(self.segmentControl.selectedSegmentIndex ==1) {
      
        isCourses = NO;
        isResto = YES;
        isVegeterian = NO;
        isAvrResto = NO;
        isAvrCourses = NO;
        isFoodTable = NO;
        isRestoTable = YES;
        [self.foodTableView setHidden:YES];
        [self.restoTableView setHidden:NO];
        [self.btnVegeterianMode setSelected:NO];
        [self.btnVegeterianMode setHidden:YES];
        [self.restoTableView  reloadData];
        
    }
    
}

- (IBAction)showAvregePrice:(id)sender
{
    if (self.segmentControl.selectedSegmentIndex == 0) {
          [self.segmentControlAvrPrice setSelectedSegmentIndex:0];
        isAvrCourses = YES;
        isAvrResto = NO;
        isVegeterian = NO;
        isCourses = NO;
        
    }else if (self.segmentControl.selectedSegmentIndex == 1){
        [self.segmentControlAvrPrice setSelectedSegmentIndex:1];
        isAvrResto = YES;
        isAvrCourses = NO;
        isVegeterian = NO;
        isCourses = NO;
        isResto = NO;
        
    }
    [UIView animateWithDuration:0.4
                          delay:0.
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [self.view layoutIfNeeded];
                         self.avregePriceView.center =
                         CGPointMake(CGRectGetWidth(self.view.bounds)/2, CGRectGetMidY(self.view.bounds));
                         NSLog(@"go!");
                         
                     }
                     completion:^(BOOL finished){
                         self.avregePriceView.translatesAutoresizingMaskIntoConstraints = YES;
                         [self.avregePriceView setNeedsUpdateConstraints];
                         
                     }];
    
}

- (IBAction)dismissAvregePrice:(id)sender

{
    if (self.segmentControlAvrPrice.selectedSegmentIndex == 1) {
        [self.segmentControl setSelectedSegmentIndex:1];
    }
    if (self.segmentControlAvrPrice.selectedSegmentIndex == 2) {
        [self.segmentControl setSelectedSegmentIndex:2];
    }
    
    [UIView animateWithDuration:0.4
                          delay:0.5
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
        [self.avregePriceView layoutIfNeeded];
        [self.avrLblValue layoutIfNeeded];
        self.avregePriceView.center =
        CGPointMake(CGRectGetWidth(self.view.bounds)/2,CGRectGetMaxY(self.view.frame)* 2);

     if (isAvrCourses && counter != 0 && !isVegeterian) {
            isCourses = NO;
            isVegeterian= NO;

     }
     if (isAvrCourses && counter !=0) {
         [self getAverageCoursesFrommServerWithFrommPrice:counter];

     }
     if (isAvrResto && counter !=0) {
         [self getAverageRestoFrommServerWithFrommPrice:counter];
     }
                         
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
completion:^(BOOL finished){
    
}];
    
}




- (IBAction)buttonChengeValuepPlus:(UIButton *)sender{
    
    if (counter > 999 )
        return;
    [self.avregePriceView layoutIfNeeded];
    [self.avrLblValue layoutIfNeeded];
    [self.avrLblValue setText:[NSString stringWithFormat:@"%ld",(long)++counter]];
}
- (IBAction)buttonChengeValuepMinus:(UIButton *)sender{
    
    if (counter < 1 )
        return;
    [self.view layoutIfNeeded];
    [self.avregePriceView layoutIfNeeded];
    [self.avrLblValue setText:[NSString stringWithFormat:@"%ld",(long)--counter]];
    
}


#pragma mark - API

- (void) getCoursesFromServerWithIncreasePage:(NSInteger)increasePage {
    

    [[ServerManager sharedManager]getCoursesFromServerWithCurrentPage:_currentPage withAcceptLanguage:_acceptLanguage OnSuccess:^(id results) {
        for (NSDictionary* dict in [results objectForKey:@"results"]) {
            
            FLCourses* courses = [[FLCourses alloc]initWithServerResponse:dict];
            if (![self.coursesArray containsObject:courses]) {
                [self.coursesArray addObject:courses];
            }
            //  [self.coursesArray addObject:courses];
        }
        [self.foodTableView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (statusCode == 403) {
            FLLoginAndPasswordVC* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FLLoginAndPasswordVC"];
            [self.navigationController presentViewController:vc
                                                    animated:YES completion:nil];
        }
    }];
    
}


-(void)getRestorauntFromServerIncreasePage:(NSInteger)increasePage
{
    [[ServerManager sharedManager]getRestorauntFromServerWithCurrentPage:increasePage withAcceptLanguage:_acceptLanguage OnSuccess:^(id result) {
        
        NSDictionary* courses = (NSDictionary *)result;
        for (NSDictionary* dict in [courses objectForKey:@"results"]) {
            
            FLResto* resto = [[FLResto alloc]initWithServerResponse:dict];
            [self.restoArray addObject:resto];
        }
        [self.restoTableView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
    
}

-(void)getVegeterianCoursesFromServer {
    
    [[ServerManager sharedManager]getVegeterianCoursesFromServerOnSuccess:^(NSArray *results) {
        self.vegeterianArray = results;
        [self.foodTableView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } withAcceptLanguage:_acceptLanguage onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}


-(void)getAverageCoursesFrommServerWithFrommPrice:(NSInteger)price {
    [[ServerManager sharedManager]getAveragePriceDishFromServerWithFrommPrice:price withAcceptLanguage:_acceptLanguage andOnSuccess:^(id results) {
        
        NSDictionary* courses = (NSDictionary *)results;
        NSMutableArray* tempArr = [NSMutableArray array];
        for (NSDictionary* dict in [courses objectForKey:@"results"]) {
            FLAverageCourses* courses = [[FLAverageCourses alloc]initWithServerResponse:dict];
            [tempArr addObject:courses];
        }
        self.averarageFoodArray = tempArr;
        [self.foodTableView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

-(void)getAverageRestoFrommServerWithFrommPrice:(NSInteger)price {
    [[ServerManager sharedManager]getAverageRestoFromServerWithFrommPrice:price andOnSuccess:^(id results) {
        NSDictionary* courses = (NSDictionary *)results;
        NSMutableArray* tempArr = [NSMutableArray array];
        for (NSDictionary* dict in [courses objectForKey:@"results"]) {
            FLAverageResto* courses = [[FLAverageResto alloc]initWithServerResponse:dict];
            [tempArr addObject:courses];
        }
        self.averageRestoArray = tempArr;
        [self.restoTableView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];

}

-(void)deleteLikeFrommServerWithIdEntity:(NSString*)strID{
    [[ServerManager sharedManager] deleteLikeInServerWithID:strID withOnSuccess:^(id results) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    if (self.segmentControl.selectedSegmentIndex == 0) {
        if (isCourses) {

          return [self.coursesArray count];
        }
        if (isVegeterian) {
            return [self.vegeterianArray count];
        }
        if (isAvrCourses) {
            return [self.averarageFoodArray count];
        }
    }
    if (self.segmentControl.selectedSegmentIndex == 1) {
        if (isResto) {
            return [self.restoArray count];
        }
        if (isAvrResto) {
            return [self.averageRestoArray count];
        }
    }

    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        static NSString *RestorauntIdentifier = @"RestorauntCell";
    static NSString *FoodIdentifier = @"FoodCell";
    if (self.segmentControl.selectedSegmentIndex == 0) {
        if (isCourses) {
            
            return  [self corsesCellForIndexPath:indexPath andWithIdentifier:FoodIdentifier];
            }
        if (isVegeterian) {
            return [self vegeterianCellForIndexPath:indexPath andWithIdentifier:FoodIdentifier];
        }
        if (isAvrCourses) {
            return [self avaregeFoodCellForIndexPath:indexPath andWithIdentifier:FoodIdentifier];
        }
    }
    if(_segmentControl.selectedSegmentIndex == 1){
        if (isResto) {
       
                return  [self restoCellForIndexPath:indexPath andWithIdentifier:RestorauntIdentifier];
        }
        if (isAvrResto) {
          return [self averageRestoCellForIndexPath:indexPath andWithIdentifier:RestorauntIdentifier];
        }
    }
    
    
    return nil;
}

- (UITableViewCell *)corsesCellForIndexPath:(NSIndexPath *)indexPath andWithIdentifier:(NSString*)identifier {
    FLCustomTableViewCell *cell = [self.foodTableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[FLCustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    FLCourses * cours = [self.coursesArray objectAtIndex:indexPath.row];
    
    NSString* strName = cours.label;
    CGFloat offsetX = self.foodTableView.frame.size.width - 80;
    CGFloat frameNamelbl = [strName getHeightOfFont:cell.nameDishesLabel.font widht:offsetX];
    cell.nameDishesLabel.frame = CGRectMake(8, 8, offsetX, frameNamelbl);
    cell.nameDishesLabel.text = strName;
    cell.delegate = self;
    cell.labelPrice.text = [NSString stringWithFormat:@"%@",cours.cost];

    NSString* str = [NSString stringWithFormat:@"%@",[cours.social objectForKey:@"like_id"]];
    NSString* strNull = @"<null>";
    if ([str isEqualToString:strNull]) {
        [cell.buttonlikes setImage:[UIImage imageNamed:@"btnLikeGrey@2x"] forState:UIControlStateNormal];
    }else if (![str isEqualToString:strNull]) {
        [cell.buttonlikes setImage:[UIImage imageNamed:@"btnLikeRed@2x"] forState:UIControlStateNormal];
    }
    cell.like_quantity.text = [NSString stringWithFormat:@"%@",[cours.social objectForKey:@"likes"]];
    

    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:cours.image_URL]];
    __weak FLCustomTableViewCell* weakCell = cell;
    __weak UIImageView *weakImageView = cell.dishesImage;
    [cell.dishesImage setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {

        [self settingUITableViewCell:weakCell withResizeImage:image withWeakUIImageView:weakImageView withPlaceHolder:placeholderImg andWithSocialView:YES];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        weakCell.dishesImage.image = nil;
        NSLog(@"%@",error);
    }];

    return cell;
}

- (UITableViewCell *)vegeterianCellForIndexPath:(NSIndexPath *)indexPath andWithIdentifier:(NSString*)identifier {
    
    FLCustomTableViewCell *cell = [self.foodTableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[FLCustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    FLVegeterianDish * cours = [self.vegeterianArray objectAtIndex:indexPath.row];
    
    NSString* strName = cours.label;
    CGFloat offsetX = self.foodTableView.frame.size.width - 80;
    CGFloat frameNamelbl = [strName getHeightOfFont:cell.nameDishesLabel.font widht:offsetX];
    cell.nameDishesLabel.frame = CGRectMake(8, 8, offsetX, frameNamelbl);
    cell.nameDishesLabel.text = strName;
    
    cell.delegate = self;
    cell.labelPrice.text = [NSString stringWithFormat:@"%@",cours.cost];
    
    CGFloat maxY = CGRectGetMaxY(cell.dishesImage.frame);
    CGFloat dBtnY = maxY  - 50;
    CGRect contView = CGRectMake(cell.containerView.frame.origin.x, dBtnY, cell.containerView.frame.size.width, cell.containerView.frame.size.height);
    cell.containerView.frame = contView;
    
    NSString* str = [NSString stringWithFormat:@"%@",[cours.social objectForKey:@"like_id"]];
    NSString* strNull = @"<null>";
    if ([str isEqualToString:strNull]) {
        [cell.buttonlikes setImage:[UIImage imageNamed:@"btnLikeGrey@2x"] forState:UIControlStateNormal];
    }else if (![str isEqualToString:strNull]) {
        [cell.buttonlikes setImage:[UIImage imageNamed:@"btnLikeRed@2x"] forState:UIControlStateNormal];
    }
    cell.like_quantity.text = [NSString stringWithFormat:@"%@",[cours.social objectForKey:@"likes"]];
    [cell.buttonlikes setHidden:NO];
    [cell.like_quantity setHidden:NO];
    [cell setNeedsLayout];
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:cours.image_URL]];
    __weak FLCustomTableViewCell* weakCell = cell;
    __weak UIImageView *weakImageView = cell.dishesImage;
    
    [cell.dishesImage setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        
        [self settingUITableViewCell:weakCell withResizeImage:image withWeakUIImageView:weakImageView withPlaceHolder:placeholderImg andWithSocialView:NO];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        
    }];
    
    return cell;
    
}

- (UITableViewCell *)avaregeFoodCellForIndexPath:(NSIndexPath *)indexPath andWithIdentifier:(NSString*)identifier {
    
    FLCustomTableViewCell *cell = [self.foodTableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[FLCustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    FLAverageCourses * cours = [self.averarageFoodArray objectAtIndex:indexPath.row];
    NSString* strName = cours.label;
    CGFloat offsetX = self.foodTableView.frame.size.width - 80;
    CGFloat frameNamelbl = [strName getHeightOfFont:cell.nameDishesLabel.font widht:offsetX];
    
    cell.nameDishesLabel.frame = CGRectMake(8, 8, offsetX, frameNamelbl);
    cell.nameDishesLabel.text = strName;
    cell.labelPrice.text = [NSString stringWithFormat:@"%@",cours.cost];
    [cell.buttonlikes setHidden:YES];
    [cell.like_quantity setHidden:YES];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:cours.image_URL]];
    __weak FLCustomTableViewCell* weakCell = cell;
    __weak UIImageView *weakImageView = cell.dishesImage;
    
    [cell.dishesImage setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        
        [self settingUITableViewCell:weakCell withResizeImage:image withWeakUIImageView:weakImageView withPlaceHolder:placeholderImg andWithSocialView:NO];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        
    }];
    
    return cell;
    
}


-(void)settingUITableViewCell:(FLCustomTableViewCell*)weakCell withResizeImage:(UIImage*)image withWeakUIImageView:(UIImageView*)weakImageView withPlaceHolder:(UIImage*)placeholder andWithSocialView:(BOOL)isSocial  {
    
    image = [image resizedImageWithBounds:CGSizeMake(weakCell.frame.size.width, weakCell.frame.size.height)];
    weakImageView.image = image;
    weakCell.dishesImage = weakImageView;
    [weakCell.placeHolder setImage:placeholder];
    weakCell.dishesImage.clipsToBounds  = YES;
    [weakCell setNeedsLayout];
    weakCell.dishesImage.frame = CGRectMake(0, 0, weakCell.frame.size.width, weakCell.frame.size.height);
    weakCell.dishesImage.contentMode = UIViewContentModeScaleAspectFill;
    
    if (isSocial) {
    CGFloat maxY = CGRectGetMaxY(weakCell.dishesImage.frame);
    CGFloat dBtnY = maxY  - 50;
    CGRect contView = CGRectMake(weakCell.containerView.frame.origin.x, dBtnY, weakCell.containerView.frame.size.width, weakCell.containerView.frame.size.height);
    weakCell.containerView.frame = contView;

    }
    
}

- (UITableViewCell *)restoCellForIndexPath:(NSIndexPath *)indexPath andWithIdentifier:(NSString*)identifier {
    
    FLRestoTableViewCell *cell = [self.restoTableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[FLRestoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    FLResto* resto = [self.restoArray objectAtIndex:indexPath.row];
    [cell.imageLogo setImageWithURL: [NSURL URLWithString:resto.logo_url]];
    
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:resto.background_image_url ]];
    
    __weak FLRestoTableViewCell* weakCell = cell;
    __weak UIImageView *weakImageView = cell.restoImage;
    
    [cell.restoImage setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        
    [self settingRestoTableViewCell:weakCell withResizeImage:image
                withWeakUIImageView:weakImageView
                    withPlaceHolder:placeholderImg
                  andWithButtomView:YES withModel:(FLResto*)resto];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        
    }];
    return cell;
    
}


- (UITableViewCell *)averageRestoCellForIndexPath:(NSIndexPath *)indexPath andWithIdentifier:(NSString*)identifier {
    
    FLRestoTableViewCell *cell = [self.restoTableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[FLRestoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    FLAverageResto* resto = [self.averageRestoArray objectAtIndex:indexPath.row];
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:resto.background_image_url ]];
    
    __weak FLRestoTableViewCell* weakCell = cell;
    __weak UIImageView *weakImageView = cell.restoImage;
    
    [cell.restoImage setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
    
        [self settingRestoTableViewCell:weakCell withResizeImage:image
                    withWeakUIImageView:weakImageView
                        withPlaceHolder:placeholderImg
                      andWithButtomView:YES withModel:(FLResto*)resto];
        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        
    }];
    return cell;
    
}

-(void)settingRestoTableViewCell:(FLRestoTableViewCell*)weakCell withResizeImage:(UIImage*)image withWeakUIImageView:(UIImageView*)weakImageView withPlaceHolder:(UIImage*)placeholder andWithButtomView:(BOOL)isButtomView withModel:(FLResto*)resto {
    
    image = [image resizedImageWithBounds:CGSizeMake(weakCell.frame.size.width, weakCell.frame.size.height)];
    weakImageView.image = image;
    weakCell.restoImage = weakImageView;
    [weakCell.placeholder setImage:placeholder];
    weakCell.placeholder.clipsToBounds  = YES;
    [weakCell setNeedsLayout];
    weakCell.placeholder.frame = CGRectMake(0, 0, weakCell.frame.size.width, weakCell.frame.size.height);
    weakCell.placeholder.contentMode = UIViewContentModeScaleAspectFill;
    
    weakCell.imageLogo.layer.borderWidth=1.0;
    weakCell.imageLogo.layer.masksToBounds = YES;
    weakCell.imageLogo.layer.borderColor=[[UIColor redColor] CGColor];
    
    [weakCell.makeReservButton setTitle:NSLocalizedString(@"Make Reservation", nil) forState:UIControlStateNormal];
    
    NSString* strName =  resto.label;
    CGFloat offsetX = self.restoTableView.frame.size.width;
    CGFloat frameNamelbl = [strName getHeightOfFont:weakCell.lblNameResto.font widht:offsetX];
    weakCell.lblNameResto.frame = CGRectMake(8, 8, offsetX, frameNamelbl);
    weakCell.lblNameResto.text = strName;
    
    [weakCell.imageLogo setImageWithURL: [NSURL URLWithString:resto.logo_url]];
    weakCell.lblNameResto.text = resto.label;

    if (isButtomView) {
        CGFloat maxY = CGRectGetMaxY(weakCell.restoImage.frame);
        CGFloat dMaxY = maxY - weakCell.conteinerView.frame.size.height;
        CGRect rectView = CGRectMake(weakCell.conteinerView.frame.origin.x , dMaxY, SCREEN_WIDTH,weakCell.conteinerView.frame.size.height);
        weakCell.conteinerView.frame = rectView;
        CGFloat dMaxLogoY = maxY - weakCell.imageLogo.frame.size.height;
        CGRect frmaLogo = CGRectMake(weakCell.imageLogo.frame.origin.x, dMaxLogoY, weakCell.imageLogo.frame.size.width, weakCell.imageLogo.frame.size.height);
        weakCell.imageLogo.frame = frmaLogo;
        
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        //its iPhone. Find out which one?
        
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.height == 480)
        {
            // iPhone Classic
            return 170.0f;
        }
        else if(result.height == 568)
        {

            return 215.0f;
        }
        else if(result.height == 667)
        {
            // iPhone 6
            return 264.0f;
        }
        else if(result.height == 736)
        {
            // iPhone 6 Plus
            return 302.0f;
        }
    }

    return 200;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (![self.foodTableView isHidden]) {
    if (self.segmentControl.selectedSegmentIndex == 0) {
        if (isCourses) {
            FLCourses* courses = [self.coursesArray objectAtIndex:indexPath.row];
            [self passDataCourseVCWithIdDish:courses];
        }
        if (isVegeterian) {
            FLVegeterianDish* courses = [self.vegeterianArray objectAtIndex:indexPath.row];
            [self passDataVegeterianFoodVCWithIdDish:courses];
        }
        if (isAvrCourses) {
            FLAverageCourses* courses = [self.averarageFoodArray objectAtIndex:indexPath.row];
            [self passDataAvaregeFoodVCWithIdDish:courses];

        }
    }
    }else if (![self.restoTableView isHidden]) {
        if (self.segmentControl.selectedSegmentIndex == 1) {
        if (isResto) {
            FLResto* resto = [self.restoArray objectAtIndex:indexPath.row];
            [self passDataRestoVCWithIdDish:resto.idResto];
        }
        if (isAvrResto) {
            FLAverageResto* resto = [self.averageRestoArray objectAtIndex:indexPath.row];
           [self passDataRestoVCWithIdDish:resto.idResto];
            }
        }
    }
}

#pragma mark FLDetailFoodViewController

-(void)passDataCourseVCWithIdDish:(FLCourses *)idDish {
    [self performSegueWithIdentifier:@"Food" sender:idDish];
}
-(void)passDataVegeterianFoodVCWithIdDish:(FLVegeterianDish *)idDish {
    [self performSegueWithIdentifier:@"Food" sender:idDish];
}

-(void)passDataAvaregeFoodVCWithIdDish:(FLVegeterianDish *)idDish {
    [self performSegueWithIdentifier:@"Food" sender:idDish];
}

#pragma mark FLDetailsRestoViewControllerDelegate

-(void)passDataRestoVCWithIdDish:(NSString *)idResto {
    [self performSegueWithIdentifier:@"RestoIdentifier" sender:idResto];
   
}


-(void)likeButton:(FLCustomTableViewCell *)cell{
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if (isCourses) {
        cell.buttonlikes.enabled = NO;
        NSIndexPath *indexPath = [self.foodTableView indexPathForCell:cell];
        FLCourses* post = [self.coursesArray objectAtIndex:indexPath.row];
        
       self.likeID = [NSString stringWithFormat:@"%@",[post.social objectForKey:@"like_id"]];

        NSString* strNull = @"<null>";
        if ([_likeID isEqualToString:strNull]) {
            
            [[ServerManager sharedManager] postLikeInServerWithID:post.idDish withOnSuccess:^(id results) {
                NSString* string  = [NSString stringWithFormat:@"%@",[post.social objectForKey:@"likes"]];
                int likes = [string intValue];
                likes += 1;
                 NSLog(@"results %@",results);
                self.likeID =  [NSString stringWithFormat:@"%@",[results objectForKey:@"id"]];
                [post.social setObject:[NSString stringWithFormat:@"%d",likes] forKey:@"likes"];
                [post.social setObject:[NSString stringWithFormat:@"%@", self.likeID] forKey:@"like_id"];
                NSLog(@"LIKE ID %@",self.likeID);

                cell.buttonlikes.enabled = YES;

                dispatch_async(dispatch_get_main_queue(), ^{

                    [self.foodTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
                });

                  [MBProgressHUD hideHUDForView:self.view animated:YES];
            } onFailure:^(NSError *error, NSInteger statusCode) {
                   NSLog(@"%@",error);
                cell.buttonlikes.enabled = YES;
                
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
            }];
        }
        if (![_likeID isEqualToString:strNull])
        {
            //dislike
            [[ServerManager sharedManager]deleteLikeInServerWithID:self.likeID withOnSuccess:^(id results) {
                NSString* string  = [NSString stringWithFormat:@"%@",[post.social objectForKey:@"likes"]];
                int likes = [string intValue];
                likes -= 1;
                
                [post.social setObject:[NSString stringWithFormat:@"%d",likes] forKey:@"likes"];
                [post.social setObject:@"<null>" forKey:@"like_id"];
                cell.buttonlikes.enabled = YES;
                dispatch_async(dispatch_get_main_queue(), ^{

                    [self.foodTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
                });
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
            } onFailure:^(NSError *error, NSInteger statusCode) {
                NSLog(@"%@",error);
                cell.buttonlikes.enabled = YES;
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
            }];
        }
    }

    if (isVegeterian) {
        cell.buttonlikes.enabled = NO;
        NSIndexPath *indexPath = [self.foodTableView indexPathForCell:cell];
        FLVegeterianDish* post = [self.vegeterianArray objectAtIndex:indexPath.row];
        
       __block NSString* str = [NSString stringWithFormat:@"%@",[post.social objectForKey:@"like_id"]];
        NSString* strNull = @"<null>";
        if ([str isEqualToString:strNull]) {
            
            [[ServerManager sharedManager] postLikeInServerWithID:post.idDish withOnSuccess:^(id results) {
                NSString* string  = [NSString stringWithFormat:@"%@",[post.social objectForKey:@"likes"]];
                int likes = [string intValue];
                likes += 1;
                str = [NSString stringWithFormat:@"%@",[results objectForKey:@"id"]];
                [post.social setObject:[NSString stringWithFormat:@"%d",likes] forKey:@"likes"];
                [post.social setObject:[NSString stringWithFormat:@"%@",str] forKey:@"like_id"];
                
                /*
                 cell.like_quantity.text = [NSString stringWithFormat:@"%d",likes];
                 [cell.buttonlikes setImage:[UIImage imageNamed:@"btnLikeRed@2x"] forState:UIControlStateNormal];
                 */
                cell.buttonlikes.enabled = YES;
                [self.foodTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
            } onFailure:^(NSError *error, NSInteger statusCode) {
                cell.buttonlikes.enabled = YES;
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
        
            }];
        }else if (![str isEqualToString:strNull])
        {
            //dislike
            [[ServerManager sharedManager]deleteLikeInServerWithID:str withOnSuccess:^(id results) {
                NSString* string  = [NSString stringWithFormat:@"%@",[post.social objectForKey:@"likes"]];
                int likes = [string intValue];
                likes -= 1;
                
                [post.social setObject:[NSString stringWithFormat:@"%d",likes] forKey:@"likes"];
                [post.social setObject:@"<null>" forKey:@"like_id"];
                cell.buttonlikes.enabled = YES;
                [self.foodTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
            } onFailure:^(NSError *error, NSInteger statusCode) {
                cell.buttonlikes.enabled = YES;
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
            }];
        }
    }

       cell.buttonlikes.enabled = YES;
}


#pragma mark - Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"Food"]) {
      //      NSLog(@"Food!");
        FLDetailFoodViewController* foodVC = segue.destinationViewController;
        foodVC.delegate = self;
        if (isCourses) {
            foodVC.dish = sender;
        }
        if (isVegeterian) {
              foodVC.vegeterianDish = sender;
        }
        if (isAvrCourses) {
            foodVC.avrDish = sender;
        }
        
        }
    if ([segue.identifier isEqualToString:@"RestoIdentifier"]) {
       // NSLog(@"Resto");
        FLDetailsRestoViewController* restoVC = segue.destinationViewController;
            restoVC.delegate = self;
            restoVC.idResto = sender;
        }

}



-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == _restoTableView || _foodTableView){
        float scrollViewHeight = scrollView.frame.size.height;
        float scrollContentSizeHeight = scrollView.contentSize.height;
        float scrollOffset = scrollView.contentOffset.y;
        
        if (scrollOffset == 0){
            // then we are at the top
        }
    else if (scrollOffset + scrollViewHeight >= scrollContentSizeHeight - 3){
            // then we are at the end
            // _currentOffset = _currentOffset + 10;
            if (_foodTableView && isFoodTable) {
            if (self.segmentControl.selectedSegmentIndex == 0) {
                if (isCourses) {
                    _currentPage++;
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    [self getCoursesFromServerWithIncreasePage:_currentPage];
                    
                }
    
            }
        }
            if (_restoTableView && isRestoTable) {

            if (self.segmentControl.selectedSegmentIndex == 1) {
                if (isResto) {
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    [self getCoursesFromServerWithIncreasePage:_currentResoPage];
                    
                    }
                }
            }
        }
    }
}


- (void)testInternetConnection
{
    
    internetReachableFoo = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    // Internet is reachable
    internetReachableFoo.reachableBlock = ^(Reachability*reach)
    {
        // Update the UI on the main thread
        
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    };
    
    // Internet is not reachable
    internetReachableFoo.unreachableBlock = ^(Reachability*reach)
    {
        // Update the UI on the main thread
        __weak typeof(self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            __strong typeof(self) strongSelf = weakSelf;
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Somting went wrong..."
                                                            message:@"Please check internet connection"
                                                           delegate:nil
                                                  cancelButtonTitle:@"ok"
                                                  otherButtonTitles:nil];
            [alert show];
            
            [strongSelf performSelector:@selector(exitApp) withObject:nil afterDelay:3];
        });
    };
    
    [internetReachableFoo startNotifier];
}

-(void)exitApp{
    UIApplication *app = [UIApplication sharedApplication];
    [app performSelector:@selector(suspend)];
    
    //wait 2 seconds while app is going background
    [NSThread sleepForTimeInterval:2.0];
    
    //exit app when app is in background
    exit(0);
}



@end
