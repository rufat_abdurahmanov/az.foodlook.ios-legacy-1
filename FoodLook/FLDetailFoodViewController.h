//
//  FLDetailFoodViewController.h
//  FoodLook
//
//  Created by Yahin Rinat on 16.08.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FLCourses.h"
#import "FLMenuResto.h"
#import "FLCourseSearch.h"
#import "FLVegeterianDish.h"
#import "FLAverageCourses.h"
#import "FLDetailsRestoViewController.h"
@class FLDetailFoodViewController;

@protocol FLDetailFoodViewControllerDelegate <NSObject>
@optional
-(void)passDataCourseVCWithIdDish:(FLCourses*) idDish;
-(void)passDataVegeterianFoodVCWithIdDish:(FLVegeterianDish*) idDish;
-(void)passDataAvaregeFoodVCWithIdDish:(FLAverageCourses*) idDish;
-(void)passDataFoodVCWithIdDish:(NSString*) idDish;
@end


@interface FLDetailFoodViewController : UIViewController

@property (weak, nonatomic)id <FLDetailFoodViewControllerDelegate> delegate;
@property (strong, nonatomic) FLCourses* dish;
@property (strong, nonatomic) FLVegeterianDish* vegeterianDish;
@property (strong, nonatomic) FLAverageCourses* avrDish;
@property (strong, nonatomic) FLMenuResto* menuData;
@property (strong, nonatomic) NSString* idDish;

@property (weak, nonatomic) IBOutlet UIImageView *imageFood;
@property (weak, nonatomic) IBOutlet UILabel *labelNameDish;
@property (weak, nonatomic) IBOutlet UILabel *labelCostDish;
@property (weak, nonatomic) IBOutlet UILabel *lblIngredients;
@property (weak, nonatomic) IBOutlet UIImageView *imgResto;


@end
