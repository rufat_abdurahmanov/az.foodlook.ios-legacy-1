//
//  FLAppDelegate.h
//  FoodLook
//
//  Created by Yahin Rinat on 28.06.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FLLoginAndPasswordVC.h"
#import "FLGeneralViewController.h"
#import "FLNavController.h"

#define SCREEN_HEIGHT [[ UIScreen mainScreen ] bounds ].size.height
#define SCREEN_WIDTH [[ UIScreen mainScreen ] bounds ].size.width


@interface FLAppDelegate : UIResponder <UIApplicationDelegate>



@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController* navigationController;
@property (strong, nonatomic) FLGeneralViewController* generalVC;
@end
