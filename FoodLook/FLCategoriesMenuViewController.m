//
//  FLCategoriesMenuViewController.m
//  FoodLook
//
//  Created by Rinat Yahin on 26.01.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLCategoriesMenuViewController.h"
#import "ServerManager.h"
#import "FLMenuCell.h"
#import "FLMenuResto.h"
#import "UIImageView+AFNetworking.h"
#import "FLDetailFoodViewController.h"
#import <MBProgressHUD.h>
@interface FLCategoriesMenuViewController () <UITableViewDataSource,UITableViewDelegate,FLDetailFoodViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *nameCategoriesLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *nameCategories2Label;
@property (strong, nonatomic) NSString* acceptLanguage;
@property (strong, nonatomic) FLMenuCell* customCell;


@end

static NSString* kSettingsLanguage = @"language";
@implementation FLCategoriesMenuViewController
{
    NSArray* menuArr;
}

#pragma mark - ViewController Life Сycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self updateUI];
     [self loadLanguage];
    NSLog(@"%@",self.dict);
    [self getMenuDishWithCategories];
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
 
  
}


-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"Swipe received.");
}




-(void)loadLanguage
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.acceptLanguage = [userDefaults objectForKey:kSettingsLanguage];
    
}

#pragma mark - API ServerManager

-(void)getMenuDishWithCategories{
    NSString* strRestoID = [self.dict objectForKey:@"idRest"];
    NSString* strCategoriesID = [self.dict objectForKey:@"categories"];
    
    [[ServerManager sharedManager]getRestorauntCategoriesWithRestoID:strRestoID andWithIDCategories:strCategoriesID withAcceptLanguage:_acceptLanguage onSuccess:^(id results) {
        NSDictionary* result = (NSDictionary *)results;
        NSMutableArray* objectsArray = [NSMutableArray array];
        for (NSDictionary* dict in [result objectForKey:@"results"]) {
            
            FLMenuResto* menu = [[FLMenuResto alloc]initWithServerResponse:dict];
            //                NSLog(@"%@",courses.text);
            [objectsArray addObject:menu];
            
        }
        menuArr = [NSArray arrayWithArray:objectsArray];
        [self.tableView reloadData];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
}

#pragma mark - Update UI

-(void)updateUI {
    self.nameCategories2Label.text = [self.dict objectForKey:@"nameCategories"];
    self.nameCategoriesLabel.text = [self.dict objectForKey:@"label"];
}

#pragma mark - Action

- (IBAction)backButton:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  [menuArr count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        //its iPhone. Find out which one?
        
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.height == 480)
        {
            // iPhone Classic
            return 80.0f;
        }
        else if(result.height == 568)
        {
            // iPhone 5
            return 100.0f;
        }
        else if(result.height == 667)
        {
            // iPhone 6
            return 140.0f;
        }
        else if(result.height == 736)
        {
            // iPhone 6 Plus
            return 140.0f;
        }
    }
    
    return 200;

}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *reuseIdentifier = @"Cell";
    FLMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if(!cell) {
        cell = [[FLMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
         cell.imageViewDish.contentMode = UIViewContentModeScaleToFill;

    }
    FLMenuResto* menu = [menuArr objectAtIndex:indexPath.row];
    
    cell.nameDishLabel.text = menu.nameDish;
    [cell.nameDishLabel sizeToFit];
    CGFloat imageWidth = cell.conteinerView.frame.size.width;
    CGFloat cellWidth = cell.frame.size.width;
    CGFloat dX = cellWidth - imageWidth;
    dX -= 10;
    cell.nameDishLabel.frame = CGRectMake(cell.nameDishLabel.frame.origin.x, cell.nameDishLabel.frame.origin.y, dX, cell.nameDishLabel.frame.size.height);
    cell.priceLabel.text = [NSString stringWithFormat:@"%@ AZN",menu.cost];
    CGFloat yNamedish = CGRectGetMaxY(cell.nameDishLabel.frame);
    cell.priceLabel.frame = CGRectMake(cell.priceLabel.frame.origin.x, yNamedish + 4, cell.priceLabel.frame.size.width, cell.priceLabel.frame.size.height);
    [cell.priceLabel sizeToFit];

    cell.conteinerView.frame = CGRectMake(cell.conteinerView.frame.origin.x, cell.conteinerView.frame.origin.y, cell.conteinerView.frame.size.width, cell.conteinerView.frame.size.height);
    
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:menu.imageDish]];
    __weak FLMenuCell* weakCell = cell;
    
    [cell.imageViewDish setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        weakCell.imageViewDish.image = image;
        weakCell.clipsToBounds = YES;
          weakCell.imageViewDish.frame = CGRectMake(weakCell.imageViewDish.frame.origin.x, weakCell.imageViewDish.frame.origin.y, weakCell.imageViewDish.frame.size.width,  weakCell.imageViewDish.frame.size.height);
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        
    }];
    return cell;
}


#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FLMenuResto* courses = [menuArr objectAtIndex:indexPath.row];
    NSLog(@"%@",courses.idDish);
     [self passDataFoodVCWithIdDish:courses.idDish];
    
}
-(void)passDataFoodVCWithIdDish:(NSString *)idDish {
    [self performSegueWithIdentifier:@"goToFood" sender:idDish];
}


#pragma mark - Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"goToFood"]) {
        FLDetailFoodViewController* foodVC = segue.destinationViewController;
        foodVC.delegate = self;
        foodVC.idDish = sender;
    }

}
@end
