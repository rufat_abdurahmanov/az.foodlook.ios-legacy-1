//
//  FLInfoRestoCell.h
//  FoodLook
//
//  Created by Rinat Yahin on 29.01.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FLInfoRestoCell;

@protocol FLInfoRestoCellDelegate <NSObject>

@optional
-(void)showInsto:(FLInfoRestoCell*)cell;
-(void)showFaceBook:(FLInfoRestoCell*)cell;
-(void)showMap:(FLInfoRestoCell*)cell;
-(void)sendEmail:(FLInfoRestoCell*)cell;
-(void)showWeb:(FLInfoRestoCell*)cell;
@end


@interface FLInfoRestoCell : UITableViewCell
@property (weak, nonatomic) id<FLInfoRestoCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *kitchenLabel;
@property (weak, nonatomic) IBOutlet UILabel *kitchenLabel1;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumberLabel1;
@property (weak, nonatomic) IBOutlet UILabel *adresLabel;
@property (weak, nonatomic) IBOutlet UILabel *adresLabel1;
@property (weak, nonatomic) IBOutlet UILabel *workClocksLabel;
@property (weak, nonatomic) IBOutlet UILabel *workClocksLabel1;
@property (weak, nonatomic) IBOutlet UILabel *clocksKithenLabel;
@property (weak, nonatomic) IBOutlet UILabel *clocksKithenLabel1;
@property (weak, nonatomic) IBOutlet UILabel *creditCardLabel;
@property (weak, nonatomic) IBOutlet UILabel *creditCardLabel1;
@property (weak, nonatomic) IBOutlet UILabel *parkingLabel;
@property (weak, nonatomic) IBOutlet UILabel *parkingLabel1;
@property (weak, nonatomic) IBOutlet UIButton *buttonMap;
@property (weak, nonatomic) IBOutlet UIView *buttonView;
- (CGFloat) heightForKithchen:(NSString*)textKitchen AndWithAdreess:(NSString*)adress;

@end
