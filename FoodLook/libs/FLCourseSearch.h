//
//  FLCourseSearch.h
//  FoodLook
//
//  Created by Rinat Yahin on 01.02.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FLCourseSearch : NSObject
- (id) initWithServerResponse:(NSDictionary*) responseObject;


@property (strong, nonatomic) NSString* idDish;
@property (strong, nonatomic) NSString* label;
@property (strong, nonatomic) NSString* cost;
@property (strong, nonatomic) NSString* image_URL;
@end
