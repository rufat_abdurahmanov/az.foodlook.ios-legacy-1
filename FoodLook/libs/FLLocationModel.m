//
//  FLLocationModel.m
//  FoodLook
//
//  Created by Rinat Yahin on 29.01.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLLocationModel.h"

@implementation FLLocationModel

- (id) initWithServerResponse:(NSDictionary*) responseObject
{
    self = [super init];
    if (self) {
        
        
        
        NSString* address = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"address"]];
        
        if (address) {
            self.address = address;
            
        }
        
        
        NSString* latitude = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"latitude"]];
        
        if (latitude) {
            self.latitude = latitude;
            
        }
        
        NSString* longitude = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"longitude"]];
        
        if (longitude) {
            self.longitude = longitude;
            
        }
        
        NSString* telephones = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"telephones"]];
        if (telephones) {
                NSLog(@"%@",telephones);
            self.telephones = telephones;
        }
        
        
    }
    return self;
}

@end
