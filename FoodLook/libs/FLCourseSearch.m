//
//  FLCourseSearch.m
//  FoodLook
//
//  Created by Rinat Yahin on 01.02.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLCourseSearch.h"

@implementation FLCourseSearch
- (id) initWithServerResponse:(NSDictionary*) responseObject
{
    self = [super init];
    if (self) {
        
        
        
        NSString* idDish = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"id"]];
        
        if (idDish) {
            self.idDish = idDish;
            
        }
        
        
        NSString* cost = [responseObject objectForKey:@"price"];
        
        if (cost) {
            self.cost = cost;
            
        }
        
        NSString* label = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"label"]];
        
        if (label) {
            self.label = label;
            
        }
        
        NSString* image = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"image"]];
        
        if (image) {
            self.image_URL = image;
            
        }
        
        
    }
    return self;
}

@end
