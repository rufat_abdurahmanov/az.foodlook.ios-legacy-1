//
//  FLMenuCategories.m
//  FoodLook
//
//  Created by Rinat Yahin on 22/01/15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLMenuCategories.h"

@implementation FLMenuCategories

- (id) initWithServerResponse:(NSDictionary*) responseObject
{
    self = [super init];
    if (self) {
        
        
        NSString* idCategories = [responseObject objectForKey:@"id"];
        
        if (idCategories) {
            self.idCategories = idCategories;
        }
        
        
        NSString* label = [responseObject objectForKey:@"label"];
        
        if (label) {
            self.label = label;
            NSLog(@"%@",label);
        }
        
    }
    
    return self;
}


@end
