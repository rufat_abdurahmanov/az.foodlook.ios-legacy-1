//
//  FLLocationModel.h
//  FoodLook
//
//  Created by Rinat Yahin on 29.01.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FLLocationModel : NSObject

- (id) initWithServerResponse:(NSDictionary*) responseObject;

@property (strong, nonatomic) NSString* address;
@property (strong, nonatomic) NSString* latitude;
@property (strong, nonatomic) NSString* longitude;
@property (strong, nonatomic) NSString* telephones;


@end
