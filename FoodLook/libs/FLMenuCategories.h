//
//  FLMenuCategories.h
//  FoodLook
//
//  Created by Rinat Yahin on 22/01/15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FLMenuCategories : NSObject

- (id) initWithServerResponse:(NSDictionary*) responseObject;

@property (strong, nonatomic) NSString* idCategories;
@property (strong, nonatomic) NSString* label;

@end
