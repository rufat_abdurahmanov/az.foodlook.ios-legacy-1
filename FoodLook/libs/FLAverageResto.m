//
//  FLAverageResto.m
//  FoodLook
//
//  Created by Rinat Yahin on 19/01/15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLAverageResto.h"

@implementation FLAverageResto
- (id) initWithServerResponse:(NSDictionary*) responseObject
{
    self = [super init];
    if (self) {
        
        
        NSString* strResto = [responseObject objectForKey:@"background_image"];
        
        if (strResto) {
            NSString *backgroundImage = [strResto stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            self.background_image_url = backgroundImage;
        }
        
        
        NSString* idResto = [[responseObject objectForKey:@"id"]stringValue];
        
        if (idResto) {
            self.idResto = idResto;
            
        }
        
        
        NSString* label = [responseObject objectForKey:@"label"];
        
        if (label) {
            self.label = label;
            
        }
        
        NSString* urlString = [responseObject objectForKey:@"logo"];
        
        
        if (urlString) {
            NSString *str = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            self.logo_url = str;
        }
        
    }
    
    return self;
}



@end
