//
//  UIImage+Resize.h
//  Recipes
//
//  Created by Rinat Yahin on 03/03/15.
//  Copyright (c) 2015 Rinat Yahin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Resize)

- (UIImage *)resizedImageWithBounds:(CGSize)bounds;
- (UIImage *)compressForUpload:(UIImage *)original withScale:(CGFloat)scale;
@end
