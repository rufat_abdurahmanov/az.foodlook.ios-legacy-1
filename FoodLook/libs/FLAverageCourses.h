//
//  FLAverageCourses.h
//  FoodLook
//
//  Created by Rinat Yahin on 19/01/15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FLAverageCourses : NSObject
- (id) initWithServerResponse:(NSDictionary*) responseObject;


@property (strong, nonatomic) NSString* idDish;
//@property (strong, nonatomic) NSString* created;
//@property (strong, nonatomic) NSString* lastUpdated;
@property (strong, nonatomic) NSString* label;
@property (strong, nonatomic) NSString* cost;
@property (assign) BOOL isVegetarian;
@property (strong, nonatomic) NSString* image_URL;
@property (strong, nonatomic) NSString* imgDishDetail;
@property (strong, nonatomic) NSString* ingredients;
@property (strong, nonatomic) NSDictionary* result;
@property (strong, nonatomic) NSDictionary* social;

@end
