//
//  FLCommentModel.h
//  FoodLook
//
//  Created by Rinat Yahin on 07/01/15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FLCommentModel : NSObject

@property (strong, nonatomic) NSString* text;
@property (strong, nonatomic) NSString* idComment;
@property (strong, nonatomic) NSDictionary* user;

- (id) initWithServerResponse:(NSDictionary*) responseObject;

@end
