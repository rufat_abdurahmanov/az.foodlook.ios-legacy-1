//
//  FLCommentModel.m
//  FoodLook
//
//  Created by Rinat Yahin on 07/01/15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLCommentModel.h"

@implementation FLCommentModel

- (id) initWithServerResponse:(NSDictionary*) responseObject
{
    self = [super init];
    if (self) {
        
        NSString* text = [responseObject objectForKey:@"text"];
        if (text) {
//            NSLog(@"%@",text);
            self.text = text;
        }
        
        NSString* idComment = [responseObject objectForKey:@"id"];
        if (idComment) {
//                        NSLog(@"%@",idComment);
            self.idComment = idComment;
        }
        
        
        NSDictionary* user = [responseObject objectForKey:@"user"];
        if (user) {
//           NSLog(@"%@",user);
            self.user = user;
        }
        
    }
    return self;
}



@end
