//
//  FLRestoSearch.h
//  FoodLook
//
//  Created by Rinat Yahin on 01.02.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FLRestoSearch : NSObject

- (id) initWithServerResponse:(NSDictionary*) responseObject;

@property (strong, nonatomic) NSString* idResto;
@property (strong, nonatomic) NSString* label;
@property (assign) BOOL is_promoted;
@property (strong, nonatomic) NSString * background_image_url;
@property (strong, nonatomic) NSString* logo_url;
@end
