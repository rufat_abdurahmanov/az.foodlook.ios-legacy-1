//
//  FLAverageCourses.m
//  FoodLook
//
//  Created by Rinat Yahin on 19/01/15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLAverageCourses.h"

@implementation FLAverageCourses

- (id) initWithServerResponse:(NSDictionary*) responseObject
{
    self = [super init];
    if (self) {
        
        
        
        NSString* idDish = [[responseObject objectForKey:@"id"]stringValue];
        
        if (idDish) {
            self.idDish = idDish;
            
        }
        
        NSString* cost = [responseObject objectForKey:@"price"];
        
        if (cost) {
            self.cost = cost;
            
        }
        
        NSString* label = [responseObject objectForKey:@"label"];
        
        if (label) {
            self.label = label;
            
        }
        
        NSString* urlString = [responseObject objectForKey:@"image"];
        if (urlString) {
            //     NSLog(@"%@",urlString);
            self.image_URL = urlString;
        }
        
        NSDictionary* social = [responseObject objectForKey:@"social"];
        if (social) {
            //     NSLog(@"%@",urlString);
            self.social = social;
        }
        
    }
    return self;
}
@end
