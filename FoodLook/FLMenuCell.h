//
//  FLMenuCell.h
//  FoodLook
//
//  Created by Rinat Yahin on 26.01.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLMenuCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameDishLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *IngredientsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewDish;
@property (weak, nonatomic) IBOutlet UIView *conteinerView;


@end
