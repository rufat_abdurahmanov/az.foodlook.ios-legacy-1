//
//  FLAccauntViewController.m
//  FoodLook
//
//  Created by Rinat Yahin on 02.02.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLAccountViewController.h"
#import "SWRevealViewController.h"
#import "FLPasswordViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <MBProgressHUD.h>

static NSString* kSettingsLanguage = @"language";

@interface FLAccountViewController ()<UIGestureRecognizerDelegate,SWRevealViewControllerDelegate>

@property (strong, nonatomic) FLPasswordViewController* modal;
@property (strong, nonatomic) NSString* strLanguage;
@property (strong, nonatomic) IBOutlet UIButton* rusButton;
@property (strong, nonatomic) IBOutlet UIButton* azButton;
@property (strong, nonatomic) IBOutlet UIButton* changePasButton;
@property (strong, nonatomic) IBOutlet UILabel* language;
@end

@implementation FLAccountViewController
{
    MBProgressHUD* hud;
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_sidebarButton setTitle:NSLocalizedString(@"Contacts", nil) forState:UIControlStateNormal];
    [_rusButton setTitle:NSLocalizedString(@"Russian", nil) forState:UIControlStateNormal];
    [_azButton setTitle:NSLocalizedString(@"Azerbaijani", nil) forState:UIControlStateNormal];
    [_changePasButton setTitle:NSLocalizedString(@"Change password", nil) forState:UIControlStateNormal];
    [_language setText:NSLocalizedString(@"Language", nil)];

}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _rusButton.layer.cornerRadius = 10;
    _rusButton.clipsToBounds = YES;
    
    _azButton.layer.cornerRadius = 10;
    _azButton.clipsToBounds = YES;
    
    _changePasButton.layer.cornerRadius = 10;
    _changePasButton.clipsToBounds = YES;
    
    [self.sidebarButton  addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    UITapGestureRecognizer *tap = [self.revealViewController tapGestureRecognizer];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.delegate = self;
}


- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    switch (position) {
        case  FrontViewPositionLeft:
            
            break;
            
        case FrontViewPositionRight:
            
            break;
            
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ModalViewController

- (IBAction)toggleHalfModal:(UIButton *)sender {
    
    self.modal = [self.storyboard instantiateViewControllerWithIdentifier:@"FLPasswordViewController"];
    [self addChildViewController:self.modal];
    self.modal.view.frame = [self maxCoordinateModalView];
    [self.view addSubview:self.modal.view];
    
    [UIView animateWithDuration:1 animations:^{
        self.modal.view.frame = [self minCoordinateModalView];
    } completion:^(BOOL finished) {
        [self.modal didMoveToParentViewController:self];
     
    }];
    
}


-(CGRect)maxCoordinateModalView{
    
    CGRect rectInfoView = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y,                    self.view.frame.size.width, self.view.frame.size.height);
    CGFloat maxInfoViewYCordinate =  CGRectGetMaxY(rectInfoView);
    CGRect frame = CGRectMake(self.view.frame.origin.x, maxInfoViewYCordinate,self.view.frame.size.width, self.view.frame.size.height);
    return frame;
    
}

-(CGRect)minCoordinateModalView{
    
    CGRect rectInfoView = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y,self.view.frame.size.width, self.view.frame.size.height);
    CGFloat minYrectView =  CGRectGetMinY(rectInfoView);
    CGRect frame = CGRectMake(self.view.frame.origin.x, minYrectView, self.view.frame.size.width, self.view.frame.size.height - minYrectView);
    return frame;
    
}

- (IBAction)chooseRussianLanguage:(UIButton *)sender {
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    self.strLanguage = @"ru";
    [userDefaults setObject:self.strLanguage forKey:kSettingsLanguage];
    [userDefaults synchronize];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = @"Язык именен";
    [hud hide:YES afterDelay:2];

    
}

- (IBAction)chooseAzerbLanguage:(UIButton *)sender {
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    self.strLanguage = @"az";
     [userDefaults setObject:self.strLanguage forKey:kSettingsLanguage];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = @"Язык именен";
    [hud hide:YES afterDelay:2];
}

@end
