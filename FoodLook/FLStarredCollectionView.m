//
//  FLStarredCollectionView.m
//  FoodLook
//
//  Created by Rinat Yahin on 23/01/15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLStarredCollectionView.h"
#import "ServerManager.h"
#import "FLCourses.h"
#import "UIImageView+AFNetworking.h"
#import "FLImageFoodCollectionViewCell.h"
#import "FLRestoCollectionViewCell.h"
#import "FLResto.h"
#import "FLStarredResto.h"
#import "FLStarredRestoCollectionViewCell.h"
#import "FLDetailFoodViewController.h"
#import "FLDetailsRestoViewController.h"
#import "UIImage+Resize.h"
#import <MBProgressHUD.h>

@interface FLStarredCollectionView () <UIActionSheetDelegate, UICollectionViewDataSource, UICollectionViewDelegate, FLImageFoodCollectionViewCell,FLDetailFoodViewControllerDelegate,FLDetailsRestoViewControllerDelegate>
{
    BOOL isFood;
    BOOL isResto;
    BOOL isStarred;
//    MBProgressHUD* HUD;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (strong, nonatomic) NSMutableArray* dishArray;
@property (strong, nonatomic) NSMutableArray* restoArray;
@property (strong, nonatomic) NSMutableArray* starredRestoArray;
@property (assign, nonatomic) NSInteger currentPage;
@property (assign, nonatomic) NSInteger currentResoPage;
@property (assign, nonatomic) NSInteger currentStarredResoPage;
@property (strong, nonatomic) NSString* acceptLanguage;

@end

static NSString* kSettingsLanguage = @"language";
@implementation FLStarredCollectionView
{
    UISwipeGestureRecognizer* swipeLeft;
    UISwipeGestureRecognizer* swipeRight;
    NSInteger imageIndex;
    UIImage* placeholderImg;
    NSTimer* restoTimer;
    NSTimer* foodTimer;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        placeholderImg = [UIImage imageNamed:@"placeholder.png"];
      
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.segmentControl setTitle:NSLocalizedString(@"Food", nil) forSegmentAtIndex:0];
    [self.segmentControl setTitle:NSLocalizedString(@"Restaurants", nil) forSegmentAtIndex:1];
    [self.segmentControl setTitle:NSLocalizedString(@"Starred", nil) forSegmentAtIndex:2];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self.collectionView reloadData];
    });
   
        
}

-(void)loadLanguage
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.acceptLanguage = [userDefaults objectForKey:kSettingsLanguage];
    
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    isFood = YES;
    self.dishArray = [NSMutableArray array];
    self.restoArray = [NSMutableArray array];
    self.starredRestoArray = [NSMutableArray array];
    _currentPage = 1;
    _currentResoPage = 1;
     [self loadLanguage];
    [self getCoursesFromServerWithIncreasePage:_currentPage];
    [self getStarredRestoFrommServer];
    [self getRestorauntFromServerIncreasePage:_currentResoPage];
    [self addGestureSwipe];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
      if ([self.dishArray count] < 20) {
            foodTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                         selector:@selector(loadDish) userInfo:nil repeats:YES];
          if ([self.dishArray count] > 20) {
              [foodTimer invalidate];
          }
      }
    
    if ([self.restoArray count] < 20) {
            restoTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                        selector:@selector(loadResto) userInfo:nil repeats:YES];
        if ([self.restoArray count] > 20) {
            [restoTimer invalidate];
        }
    }
}

-(void)dealloc {
     [restoTimer invalidate];
     [foodTimer invalidate];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [restoTimer invalidate];
    [foodTimer invalidate];
}
-(void)loadDish {
  
        _currentPage ++;
        
        [self getCoursesFromServerWithIncreasePage:_currentPage];
}
-(void)loadResto {
    
        _currentResoPage ++;
        [self getRestorauntFromServerIncreasePage:_currentResoPage];
}

-(void)addGestureSwipe {
    swipeLeft = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipe:)];
    swipeRight = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(handleSwipe:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    swipeLeft.numberOfTouchesRequired = 1;
    swipeRight.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:swipeRight];
    [self.view addGestureRecognizer:swipeLeft];
}

- (void)handleSwipe:(UISwipeGestureRecognizer *)sender {
    
    if (sender.direction & UISwipeGestureRecognizerDirectionLeft) {
        imageIndex ++;
    }
    if (sender.direction & UISwipeGestureRecognizerDirectionRight) {
        imageIndex --;
    }
    int number = self.segmentControl.numberOfSegments;
    imageIndex = (imageIndex < 0) ? (number -1):(imageIndex)% number;
    self.segmentControl.selectedSegmentIndex = imageIndex;
    [self.collectionView reloadData];
    [self tableSwitch:self];
}





#pragma mark -Actions

- (IBAction)backToGenerelVC:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)tableSwitch:(id )sender {
    
    if (self.segmentControl.selectedSegmentIndex == 0) {
        isFood = YES;
        isStarred = NO;
        isResto = NO;
        [self.collectionView  reloadData];
    }
        if(self.segmentControl.selectedSegmentIndex == 1) {
        isFood = NO;
        isResto = YES;
        isStarred = NO;
        [self.collectionView  reloadData];
        }
      
        if (self.segmentControl.selectedSegmentIndex == 2) {
        isFood = NO;
        isResto = NO;
        isStarred = YES;
        [self.collectionView  reloadData];
        }
}



#pragma mark - API

-(void) getStarredRestoFrommServer {

    [[ServerManager sharedManager] getStarredRestorauntFromServerOnSuccess:^(id result) {
        NSDictionary* courses = (NSDictionary *)result;
        
        NSMutableArray* objectsArray = [NSMutableArray array];
        //         NSLog(@"%@",responseObject);
        
        for (NSDictionary* dict in [courses objectForKey:@"results"]) {
            // NSLog(@"%@",[dict allKeys]);
            FLStarredResto* resto = [[FLStarredResto alloc]initWithServerResponse:dict];
            [objectsArray addObject:resto];
        }
        self.starredRestoArray = objectsArray;
        [self.collectionView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void) getCoursesFromServerWithIncreasePage:(NSInteger)increasePage  {
    
    
    [[ServerManager sharedManager] getCoursesFromServerWithCurrentPage:increasePage withAcceptLanguage:_acceptLanguage OnSuccess:^(id results) {
        for (NSDictionary* dict in [results objectForKey:@"results"]) {
            
            FLCourses* courses = [[FLCourses alloc]initWithServerResponse:dict];
            if (![self.dishArray containsObject:courses]) {
                [self.dishArray addObject:courses];
                NSLog(@"%d",[self.dishArray count]);
            }
            [self.collectionView reloadData];
        }
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
}


-(void)getRestorauntFromServerIncreasePage:(NSInteger)increasePage
    {

        [[ServerManager sharedManager]getRestorauntFromServerWithCurrentPage:increasePage withAcceptLanguage:_acceptLanguage OnSuccess:^(id result) {
            NSDictionary* courses = (NSDictionary *)result;
            for (NSDictionary* dict in [courses objectForKey:@"results"]) {
                
                FLResto* resto = [[FLResto alloc]initWithServerResponse:dict];
                [self.restoArray addObject:resto];
            }
            [self.collectionView reloadData];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        } onFailure:^(NSError *error, NSInteger statusCode) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }];
}

#pragma UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    NSLog(@" isFood %d",isFood);
    NSLog(@" isResto %d",isResto);
    NSLog(@" isStarred %d",isStarred);
    if (self.segmentControl.selectedSegmentIndex == 0) {
        return [self.dishArray count];
    }
     if (self.segmentControl.selectedSegmentIndex == 1){
        return [self.restoArray count];
     }
     if (self.segmentControl.selectedSegmentIndex == 2) {
        return [self.starredRestoArray count];
    }
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifierFood = @"Dishcell";
    static NSString *identifierResto = @"RestoCell";
    static NSString *identifierStarredResto = @"StarredRestoCell";
    NSString* reuseIdentifier;
    
    

    if (self.segmentControl.selectedSegmentIndex == 0) {
        reuseIdentifier = identifierFood;
        FLImageFoodCollectionViewCell *cell = (FLImageFoodCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
        
        FLCourses* post = [self.dishArray objectAtIndex:indexPath.row];
        NSString* str = [NSString stringWithFormat:@"%@",[post.social objectForKey:@"likes"]];
        cell.lblLikeCount.text = str;
        
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:post.image_URL ]];
        __weak FLImageFoodCollectionViewCell* weakCell = cell;
        __weak UIImageView *weakImageView = cell.imageFood;
        
        [cell.imageFood setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            
            image = [image resizedImageWithBounds:CGSizeMake(weakCell.frame.size.width, weakCell.frame.size.height)];
            weakImageView.image = image;
            weakCell.imageFood = weakImageView;
         [weakCell.placeholder setImage:placeholderImg];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
       
            weakCell.imageFood.image = nil;
            NSLog(@"%@",error);
        }];

        
        return cell;
    }else if(self.segmentControl.selectedSegmentIndex == 1) {
        
        reuseIdentifier = identifierResto;
        FLRestoCollectionViewCell *cell = (FLRestoCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
        
        FLResto* post = [self.restoArray objectAtIndex:indexPath.row];
        
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:post.background_image_url ]];
        __weak FLRestoCollectionViewCell* weakCell = cell;
        __weak UIImageView *weakImageView = cell.imageResto;
        [cell.imageResto setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            
            image = [image resizedImageWithBounds:CGSizeMake(weakCell.frame.size.width, weakCell.frame.size.height)];
            weakImageView.image = image;
            weakCell.imageResto = weakImageView;
            [weakCell.placeholder setImage:placeholderImg];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            
            weakCell.imageResto.image = nil;
            NSLog(@"%@",error);
        }];
        
        return cell;
        
    }else if(self.segmentControl.selectedSegmentIndex == 2) {
        reuseIdentifier = identifierStarredResto;
        FLStarredRestoCollectionViewCell *cell = (FLStarredRestoCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
        
        FLStarredResto* post = [self.starredRestoArray objectAtIndex:indexPath.row];
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:post.background_image_url ]];
        __weak FLStarredRestoCollectionViewCell* weakCell = cell;
        __weak UIImageView *weakImageView = cell.imageResto;
        [cell.imageResto setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            
            image = [image resizedImageWithBounds:CGSizeMake(weakCell.frame.size.width, weakCell.frame.size.height)];
            weakImageView.image = image;
            weakCell.imageResto = weakImageView;
            [weakCell.placeholder setImage:placeholderImg];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            
            weakCell.imageResto.image = nil;
            NSLog(@"%@",error);
        }];
        return cell;
    }

    return nil;
  
}

#pragma mark - 

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.segmentControl.selectedSegmentIndex == 0) {
        if (isFood) {
             FLCourses* post = [self.dishArray objectAtIndex:indexPath.row];
            [self passDataCourseVCWithIdDish:post];
        }
    }
    if (self.segmentControl.selectedSegmentIndex == 1) {
        if (isResto) {
            FLResto* post = [self.restoArray objectAtIndex:indexPath.row];
            [self passDataRestoVCWithIdDish:post.idResto];

        }
    }
    if (self.segmentControl.selectedSegmentIndex == 2) {
        if (isStarred) {
             FLStarredResto* post = [self.restoArray objectAtIndex:indexPath.row];
           [self passDataRestoVCWithIdDish:post.idResto];
        }
    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat widht = SCREEN_WIDTH/3;
    CGFloat resultWidht = widht - 1;
    return CGSizeMake(resultWidht, 90);

    
}


#pragma mark FLDetailFoodViewController

-(void)passDataCourseVCWithIdDish:(FLCourses *)idDish {
    [self performSegueWithIdentifier:@"Food" sender:idDish];
}

#pragma mark FLDetailsRestoViewControllerDelegate

-(void)passDataRestoVCWithIdDish:(NSString *)idResto {
    [self performSegueWithIdentifier:@"Resto" sender:idResto];
    
}

#pragma mark - Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"Food"]) {
        
        FLDetailFoodViewController* foodVC = segue.destinationViewController;
        foodVC.delegate = self;
        foodVC.dish = sender;
    }
    if ([segue.identifier isEqualToString:@"Resto"]) {
        
        FLDetailsRestoViewController* restoVC = segue.destinationViewController;
        restoVC.delegate = self;
        restoVC.idResto = sender;
    }
    
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    if (scrollView == _collectionView)
    {
        float scrollViewHeight = scrollView.frame.size.height;
        float scrollContentSizeHeight = scrollView.contentSize.height;
        float scrollOffset = scrollView.contentOffset.y;
        
        if (scrollOffset == 0)
        {
            // then we are at the top
        }
        else if (scrollOffset + scrollViewHeight >= scrollContentSizeHeight - 3)
        {
            // then we are at the end
            // _currentOffset = _currentOffset + 10;
            
            if (self.segmentControl.selectedSegmentIndex == 0) {
                _currentPage++;
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                [self getCoursesFromServerWithIncreasePage:_currentPage];
            }
            if (self.segmentControl.selectedSegmentIndex == 1) {
                 _currentResoPage++;
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                [self getRestorauntFromServerIncreasePage:_currentResoPage];
            }
        }
    }
}

@end