//
//  FLResto.h
//  FoodLook
//
//  Created by Yahin Rinat on 06.10.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FLResto : NSObject

- (id) initWithServerResponse:(NSDictionary*) responseObject;

@property (strong, nonatomic) NSString* idResto;
@property (strong, nonatomic) NSString* label;
@property (assign) BOOL is_promoted;
@property (strong, nonatomic) NSString * background_image_url;
@property (strong, nonatomic) NSString* logo_url;


#pragma mark - restoDetails
@property (strong, nonatomic) NSDictionary* detailResto;
@property (strong, nonatomic) NSString* descriptionResto;
@property (strong, nonatomic) NSString* cuisine;
@property (strong, nonatomic) NSString* averageBill;
@property (strong, nonatomic) NSString* liveMusic;
@property (strong, nonatomic) NSString* acceptPaymentCards;
@property (strong, nonatomic) NSString* parking;
@property (strong, nonatomic) NSString* restaurantOpened;
@property (strong, nonatomic) NSString* restaurantClosed;
@property (strong, nonatomic) NSString* kitchenOpened;
@property (strong, nonatomic) NSString* kitchenClosed;
@property (strong, nonatomic) NSString* email;
@property (strong, nonatomic) NSString* websiteUrl;
@property (strong, nonatomic) NSString* facebook;
@property (strong, nonatomic) NSString* instagram;
@property (strong, nonatomic) NSString* address;
@property (strong, nonatomic) NSString* telephoneNumbers;
@property (strong, nonatomic) NSArray* locations;
@property (strong, nonatomic) NSArray* courses;
@end
