//
//  FLImageFoodCollectionViewCell.m
//  FoodLook
//
//  Created by Yahin Rinat on 07.08.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import "FLImageFoodCollectionViewCell.h"

@implementation FLImageFoodCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (IBAction)likeButton:(id)sender {
    [self.delegate likeButton:self];
}




@end
