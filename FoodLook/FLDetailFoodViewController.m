//
//  FLDetailFoodViewController.m
//  FoodLook
//
//  Created by Yahin Rinat on 16.08.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import "FLDetailFoodViewController.h"
#import "FLRestoTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "ServerManager.h"
#import "FLCommentViewController.h"
#import "FLCommentTableViewCell.h"
#import "FLCommentModel.h"
#import "FLGeneralViewController.h"
#import <Social/Social.h>
#import "NSString+CalculateHeight.h"
#import <MBProgressHUD.h>
#import "FLDetailsRestoViewController.h"
@interface FLDetailFoodViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *addCommentButton;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *addComentBtn;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *lblLikeCount;
@property (weak, nonatomic) IBOutlet UILabel *lblCountShare;
@property (weak, nonatomic) IBOutlet UILabel *lblCountComment;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *topViewLine;
@property (weak, nonatomic) IBOutlet UIView *buttonlineView;
@property (weak, nonatomic) IBOutlet UIButton *showRestoBtn;
@property (strong, nonatomic) FLCommentTableViewCell* customCell;
@property (strong, nonatomic) NSArray* arrayComment;
@property (strong, nonatomic) NSString* acceptLanguage;
@property (strong, nonatomic) NSDictionary* socialDict;


@end
static NSString* kSettingsLanguage = @"language";
@implementation FLDetailFoodViewController

{

    NSDictionary* resultDict;
    NSDictionary* social;
   NSDictionary* restoDict;
}
- (instancetype)init
{
    self = [super init];
    if (self) {

    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_addComentBtn setTitle:NSLocalizedString(@"Add or read feedback", nil) forState:UIControlStateNormal];
    [self loadLanguage];
    
    NSString* strIdDish = nil;
    
    if (self.dish != nil) {
        strIdDish = self.dish.idDish;
    }
    if (self.vegeterianDish != nil) {
        strIdDish = self.vegeterianDish.idDish;
    }
    if (self.avrDish != nil) {
        strIdDish = self.avrDish.idDish;
    }
    if (self.idDish != nil) {
        strIdDish = self.idDish;
    }
      NSLog(@"%@",strIdDish);
    [self getDetailsDishFromServerWithStrID:strIdDish];
    [self getCommentsFromServerWithStrID:strIdDish];
    
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.addCommentButton setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:0]];
  
    self.arrayComment = [NSArray new];
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
    [self socialSettings];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"Swipe received.");
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark loadLanguage
-(void)loadLanguage
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.acceptLanguage = [userDefaults objectForKey:kSettingsLanguage];
    
}



#pragma mark - API
-(void)getDetailsDishFromServerWithStrID:(NSString*)strID
{
    [[ServerManager sharedManager]getCoursesDetailWithID:strID withAcceptLanguage:_acceptLanguage onSuccess:^(id result) {
        resultDict = result;
//        NSLog(@"%@",result);
        restoDict = [resultDict objectForKey:@"restaurant"];
        social = [resultDict objectForKey:@"social"];
        [self updateUI];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
    
}

-(void)getCommentsFromServerWithStrID:(NSString*)strID {
    
    [[ServerManager sharedManager] getCommentWithID:strID onSuccess:^(NSArray* result) {
//        NSLog(@"%@",result);
        self.arrayComment = result;
        [self.tableView reloadData];
    } onFailure:^(NSError *error, NSInteger statusCode) {
//
        NSLog(@"%@",error);
    }];
    
}

-(void)postShareFrommServerWithStrStrID:(NSString*)strID {
    
    [[ServerManager sharedManager]postShareInServerWithID:strID withOnSuccess:^(id results) {
    [self getDetailsDishFromServerWithStrID:strID];
          [MBProgressHUD hideHUDForView:self.view animated:YES];
    } onFailure:^(NSError *error, NSInteger statusCode) {
          [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

-(void)deleteLikeInServerWithIDEntity:(NSString*)strEntity andWithDishID:(NSString*)dishID {
    
    [[ServerManager sharedManager]deleteLikeInServerWithID:strEntity withOnSuccess:^(id results) {
    
        NSString* string  = [NSString stringWithFormat:@"%@",[self.dish.social objectForKey:@"likes"]];
        int likes = [string intValue];
        likes -= 1;
        
        [self.dish.social setObject:[NSString stringWithFormat:@"%d",likes] forKey:@"likes"];
        [self.dish.social setObject:@"<null>" forKey:@"like_id"];
        NSLog(@"%@",self.dish.social);
          [MBProgressHUD hideHUDForView:self.view animated:YES];
   [self getDetailsDishFromServerWithStrID:dishID];
    } onFailure:^(NSError *error, NSInteger statusCode) {
          [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}


-(void)updateUI
{
    
    NSString* nameDish = [resultDict objectForKey:@"label"];
    _labelNameDish.text = nameDish;
    [_labelNameDish sizeToFit];
      self.labelCostDish.text = [NSString stringWithFormat:@"%@",[resultDict objectForKey:@"price"]];
    
    CGFloat minButton = CGRectGetMinY(self.likeButton.frame);
    minButton -= 20;
    CGRect rect = CGRectMake(self.imageFood.frame.origin.x, self.imageFood.frame.origin.y, self.imageFood.frame.size.width, minButton);
    self.imageFood.frame = rect;
    
    
    NSString* text = [resultDict objectForKey:@"ingredients"];
    UIFont* font = [UIFont systemFontOfSize:14.f];
    CGFloat height  = [text getHeightOfFont:font widht:200];
    CGSize sizeLb = CGSizeMake(0, height);
    CGRect newFrame = self.lblIngredients.frame;
    newFrame.size.height = sizeLb.height;
    
    self.lblIngredients.frame = newFrame;
    self.lblIngredients.text = text;
    CGFloat maxYlabel =  CGRectGetMaxY(newFrame);
    CGRect buttomViewFrame = CGRectMake(self.buttonlineView.frame.origin.x, maxYlabel + 4, self.buttonlineView.frame.size.width, self.buttonlineView.frame.size.height);
    _buttonlineView.frame = buttomViewFrame;
    
    
    

    NSString *strLogo=  [restoDict objectForKey:@"logo"];
    NSString *strImage=  [resultDict objectForKey:@"image"];
    NSLog(@"%@",strImage);
    [self.imageFood setImageWithURL:[NSURL URLWithString:strImage]];
    [self.imgResto setImageWithURL:[NSURL URLWithString:strLogo ]];
    
    //Social
    

    self.lblLikeCount.text = [NSString stringWithFormat:@"%@",
                                   [social objectForKey:@"likes"]];
    [self.lblLikeCount sizeToFit];
    self.lblCountShare.text = [NSString stringWithFormat:@"%@",
                               [social objectForKey:@"shares"]];
    [self.lblCountShare sizeToFit];
    self.lblCountComment.text = [NSString stringWithFormat:@"%@",
                                 [social objectForKey:@"comments"]];
    [self.lblCountComment sizeToFit];
    
    NSString* str = [NSString stringWithFormat:@"%@",[social objectForKey:@"like_id"]];
    NSString* strNull = @"<null>";
    if ([str isEqualToString:strNull]) {
        [_likeButton setImage:[UIImage imageNamed:@"btnLikeGrey@2x"] forState:UIControlStateNormal];
    }else if (![str isEqualToString:strNull]) {
        [_likeButton setImage:[UIImage imageNamed:@"btnLikeRed@2x"] forState:UIControlStateNormal];
    }
    
    
    self.imgResto.layer.borderWidth=1.0;
    self.imgResto.layer.masksToBounds = YES;
    self.imgResto.layer.borderColor=[[UIColor redColor] CGColor];
    
}

-(void)socialSettings {
    
    static float offset = 17;
    static float offsetLbl = 4;

    float maxLikeBtnY = CGRectGetMaxX(self.likeButton.frame);
    self.lblLikeCount.frame = CGRectMake(maxLikeBtnY + offsetLbl,
                                         self.lblLikeCount.frame.origin.y,
                                         self.lblLikeCount.frame.size.width,
                                         self.lblLikeCount.frame.size.height);
    
    float maxLikeY = CGRectGetMaxX(self.lblLikeCount.frame);
    self.shareButton.frame = CGRectMake(maxLikeY + offset,
                                        self.shareButton.frame.origin.y,
                                        self.shareButton.frame.size.width,
                                        self.shareButton.frame.size.height);
    


    float maxshareBtnY = CGRectGetMaxX(self.shareButton.frame);
    self.lblCountShare.frame = CGRectMake(maxshareBtnY + offsetLbl,
                                          self.lblCountShare.frame.origin.y,
                                          self.lblCountShare.frame.size.width,
                                          self.lblCountShare.frame.size.height);
    float maxCountShareY = CGRectGetMaxX(self.lblCountShare.frame);
    self.addCommentButton.frame = CGRectMake(maxCountShareY + offset,
                                             self.addCommentButton.frame.origin.y,
                                             self.addCommentButton.frame.size.width,
                                             self.addCommentButton.frame.size.height);
    

    float maxbtnCommentY = CGRectGetMaxX(self.addCommentButton.frame);
   
    self.lblCountComment.frame = CGRectMake(maxbtnCommentY + offsetLbl,
                                            self.lblCountComment.frame.origin.y,
                                            self.lblCountComment.frame.size.width,
                                            self.lblCountComment.frame.size.height);
}


-(CGRect)updateSocialFrame:(float)maxYlabel
        andWtihButtonFrame:(CGRect)btnFrame{
    static float offset = 14;
    CGRect newFrameBtn = CGRectMake(maxYlabel + offset, btnFrame.origin.y,
                                    btnFrame.size.width,
                                    btnFrame.size.height);
    return newFrameBtn;
}

#pragma mark - Action

- (IBAction)likeAction:(UIButton *)sender {
      [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString* strDishID = [resultDict objectForKey:@"id"];
    [self getDetailsDishFromServerWithStrID:strDishID];
   __block NSString* likeID = [NSString stringWithFormat:@"%@",[social objectForKey:@"like_id"]];
    [[ServerManager sharedManager] postLikeInServerWithID:strDishID withOnSuccess:^(id result) {
        
        NSLog(@"%@",self.dish.social);
        NSString* string  = [NSString stringWithFormat:@"%@",[social objectForKey:@"likes"]];
        likeID = [NSString stringWithFormat:@"%@",[result objectForKey:@"id"]];
        int likes = [string intValue];
        likes += 1;
        [self.dish.social setValue:[NSString stringWithFormat:@"%d",likes]forKey:@"likes"];
        [self.dish.social setValue:likeID forKey:@"like_id"];
        NSLog(@"%@",self.dish.social);
          [MBProgressHUD hideHUDForView:self.view animated:YES];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        [self deleteLikeInServerWithIDEntity:likeID andWithDishID:strDishID];
          [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}
- (IBAction)addComment:(UIButton *)sender {
    [self performSegueWithIdentifier:@"FLCommentView" sender:nil];
}

- (IBAction)postToFacebook:(id)sender {
    
    NSString* strDishID = [resultDict objectForKey:@"id"];
    NSString* urlStr = [resultDict objectForKey:@"image"];
    NSString* textStr = [resultDict objectForKey:@"label"];

    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [controller setInitialText:textStr];
        [controller addURL:[NSURL URLWithString:urlStr]];
        [controller setCompletionHandler:^(SLComposeViewControllerResult result) {
            
            switch (result) {
                case SLComposeViewControllerResultCancelled:
//                    NSLog(@"Post Canceled");
                    break;
                case SLComposeViewControllerResultDone:
                    [self postShareFrommServerWithStrStrID:strDishID];
//                    NSLog(@"Post Sucessful");
                    break;
                    
                default:
                    break;
            }
        }];
        [self presentViewController:controller animated:YES completion:nil];
    }else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry!"
                                                        message:@"Please login to Facebook from the phone settings"
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:@"Ok",nil];
        [alert show];
    }
}

- (IBAction)backButtonAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
       NSString* strDishID = [resultDict objectForKey:@"id"];
    if ([segue.identifier isEqualToString:@"FLCommentViewController"]) {
        FLCommentViewController* vc = [segue destinationViewController];
        vc.strID = strDishID;
    }
    if ([segue.identifier isEqualToString:@"FLCommentView"]) {
    FLCommentViewController* vc = [segue destinationViewController];
        vc.strID = strDishID;
        
    }
    if ([segue.identifier isEqualToString:@"showDetailResto"]) {
        NSString* restoID = [[resultDict objectForKey:@"restaurant"]objectForKey:@"id"];
        NSLog(@"%@",restoID);
        FLDetailsRestoViewController* vc = [segue destinationViewController];
        vc.idResto = restoID;
        
    }
}

- (BOOL)canPerformUnwindSegueAction:(SEL)action fromViewController:(UIViewController *)fromViewController withSender:(id)sender{
    
    return YES;
}
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
  
    return YES;
}

#pragma mark - UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrayComment count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    FLCommentTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];

    if (!cell) {
        cell = [[FLCommentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    FLCommentModel* comment = [self.arrayComment objectAtIndex:indexPath.row];
    NSString* str = [NSString stringWithFormat:@"%@:",[comment.user objectForKey:@"username"]] ;
    cell.lblUsername.text = str;
    cell.lblComment.text  = comment.text;

    CGFloat tableViewWidth =   self.tableView.frame.size.width;
    CGFloat dX = tableViewWidth - tableViewWidth/5;
      
    CGFloat commentHeight = [comment.text getHeightOfFont:cell.lblComment.font widht:dX];
    CGFloat userName = [str getHeightOfFont:cell.lblUsername.font widht:tableViewWidth/5];
    cell.lblComment.frame = CGRectMake(tableViewWidth/5, cell.lblComment.frame.origin.y, dX, commentHeight);
    cell.lblUsername.frame = CGRectMake(0, cell.lblUsername.frame.origin.y, tableViewWidth/5, userName);

    return cell;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    FLCommentModel* post = [self.arrayComment objectAtIndex:indexPath.row];
    NSString* strComment = post.text;
    UIFont* font = [UIFont systemFontOfSize:13];
    CGFloat tableViewWidth =   self.tableView.frame.size.width;
    CGFloat dX = (tableViewWidth - tableViewWidth/5);
    NSString* strUser = [NSString stringWithFormat:@"%@:",[post.user objectForKey:@"username"]] ;

    CGFloat userName = [strUser getHeightOfFont:font widht:tableViewWidth/5];
    CGFloat commentHeight = [strComment getHeightOfFont:font widht:dX];
    if (commentHeight <  userName ) {
        return userName;
    }
  
   
    return commentHeight;
}



@end
