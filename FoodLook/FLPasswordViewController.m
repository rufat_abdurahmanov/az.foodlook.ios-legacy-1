//
//  FLPasswordViewController.m
//  FoodLook
//
//  Created by Rinat Yahin on 02.02.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLPasswordViewController.h"
#import "ServerManager.h"
#import "CredentialStore.h"
#import <MBProgressHUD.h>
@interface FLPasswordViewController () <UITextFieldDelegate, UIGestureRecognizerDelegate>
@property (strong, nonatomic) FLPasswordViewController* modal;
@property (weak, nonatomic) IBOutlet UITextField* oldPassTextField;
@property (weak, nonatomic) IBOutlet UITextField* existingPasField;
@property (weak, nonatomic) IBOutlet UITextField* confirmationField;
@property (weak, nonatomic) IBOutlet UIButton* dissmisBtn;
@property (weak, nonatomic) IBOutlet UILabel* changePassword;
@property (weak, nonatomic) IBOutlet UIView* containerView;
@property (strong, nonatomic) NSString* acceptLanguage;
@property (nonatomic, strong) CredentialStore *credentialStore;
@end

static NSString* kSettingsLanguage = @"language";
@implementation FLPasswordViewController
{
    MBProgressHUD* hud;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadLanguage];
    
//    UITapGestureRecognizer *tap = [self.revealViewController tapGestureRecognizer];
//    tap.delegate = self;
//    [self.view addGestureRecognizer:tap];
    
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_dissmisBtn setTitle:NSLocalizedString(@"Contacts", nil) forState:UIControlStateNormal];
    [_changePassword setText:NSLocalizedString(@"Change password", nil)];
    [_oldPassTextField  setPlaceholder:NSLocalizedString(@"Old password", nil)];
    [_existingPasField  setPlaceholder:NSLocalizedString(@"New password", nil)];
    [_confirmationField  setPlaceholder:NSLocalizedString(@"Repeat new password", nil)];
    
    [self updateView];
}

-(void)updateView {
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        //its iPhone. Find out which one?
        
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.height == 480)
        {
            // iPhone Classic
            CGRect conFrame = CGRectMake(self.containerView.frame.origin.x, self.containerView.frame.origin.y - 30, SCREEN_WIDTH, self.containerView.frame.size.height);
            self.containerView.frame = conFrame;
        }
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark loadLanguage
-(void)loadLanguage
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.acceptLanguage = [userDefaults objectForKey:kSettingsLanguage];
    [self.dissmisBtn addTarget:self action:@selector(dismissVC) forControlEvents:UIControlEventTouchUpInside];
}




- (void)dismissVC{
    
    [UIView animateWithDuration:0.4 animations:^ {
        CGRect rect = self.view.bounds;
        rect.origin.y += rect.size.height;
        self.view.frame = rect;

    }
                     completion:^(BOOL finished)
     {
         [self.view removeFromSuperview];
         [self removeFromParentViewController];
     }];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if ([textField isEqual:self.oldPassTextField]) {
        [self.existingPasField becomeFirstResponder];
    }else if ([textField isEqual:self.existingPasField]) {
        [self.confirmationField becomeFirstResponder];
            return YES;
    }
    [self changePassword:self.oldPassTextField.text andWithNewPassword:self.existingPasField.text];
    
    return YES;
}

-(void)changePassword:(NSString*)oldPassword andWithNewPassword:(NSString*)newPassword{
    [[ServerManager sharedManager]changeOldPassword:oldPassword withNewPassword:newPassword onSuccess:^(id results) {
        NSLog(@"%@",results);
    
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = @"Пароль изменен";
        [hud hide:YES afterDelay:2];
        
    
        NSString* password = self.confirmationField.text;
    
        [self.credentialStore setPassword:password];
        [self.confirmationField resignFirstResponder];
        [self dismissVC];
    
    } withAcceptLanguage:_acceptLanguage onFailure:^(NSError *error, NSInteger statusCode) {
        
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.labelText = @"Проверьте правильность ввода";
        [hud hide:YES afterDelay:2];
        
        [self.oldPassTextField becomeFirstResponder];
    }];
}

@end
