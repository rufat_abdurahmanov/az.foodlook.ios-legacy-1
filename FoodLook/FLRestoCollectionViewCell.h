//
//  FLRestoCollectionViewCell.h
//  FoodLook
//
//  Created by Yahin Rinat on 19.10.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLRestoCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageResto;
@property (weak, nonatomic) IBOutlet UIImageView *placeholder;
@end
