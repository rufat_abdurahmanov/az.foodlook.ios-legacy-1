//
//  FLSearchViewController.m
//  FoodLook
//
//  Created by Yahin Rinat on 01.07.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import "FLSearchViewController.h"
#import "FLCourses.h"
#import "FLResto.h"
#import "FLCustomTableViewCell.h"
#import "FLRestoTableViewCell.h"
#import "ServerManager.h"
#import "UIImageView+AFNetworking.h"
#import "FLDetailFoodViewController.h"
#import "FLDetailsRestoViewController.h"
#import "FLRestoSearch.h"
#import "FLCourseSearch.h"
#import "NSString+CalculateHeight.h"

@interface FLSearchViewController () <UITableViewDataSource, UITableViewDelegate,UISearchBarDelegate,FLCustomTableViewCellDelegate, UISearchDisplayDelegate,FLDetailFoodViewControllerDelegate,FLDetailsRestoViewControllerDelegate>
{
    BOOL isResto;
    BOOL isFood;
}

@property (strong, nonatomic) NSArray* coursesArray;
@property (strong, nonatomic) NSArray* restoArray;
@property (strong, nonatomic) NSString* acceptLanguage;
@property (strong, nonatomic) FLRestoTableViewCell* restoCell;
@property (strong, nonatomic) FLCustomTableViewCell* foodCell;
@property (weak, nonatomic) IBOutlet UITableView *tableViewMain;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong,nonatomic)UIImage* placeholder;
@end


static NSString* kSettingsLanguage = @"language";
@implementation FLSearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (IBAction)dismissVC:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - ViewController LifeCycle


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.segmentedControl setTitle:NSLocalizedString(@"Food", nil) forSegmentAtIndex:0];
    [self.segmentedControl setTitle:NSLocalizedString(@"Restaurants", nil) forSegmentAtIndex:1];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.coursesArray = [NSArray array];
    self.restoArray = [NSMutableArray array];
    self.placeholder = [UIImage imageNamed:@"placeholder"];
    isFood = YES;
    [self loadLanguage];
    
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];

}

-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"Swipe received.");
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

-(void)loadLanguage
{
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.acceptLanguage = [userDefaults objectForKey:kSettingsLanguage];
    
}

#pragma mark - API

- (void) searchDishWithText:(NSString*)text {

    
    [[ServerManager sharedManager]searchDishInServerWithText:text onSuccess:^(id results) {
        NSDictionary* courses = (NSDictionary *)results;
        NSMutableArray* tempArr = [NSMutableArray array];
        NSLog(@"%@",results);
        for (NSDictionary* dict in [courses objectForKey:@"results"]) {
            FLCourseSearch* courses = [[FLCourseSearch alloc]initWithServerResponse:dict];
            [tempArr addObject:courses];
        }
        self.coursesArray = tempArr;
        [self.tableViewMain reloadData];
    } withAcceptLanguage:_acceptLanguage onFailure:^(NSError *error, NSInteger statusCode) {
    
    }];
    
}



- (void) searchRestoWithText:(NSString*)text
{
    [[ServerManager sharedManager]searchRestoInServerWithText:text onSuccess:^(id results) {
        NSDictionary* courses = (NSDictionary *)results;
        NSMutableArray* tempArr = [NSMutableArray array];
         NSLog(@"%@",results);
        for (NSDictionary* dict in [courses objectForKey:@"results"]) {
            FLRestoSearch* courses = [[FLRestoSearch alloc]initWithServerResponse:dict];
            [tempArr addObject:courses];
        }
        self.restoArray = tempArr;
        [self.tableViewMain reloadData];
    } onFailure:^(NSError *error, NSInteger statusCode) {
        
    }];
    
    
}



#pragma mark - Actions

- (IBAction)goToGeneralVC:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)tableSwitch:(UISegmentedControl *)sender {
    
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        isResto = NO;
        isFood = YES;
        [self.tableViewMain  reloadData];
    }else if(self.segmentedControl.selectedSegmentIndex ==1) {
        isResto = YES;
        isFood = NO;
        [self.tableViewMain  reloadData];
        
    }
    
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        return [self.coursesArray count];
    }
    if (self.segmentedControl.selectedSegmentIndex == 1) {
        return [self.restoArray count];
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        //its iPhone. Find out which one?
        
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.height == 480)
        {
            // iPhone Classic
            return 180.0f;
        }
        else if(result.height == 568)
        {
            // iPhone 5
            return 220.0f;
        }
        else if(result.height == 667)
        {
            // iPhone 6
            return 266.0f;
        }
        else if(result.height == 736)
        {
            // iPhone 6 Plus
            return 302.0f;
        }
    }
    return 200;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *RestorauntIdentifier = @"RestorauntCell";
    static NSString *FoodIdentifier = @"FoodCell";

    
    if (self.segmentedControl.selectedSegmentIndex == 0 ) {
        
        FLCustomTableViewCell *cell = [self.tableViewMain dequeueReusableCellWithIdentifier:FoodIdentifier];
        if (!cell) {
            cell = [[FLCustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:FoodIdentifier];
        }
        
        FLCourseSearch * cours = [self.coursesArray objectAtIndex:indexPath.row];
        
        NSString* strName = cours.label;
        CGFloat offsetX = self.tableViewMain.frame.size.width - 75;
        CGFloat frameNamelbl = [strName getHeightOfFont:cell.nameDishesLabel.font widht:offsetX];
        cell.nameDishesLabel.frame = CGRectMake(8, 8, 250, frameNamelbl);
        cell.nameDishesLabel.text = strName;
        
        cell.delegate = self;
        cell.labelPrice.text = [NSString stringWithFormat:@"%@ AZN",cours.cost];
        cell.dishesImage.clipsToBounds  = YES;
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:cours.image_URL]];
        __weak FLCustomTableViewCell* weakCell = cell;
        
        [cell.dishesImage setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            weakCell.imageView.frame = CGRectMake(0, 0, weakCell.frame.size.width, weakCell.frame.size.height);
            weakCell.dishesImage.clipsToBounds  = YES;
            cell.dishesImage.frame = CGRectMake(0, 0, weakCell.frame.size.width, weakCell.frame.size.height);
            weakCell.dishesImage.contentMode = UIViewContentModeScaleAspectFill;
            weakCell.dishesImage.autoresizingMask = UIViewAutoresizingNone;
            weakCell.dishesImage.image = image;
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            
        }];
        
        return cell;

    }else if (self.segmentedControl.selectedSegmentIndex == 1) {
    
        
        FLRestoTableViewCell *cell = [self.tableViewMain dequeueReusableCellWithIdentifier:RestorauntIdentifier];
        if (!cell) {
            cell = [[FLRestoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:RestorauntIdentifier];
        }
        
        FLRestoSearch* resto = [self.restoArray objectAtIndex:indexPath.row];
        NSString* strName =  resto.label;
        CGFloat offsetX = self.tableViewMain.frame.size.width;
        CGFloat frameNamelbl = [strName getHeightOfFont:cell.lblNameResto.font widht:offsetX];
        cell.lblNameResto.frame = CGRectMake(8, 8, offsetX, frameNamelbl);
        cell.lblNameResto.text = strName;
        [cell.imageLogo setImageWithURL: [NSURL URLWithString:resto.logo_url]];
        cell.imageLogo.layer.borderWidth=1.0;
        cell.imageLogo.layer.masksToBounds = YES;
        cell.imageLogo.layer.borderColor=[[UIColor redColor] CGColor];
        [cell.makeReservButton setTitle:NSLocalizedString(@"Make Reservation", nil) forState:UIControlStateNormal];
        CGFloat maxY = CGRectGetMaxY(cell.frame);
        CGFloat dMaxY = maxY - cell.conteinerView.frame.size.height;
        CGRect rectView = CGRectMake(cell.conteinerView.frame.origin.x , dMaxY, SCREEN_WIDTH, cell.conteinerView.frame.size.height);
        cell.conteinerView.frame = rectView;
        CGFloat dMaxLogoY = maxY - cell.imageLogo.frame.size.height;
        CGRect frmaLogo = CGRectMake(cell.imageLogo.frame.origin.x, dMaxLogoY, cell.imageLogo.frame.size.width, cell.imageLogo.frame.size.height);
        cell.imageLogo.frame = frmaLogo;
        
        NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:resto.background_image_url ]];
        __weak FLRestoTableViewCell* weakCell = cell;
        
        [cell.restoImage setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            weakCell.restoImage.clipsToBounds  = YES;
            cell.restoImage.frame = CGRectMake(0, 0, weakCell.frame.size.width, weakCell.frame.size.height);
            weakCell.restoImage.contentMode = UIViewContentModeScaleAspectFill;
            weakCell.restoImage.autoresizingMask = UIViewAutoresizingNone;
            
            weakCell.restoImage.image = image;
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            
        }];
        return cell;

        
    }
    return nil;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (isFood) {
        FLCourseSearch* cours = [self.coursesArray objectAtIndex:indexPath.row];
        [self passDataFoodVCWithIdDish:cours.idDish];
    }
    if (isResto) {
        FLRestoSearch* resto = [self.restoArray objectAtIndex:indexPath.row];
        [self passDataRestoVCWithIdDish:resto.idResto];
    }
}



#pragma mark Segue

-(void)passDataFoodVCWithIdDish:(NSString *)idDish {
    [self performSegueWithIdentifier:@"Food" sender:idDish];
}

-(void)passDataRestoVCWithIdDish:(NSString *)idResto {
    [self performSegueWithIdentifier:@"RestoIdentifier" sender:idResto];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"Food"]) {
        //      NSLog(@"Food!");
        FLDetailFoodViewController* foodVC = segue.destinationViewController;
        foodVC.delegate = self;
        foodVC.idDish = sender;
    }
    if ([segue.identifier isEqualToString:@"RestoIdentifier"]) {
        // NSLog(@"Resto");
        FLDetailsRestoViewController* restoVC = segue.destinationViewController;
        restoVC.delegate = self;
        restoVC.idResto = sender;
    }
    
}


#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar {
    if (self.segmentedControl.selectedSegmentIndex == 0) {
            searchBar.text = @"";
       
        
    }else {
            searchBar.text = @"";

            [self.tableViewMain reloadData];
    }
    [searchBar resignFirstResponder];
  
    [searchBar setShowsCancelButton:NO animated:YES];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    if (self.segmentedControl.selectedSegmentIndex == 0) {
        [self  searchDishWithText:searchBar.text];
    }
    if (self.segmentedControl.selectedSegmentIndex == 1){
        [self  searchRestoWithText:searchBar.text];
    }
    
    [searchBar resignFirstResponder];
}







@end
