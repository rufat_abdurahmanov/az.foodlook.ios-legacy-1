//
//  FLMenuResto.h
//  FoodLook
//
//  Created by Yahin Rinat on 10.10.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FLMenuResto : NSObject
- (id) initWithServerResponse:(NSDictionary*) responseObject;

@property (strong, nonatomic) NSString* idDish;
@property (strong, nonatomic) NSString* nameDish;
@property (strong, nonatomic) NSString* cost;
@property (strong, nonatomic) NSString * imageDish;




@end
