    //
//  main.m
//  FoodLook
//
//  Created by Yahin Rinat on 28.06.14.
//  Copyright (c) 2014 Yahin Rinat. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FLAppDelegate class]));
    }
}
