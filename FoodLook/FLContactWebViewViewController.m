//
//  FLContactWebView.m
//  FoodLook
//
//  Created by Rinat Yahin on 14.02.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLContactWebViewViewController.h"

@interface FLContactWebViewViewController () <UIWebViewDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView* indicator;
@property (weak, nonatomic) IBOutlet UIWebView* webView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem* backButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem* forwardButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem* refresh;
@end

@implementation FLContactWebViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",self.urlWeb);
    NSURL* url = [NSURL URLWithString:self.urlWeb];
    NSURLRequest* req = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:req];
    
    //    [self.dissmisVC addTarget:self action:@selector(dismissVC) forControlEvents:UIControlEventTouchUpInside];
//    UITapGestureRecognizer *tap = [self.revealViewController tapGestureRecognizer];
//    tap.delegate = self;
//    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc {
    self.webView.delegate = nil;
    NSLog(@"webView deallocate");
}



#pragma mark - Actions


-(IBAction)actionBack:(id)sender{
    if ([self.webView canGoBack]) {
        [self.webView stopLoading];
        [self.webView goBack];
    }
    
}

-(IBAction)actionForward:(id)sender{
    if ([self.webView canGoForward]) {
        [self.webView stopLoading];
        [self.webView goForward];
    }
}

-(IBAction)actionRefresh:(id)sender{
    [self.webView reload];
}

-(IBAction)cancel:(id)sender{
        [UIView animateWithDuration:0.4 animations:^ {
            CGRect rect = self.view.bounds;
            rect.origin.y += rect.size.height;
            self.view.frame = rect;
    
        }
                         completion:^(BOOL finished)
         {
             [self.view removeFromSuperview];
             [self removeFromParentViewController];
         }];
}

#pragma mark -  UIWebViewDelegate


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}
- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self.indicator startAnimating];
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self.indicator stopAnimating];
    
    self.backButtonItem.enabled = [self.webView canGoForward];
    self.forwardButton.enabled = [self.webView canGoForward];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self.indicator stopAnimating];
}

@end
