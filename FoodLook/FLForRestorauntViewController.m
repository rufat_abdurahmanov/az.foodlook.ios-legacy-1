//
//  FLForRestorauntViewController.m
//  FoodLook
//
//  Created by Rinat Yahin on 02.02.15.
//  Copyright (c) 2015 Yahin Rinat. All rights reserved.
//

#import "FLForRestorauntViewController.h"
#import "SWRevealViewController.h"


@interface FLForRestorauntViewController ()<UIGestureRecognizerDelegate,SWRevealViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *sidebarButton;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIView *customNavigationBar;
@end

@implementation FLForRestorauntViewController

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_sidebarButton setTitle:NSLocalizedString(@"For restaurants", nil) forState:UIControlStateNormal];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.sidebarButton  addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    CGFloat ih = self.imgView.image.size.height;
    CGFloat nh = self.customNavigationBar.frame.size.height;
    
    CGFloat sizeOfContent = ih+nh;
    
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH, sizeOfContent);
    CGFloat maxNavY = CGRectGetMaxY(self.customNavigationBar.frame);
    self.imgView.frame = CGRectMake(0,maxNavY, SCREEN_WIDTH, self.imgView.image.size.height);
    UITapGestureRecognizer *tap = [self.revealViewController tapGestureRecognizer];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    SWRevealViewController *revealViewController = self.revealViewController;
    revealViewController.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position {
    switch (position) {
        case  FrontViewPositionLeft:
            
            break;
            
        case FrontViewPositionRight:
            
            break;
            
        default:
            break;
    }
}

@end
